//http://ctms.engin.umich.edu/CTMS/index.php?example=MotorSpeed&section=SystemModeling

J=0.01;//moment of inertia of the rotor+load
b=0.1; //viscous friction
K=0.01; //torque factor
R=1;  //Field resitance
L=0.5; //field inductance
//Voltage ->current
V2I=syslin('c',1/((%s*L+R)
//Voltage -->torque
V2T=syslin('c',K/((%s*L+R)
//Torque -> angular speed
T2S=syslin('c',1/((%s*J+b)
//Voltage -> angular speed
V2S=syslin('c',K/((J*%s+b)*(L*%s+R)+K^2))
