eqns:={e=F*r-dt,u=C*e+du,y=G1*u+Gd*dy,dt=y-G2*C*e};




S:=eliminate(eqns,{e,y,dt}):
U:=solve(S[2][1],u);

S:=eliminate(eqns,{e,u,dt}):
Y:=solve(S[2][1],y);

S:=eliminate(eqns,{e,u,y}):
DT:=solve(S[2][1],dt);

with(CodeGeneration):
Matlab(U);
Matlab(Y);
Matlab(DT);
