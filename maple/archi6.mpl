eqns:={e1=F*r1-(n2+H2*y1),e2=C1*e1-(n1+H1*y2),u1=C2*e2+du1,y2=G1*u1,u2=y2+du2,y1=G2*u2+dy}:

S_u1:=eliminate(eqns,{e1,e2,u2,y1,y2}):
U1:=solve(S_u1[2][1],u1);

S_u2:=eliminate(eqns,{e1,e2,u1,y1,y2}):
U2:=solve(S_u2[2][1],u2);

S_y1:=eliminate(eqns,{e1,e2,u1,u2,y2}):
Y1:=solve(S_y1[2][1],y1);

S_y2:=eliminate(eqns,{e1,e2,u1,u2,y1}):
Y2:=solve(S_y2[2][1],y2);

with(CodeGeneration):
Matlab(U1);
Matlab(U2);
Matlab(Y1);
Matlab(Y2);

#Open loop output of C1
eqns := {e=uo-H1*y2,y2=G1*C2*e,y1=G2*y2, yo = C1*y1}:
S:=eliminate(eqns,{y1,y2,u1,e}):
Yo:=solve(S[2][1],yo);

#Open loop output of C2
eqns := {y2=G1*uo,y1=G2*y2, yo=-C2*H1*y2-C2*C1*H2*y1 }:
S:=eliminate(eqns,{y1,y2}):
Yo:=solve(S[2][1],yo);
