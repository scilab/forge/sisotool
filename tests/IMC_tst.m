%%continuous time
%stable and MP
[C, Q] = IMC(-1,[-2 -3],1,1.2);Q,C
[C, Q] = IMC(-1,[-1+i -1-i],1,1.2);Q,C
[C, Q] = IMC([-1 -1],[-2 -3],1,1.2);Q,C

[C, Q] = IMC(-1,[-2 -3 -3],1,1.2);Q,C


%unstable and MP
[C, Q] = IMC(-1,[2 -3],1,1.2);Q,C
[C, Q] = IMC([-1 -1],[2 -3],1,1.2);Q,C
[C, Q] = IMC(-1,[2 -3 -3],1,1.2);Q,C
%unstable and NMP
[C, Q] = IMC(2,[1;-1],1,1.2);Q,C

%%discrete time
%stable and MP                         
[C,Q,Qtild,f] = IMC_d(-0.5,[-0.2 -0.3],1,1,1.2);

[C,Q,Qtild,f] = IMC_d(-0.5,[0.2 0.3],1,1,1.2);

[C,Q,Qtild,f] = IMC_d([-0.5 0.5],[-0.2;-0.3],1,1,1.2);

%stable and NMP                            
[C,Q,Qtild,f] = IMC_d(-1.5,[-0.2;-0.3],1,1,1.2);

%unstable and MP    
[C,Q,Qtild,f] = IMC_d(-0.5,[-0.2;-1.3],1,1,1.2);

%unstable and NMP    
                        
[C,Q,Qtild,f] = IMC_d(-1.5,[-0.2;-1.3],1,1,1.2);
