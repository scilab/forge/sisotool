s=%s
h=tlist(["r","num","den","dt"],-21197.921+245107.14*s+47801.858*s^2+4159.5708*s^3+228.8084*s^4+10.9295*s^5,14014.322+15303.246*s+25546.243*s^2+4411.335*s^3+509.66162*s^4+33.6172*s^5+s^6,"c") ; 
step_response(sys);

step_response(h/(%s^2+%s*0.1+20);

step_response(h/(%s+1d-6));

step_response(h/(%s^2));//strange

step_response(h/(%s));

zeta=0.51;wn=0.52;
step_response(h/(%s^2+2*zeta*wn*%s+wn^2));

zeta=0.51;wn=2;
h=syslin("c",1/(%s^2+2*zeta*wn*%s+wn^2));
step_response(h);

zeta=0.1;wn=2;
h=syslin("c",1/(%s^2+2*zeta*wn*%s+wn^2));
step_response(h);
