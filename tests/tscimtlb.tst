//test  marge de phase
s=%s;
G=syslin('c',poly(1,'s','c'),poly(1,'s','c'));//plant
C=2*syslin('c',poly(-0.7074,'s'),real(poly([-0.5852-0.6741*%i,-0.5852+0.6741*%i],'s')));//compensator
H=1; //sensor dynamics
F=1; //prefilter
sisotool(['rlocus','nichols',"compensatoreditor"],G,C,H,F);
