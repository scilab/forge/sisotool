
function [in,out]=BLOCK(x,y,col,dims,label)
  if exists('dims','local') then
    w=dims(1),h=dims(2)
  else
    w=60;h=40;
  end
  xrect(x,y+h/2,w,h)
  e=gce();
  e=gce();e.fill_mode='on';
  e.background=col;
  if exists('label','local') then
    xstringb(x,y-h/2,label,w,h)
  end

  in=[x,y];
  out=[x+w,y];
endfunction

function [in,out]=VBLOCK(x,y,col,dims,label)
  if exists('dims','local') then
    w=dims(1),h=dims(2)
  else
    w=60;h=40;
  end
  xrect(x-w/2,y,w,h)
  e=gce();
  e=gce();e.fill_mode='on';
  e.background=col;
  if exists('label','local') then
    xstringb(x-w/2,y-h,label,w,h)
  end

  in=[x,y];
  out=[x,y-h];
endfunction

function [inH,inV,out]=SUM(x,y,o)
  if argn(2)<3 then o='down',end
  d=8
  xarc(x,y+d/2,d,d,0,360*64)
  inH=[x,y];
  out=[x+d,y];
  if o=='down' then
    inV=[x+d/2,y+d/2]
  else
    inV=[x+d/2,y-d/2]
  end
endfunction
function CONNECT(out,in,opt,ilabel,elabel)

  curx=out(1);cury=out(2);
  if exists('ilabel','local')&exists("withlabel")&withlabel then
    xstring(curx,cury,ilabel)
  end
  
  for k=1:size(in,1)
    xsegs([curx;in(k,1)],[cury;in(k,2)])
    curx=in(k,1);cury=in(k,2);
  end
  e=gce();e.arrow_size=230;
  if exists('elabel','local')&exists("withlabel")&withlabel then
    xstring(curx,cury,elabel)
  end

  if exists('opt','local') then
    e.segs_color=color('lightgray')
    e.line_style=5
  end
endfunction



function c1()
  
  ll=27; 
  //filter
  [infilt,outfilt]=BLOCK(30,40,15,label='F')
  //input
  CONNECT(infilt-[ll 0],infilt,ilabel='r');
  //sum
  [insumh,insumv,outsum]=SUM(outfilt(1)+ll,outfilt(2));
  CONNECT(outfilt,insumh);
  //compensator
  [inc,outc]=BLOCK(outsum(1)+ll,outsum(2),5,label='C');
  CONNECT(outsum,inc);
  //sum2
   [insum2h,insum2v,outsum2]=SUM(outc(1)+ll,outc(2),'up');
  CONNECT(outc,insum2h);
  //du
  CONNECT(insum2v-[0 ll],insum2v,'aux',ilabel='du')
  //G
  [inpg,outpg]=BLOCK(outsum2(1)+ll,outsum2(2),32,label='G');
  CONNECT(outsum2,inpg);
  //sum3
  [insum3h,insum3v,outsum3]=SUM(outpg(1)+ll,outpg(2),'up');
  //dy
  CONNECT(insum3v-[0 ll],insum3v,'aux',ilabel='dy')

  CONNECT(outpg,insum3h);

  //output
  CONNECT(outsum3,[outsum3(1)+2*ll,outsum3(2)],elabel='y');
  //H
  [outh,inh]=BLOCK(insum2h(1),110,32,label='H');
  CONNECT(outsum3+[ll 0],[outsum3(1)+ll,inh(2);inh]);
  //sum4
  [outsum4,insum4v,insum4h]=SUM(outh(1)-ll,outh(2));
  //n
  CONNECT(insum4v+[0 ll],insum4v,'aux',ilabel='n')

  CONNECT(outh,insum4h);

  CONNECT(outsum4,[[insumv(1) outsum4(2)];insumv])
endfunction

function c2()
  ll=27; 
  L=90;
  //filter
  [infilt,outfilt]=BLOCK(30,45,15,label='F')
  //input
  CONNECT(infilt-[ll 0],infilt,ilabel='r');
  //sum
  [insumh,insumv,outsum]=SUM(outfilt(1)+ll,outfilt(2));
  CONNECT(outfilt,insumh);
  //du
  insumup=[insumv(1) 2*outsum(2)-insumv(2)]
  CONNECT(insumup-[0 ll],insumup,'aux',ilabel='du')
  // G
  [ing,outg]=BLOCK(outsum(1)+L,outsum(2),32,label='G');  
  CONNECT(outsum,ing);
  //sum
  [insum2h,insum2v,outsum2]=SUM(outg(1)+2*ll,outg(2),'up');
  CONNECT(outg,insum2h);
  //dy
  CONNECT(insum2v-[0 ll],insum2v,'aux',ilabel='dy')
  //output
  CONNECT(outsum2,outsum2+[L 0],elabel='y');
  
  //H
  [outh,inh]=BLOCK(insum2h(1)-ll,100,32,label='H');
  
  CONNECT([outsum2(1)+L/2,outg(2)],[[outsum2(1)+L/2,inh(2)];inh]);
  //sum3
  [outsum3,insum3v,insum3h]=SUM(outh(1)-ll,outh(2));
  CONNECT(outh,insum3h);

  //n
   CONNECT(insum3v+[0 ll],insum3v,'aux',ilabel='n');
  //C
  [outc,inc]=BLOCK(insumh(1)+ll ,outh(2),5,label='C');
  CONNECT(outsum3,inc);
    
  CONNECT(outc,[[insumv(1) outc(2)];insumv])
endfunction

function c3()
  ll=27; 
  L=70;
  
  //sum1
  [insum1h,insum1v,outsum1]=SUM(3+2*ll,70);
  //input
  CONNECT(insum1h-[2*ll 0],insum1h,ilabel='r');
  Node=insum1h-[ll 0];
  //filter
  [infilt,outfilt]=BLOCK(55,25,15,label='F')
  CONNECT(Node,[Node(1) infilt(2);infilt]);
  //compensator
  [inc,outc]=BLOCK(outsum1(1)+L ,outsum1(2),5,label='C');
  CONNECT(outsum1,inc);
  //sum2
  [insum2h,insum2v,outsum2]=SUM(outc(1)+ll,outc(2),'up');
  CONNECT(outc,insum2h);
  CONNECT(outfilt,[insum2v(1) outfilt(2);insum2v]);
  //sum3
  [insum3h,insum3v,outsum3]=SUM(outsum2(1)+ll,outsum2(2),'up');
  CONNECT(outsum2,insum3h);
  //du
  CONNECT(insum3v-[0 ll],insum3v,'aux',ilabel='du')

  // G
  [inG,outG]=BLOCK(outsum3(1)+ll,outsum3(2),32,label='G');
  CONNECT(outsum3,inG);
  //sum4
  [insum4h,insum4v,outsum4]=SUM(outG(1)+ll,outG(2),'up');
  CONNECT(outG,insum4h);
  //dy
  CONNECT(insum4v-[0 ll],insum4v,'aux',ilabel='dy')

  
  //output
  CONNECT(outsum4,outsum4+[2*ll 0],elabel='y');
  Node=[outsum4(1)+ll,outsum4(2)]
  
  //H
  [outH,inH]=BLOCK(insum2h(1),110,32,label='H');
  CONNECT(Node,[[Node(1),inH(2)];inH]);
  //sum5
  [outsum5,insum5v,insum5h]=SUM(outH(1)-ll,outH(2));
  CONNECT(outH,insum5h);
  //n
  CONNECT(insum5v+[0 ll],insum5v,'aux',ilabel='n')
 
    
  CONNECT(outsum5,[[insum1v(1) outsum5(2)];insum1v])
endfunction

function c4()
  ll=27; 
  L=90;
  d=[60,40];//block sizes
    //filter
  [infilt,outfilt]=BLOCK(30,35,15,d,label='F')
  //input
  CONNECT(infilt-[ll 0],infilt,ilabel='r');
  //sum1
  [insum1h,insum1v,outsum1]=SUM(outfilt(1)+ll,outfilt(2));
  CONNECT(outfilt,insum1h);
  //compensator
  [inc,outc]=BLOCK(outsum1(1)+ll,outsum1(2),5,d,label='C1');
  CONNECT(outsum1,inc);
  //sum2
  [insum2h,insum2v,outsum2]=SUM(outc(1)+ll,outc(2));
  insum2up=[insum2v(1) 2*outsum2(2)-insum2v(2)]
  //du
  CONNECT(insum2up-[0 ll],insum2up,'aux',ilabel='du')
  CONNECT(outc,insum2h);
  //G
  [inG,outG]=BLOCK(outsum2(1)+ll,outsum2(2),32,d,label='G');
  CONNECT(outsum2,inG);
  //sum3
  [insum3h,insum3v,outsum3]=SUM(outG(1)+ll,outG(2),'up');
   CONNECT(outG,insum3h);
  //dy
  CONNECT(insum3v-[0 ll],insum3v,'aux',ilabel='dy')
  
  //output
  CONNECT(outsum3,outsum3+[2*ll 0],elabel='y');
  Node=outsum3+[ll 0]
  // H
  [outH,inH]=BLOCK(inG(1)+ll,115,32,d,label='H');
  CONNECT(Node,[Node(1),inH(2);inH]);
  //sum4
  [outsum4,insum4v,insum4h]=SUM(outH(1)-ll,outH(2));
  CONNECT(outH,insum4h)
  //n
  CONNECT(insum4v+[0 ll],insum4v,'aux',ilabel='n')
  CONNECT(outsum4,[[insum1v(1) outsum4(2)];insum1v])
  
  //compensator 2
  [inC2,outC2]=VBLOCK(insum2v(1),outsum4(2)-ll/1.5,5,[40,40],label='C2');

  CONNECT([insum2v(1),outH(2)],inC2);
  CONNECT(outC2,insum2v)
 

endfunction

function c5()
  ll=27; 
  L=90;
  d=[60,40];//block sizes
  //F
  [infilt,outfilt]=BLOCK(30,68,15,d,label='F')
  //input
  CONNECT(infilt-[ll 0],infilt,ilabel='r');
  //sum1
  [insum1h,insum1v,outsum1]=SUM(outfilt(1)+ll,outfilt(2));
  CONNECT(outfilt,insum1h);
  //C
  [inc,outc]=BLOCK(outsum1(1)+ll,outsum1(2),5,d,label='C');
  CONNECT(outsum1,inc);
  //sum u
  [insumuh,insumuv,outsumu]=SUM(outc(1)+ll,outc(2),'up');
  CONNECT(outc,insumuh)
  //du
  CONNECT(insumuv-[0 ll],insumuv,'aux',ilabel='du')
  
   //G1
  [ing1,outg1]=BLOCK(outc(1)+3*ll,outc(2),32,d,label='G1');
  CONNECT(outsumu,ing1);
  
  //sum2
  [insum2h,insum2v,outsum2]=SUM(outg1(1)+ll,outg1(2),'up');
  CONNECT(outg1,insum2h);
  //output
  CONNECT(outsum2,[outsum2(1)+1.5*ll,outsum2(2)],elabel='y');
  //Gd
  [ingd,outgd]=BLOCK(ing1(1),24,32,d,label='Gd');
  CONNECT(outgd,[insum2v(1),outgd(2);insum2v])
  //input
  CONNECT(ingd-[ll 0],ingd,'aux',ilabel='dy')
  
  //G2
  [ing2,outg2]=BLOCK(ing1(1),112,32,d,label='G2');
  CONNECT(outc+[ll/3 0],[outc(1)+ll/3 ing2(2);ing2])
   //sum3
  [insum3h,insum3v,outsum3]=SUM(outg2(1)+2*ll,outg2(2),'up');
  CONNECT(outg2,insum3h);
  CONNECT([insum3v(1) outsum2(2)],insum3v)
  
  CONNECT(outsum3,[outsum3+[ll/2 0];outsum3(1)+ll/2,145;insum1v(1),145;insum1v]) 
endfunction


function c6()
  ll=18; 
  ll1=27
  L=90;
  d=[30,40];//block sizes
  //F
  [infilt,outfilt]=BLOCK(30,40,15,d,label='F')
  //input
  CONNECT(infilt-[ll 0],infilt,ilabel='r');
  //sum1
  [insum1h,insum1v,outsum1]=SUM(outfilt(1)+ll,outfilt(2));
  CONNECT(outfilt,insum1h);
  //C1
  [inc1,outc1]=BLOCK(outsum1(1)+ll,outsum1(2),5,d,label='C1');
  CONNECT(outsum1,inc1);
  
  //sum2
  [insum2h,insum2v,outsum2]=SUM(outc1(1)+ll,outc1(2));
  CONNECT(outc1,insum2h);
  //C2
  [inC2,outC2]=BLOCK(outsum2(1)+ll,outsum2(2),5,d,label='C2');
  CONNECT(outsum2,inC2);
  //sum3
  [insum3h,insum3v,outsum3]=SUM(outC2(1)+ll,outC2(2),'up');
  //du1
  CONNECT(insum3v-[0 ll1],insum3v,'aux',ilabel='du1')
  CONNECT(outC2,insum3h);
   //G1
  [ing1,outg1]=BLOCK(outsum3(1)+1.5*ll,outsum3(2),32,d,label='G1');
  CONNECT(outsum3,ing1);
  //sum4
  [insum4h,insum4v,outsum4]=SUM(outg1(1)+2*ll,outg1(2),'up');
  //du2
  CONNECT(insum4v-[0 ll1],insum4v,'aux',ilabel='du2')
  CONNECT(outg1,insum4h);
  //G2
  [ing2,outg2]=BLOCK(outsum4(1)+ll,outsum4(2),32,d,label='G2');
  CONNECT(outsum4,ing2);
  //sum5
  [insum5h,insum5v,outsum5]=SUM(outg2(1)+ll,outg2(2),'up');
  //dY
  CONNECT(insum5v-[0 ll1],insum5v,'aux',ilabel='dy')

  CONNECT(outg2,insum5h);
  //output
  CONNECT(outsum5,outsum5+[2*ll 0],elabel='y')
  //H1
  [outh1,inh1]=BLOCK(ing1(1),90,32,d,label='H1');
  CONNECT(outg1+[ll 0],[outg1(1)+ll inh1(2);inh1])
  //sum6
  [outsum6,insum6v,insum6h]=SUM(outh1(1)-ll-8,outh1(2));
  //n1
  CONNECT(insum6v+[0 ll1],insum6v,'aux',ilabel='n1')

  CONNECT(outh1,insum6h)
  CONNECT(outsum6,[insum2v(1) outsum6(2);insum2v])
  //H2
  [outh2,inh2]=BLOCK(ing2(1),120,32,d,label='H2');
  CONNECT(outsum5+[ll 0],[outsum5(1)+ll inh2(2);inh2])
  //sum7
  [outsum7,insum7v,insum7h]=SUM(outh2(1)-ll-8,outh2(2));
  //n2
  CONNECT(insum7v+[0 ll1],insum7v,'aux',ilabel='n2')

  CONNECT(outh2,insum7h)
  CONNECT(outsum7,[insum1v(1) outsum7(2);insum1v])

  
endfunction
//path='/home/steer/sisotool/images/'
path='/tmp/'
clf();fig=gcf();
fig.axes_size = [420,150];
ax=gca();
ax.data_bounds=[0 0;420 150];
ax.margins=[0 0 0 0];
ax.axes_reverse=['off','on'];
ax.isoview='on';
ax.thickness=2;
  //ax.box='on';

withlabel=%t
for k=1:6,
  ax=gca();
  ax.data_bounds=[0 0;420 150];
  ax.margins=[0 0 0 0];
  ax.axes_reverse=['off','on'];
  ax.isoview='on';
  ax.thickness=2;

  drawlater();execstr('c'+string(k)+'()'),drawnow()
  xs2png(0,path+'C'+string(k)+'.png')

  clf
end

withlabel=%f
fig.axes_size = [140,50];
for k=1:6,
  ax=gca();
  ax.data_bounds=[0 0;420 150];
  ax.margins=[0 0 0 0];
  ax.axes_reverse=['off','on'];
  ax.isoview='on';
  ax.thickness=2;

  drawlater();execstr('c'+string(k)+'()'),drawnow()
  xs2png(0,path+'c'+string(k)+'.png')
 clf
end
