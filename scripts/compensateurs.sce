h=syslin('c',(-4*s^4+48*s^3-18*s^2+250*s+600)/(s^4+30*s^3+282*s^2+525*s+60))
//povy
sl=syslin('c',5*(1+s)/(.1*s^4+s^3+15*s^2+3*s+1))
black(sl,0.01,100)
http://www.charlotte-seidner.fr/docs/2009/GEII/CoursCh5.pdf


http://www.trop.uha.fr/master/downloads/correcteursblack.pdf
G=syslin('c',16000/((s+1)*(s+10)*(s+100)))
clf;black(G,0.01,100);nicholschart()
//avance de phase
a=10;tau=0.01;
clf;black([G;G*(1+a*tau*s)/(1+tau*s)],0.01,100);nicholschart()
//+gain
 A=syslin('c',(1+a*tau*s)/(1+tau*s))
clf;black([G;G*A;G*A*3.47],0.01,100);nicholschart()
//PI
PI=(1+3*s)/(3*s);
clf;black([G;G*PI],0.01,100);nicholschart()
//R
R=syslin('c',10*(1+2*s)/(1+10*2*s));
clf;black([G;G*R ],0.01,100);nicholschart()
//PID
tau=0.2;xsi=1.2
PID=syslin('c',(1/(2*xsi*tau*s))*(1+2*xsi*tau*s+tau^2*s^2))
clf;black([G;G*PID ],0.01,100);nicholschart()
phase lead and phase lag compensators

