<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolRedrawAnalysers" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolRedrawAnalysers</refname>

    <refpurpose>updates Analysers curves (recomputing
    discretization).</refpurpose>
  </refnamediv>

  <refsection>
    <title>Synopsis</title>

    <synopsis>sisotoolRedrawAnalysers(mainH,update,sel) </synopsis>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>mainH</term>

        <listitem>
          <para>The handle of the Editors window.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>update</term>

        <listitem>
          <para>boolean: if true a simple update of the analyser sub windows
          is made for efficiency during move operations, if false a complete
          redraw is made.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>sel</term>

        <listitem>
          <para>The indexes of the selected sub windows.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Updates Analysers curves (recomputing discretization).</para>
  </refsection>

  <refsection>
    <title>Called functions</title>

    <informaltable border="1">
          <tr>
            <td>
              <link linkend="sisotoolGetSession">
                <function>sisotoolGetSession</function>
              </link>
            </td>

            <td>retrieves the <link linkend="sisotoolSessionData">session data
            structure</link> from the editors window user_data field.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sim_horizon">
                <function>sim_horizon</function>
              </link>
            </td>

            <td>computes final time and discretization step for step
            responses.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolFinalValue">
                <function>sisotoolFinalValue</function>
              </link>
            </td>

            <td>returns steady state value for stable systems step and
            impulse responses.</td>
          </tr>

          <tr>
            <td>
              <link linkend="updateDatatips">
                <function>updateDatatips</function>
              </link>
            </td>

            <td>update the curves <link linkend="???">datatips</link> when
            curve data changes (bug 14788 workaround).</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolFreqBounds">
                <function>sisotoolFreqBounds</function>
              </link>
            </td>

            <td>computes default frequency range for frequency
            responses.</td>
          </tr>

          <tr>
            <td>
              <link linkend="freqconv">
                <function>freqconv</function>
              </link>
            </td>

            <td>is used to convert frequency units.</td>
          </tr>

          <tr>
            <td>
              <link linkend="phaseconv">
                <function>phaseconv</function>
              </link>
            </td>

            <td>is used to convert phase units.</td>
          </tr>

          <tr>
            <td>
              <link linkend="magconv">
                <function>magconv</function>
              </link>
            </td>

            <td>is used to convert magnitude units.</td>
          </tr>

          <tr>
            <td>
              <link linkend="scaleNicholsChart">
                <function>scaleNicholsChart</function>
              </link>
            </td>

            <td>is used to change the axes units of a drawn Nichols
            chart.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolGetEditorData">
                <function>sisotoolGetEditorData</function>
              </link>
            </td>

            <td>retrieves the <link linkend="sisotoolEditorData">editor data
            structure</link> out of the specified editor axes
            user_data.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolCharacteristicsTypes">
                <function>sisotoolCharacteristicsTypes</function>
              </link>
            </td>

            <td>returns the available characteristics for the specified
            analyser sub window.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolManageCharacteristics">
                <function>sisotoolManageCharacteristics</function>
              </link>
            </td>

            <td>creates, updates or delete the characteristics graphic
            entities of the specified editor.</td>
          </tr>

          <tr>
            <td>
              <link linkend="sisotoolSetEditorData">
                <function>sisotoolSetEditorData</function>
              </link>
            </td>

            <td>stores the <link linkend="sisotoolEditorData">editor data
            structure</link> in the specified editor axes user_data.</td>
          </tr>
    </informaltable>
  </refsection>
</refentry>
