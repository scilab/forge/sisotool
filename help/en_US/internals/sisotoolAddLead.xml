<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolAddLead" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>sisotoolAddLead</refname>

    <refpurpose>handles the graphical localization by the user and the
    addition of a lead filter in the current compensator block.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Synopsis</title>

    <synopsis>sisotoolAddLead(win)</synopsis>
  </refsection>

  <refsection>
    <title>Arguments</title>

    <variablelist>
      <varlistentry>
        <term>win</term>

        <listitem>
          <para>The figure id of the Editors window.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function waits for a user left click in the current axes (set
    by <link linkend="sisotoolEditorsEvents">sisotoolEditorsEvents</link>) to
    get the lead pole localization and finally updates the <link
    linkend="sisotoolSessionData">session data structure</link>.</para>

    <para>the lead's zero is assumed to be equal to 2/3 of the pole.</para>
  </refsection>

  <refsection>
    <title>Called functions</title>

    <informaltable border="1">
      <tr>
        <td><link linkend="getPointInSubwin">
        <function>getPointInSubwin</function> </link></td>

        <td>waits for a user click in the current sub window and returns the
        selected point coordinates.</td>
      </tr>

      <tr>
        <td><link linkend="sisotoolGetSession">
        <function>sisotoolGetSession</function> </link></td>

        <td>retrieves the <link linkend="sisotoolSessionData">session data
        structure</link> from the editors window user_data field.</td>
      </tr>

      <tr>
        <td><link linkend="sisotoolSetSession">
        <function>sisotoolSetSession</function> </link></td>

        <td>stores the <link linkend="sisotoolSessionData">session data
        structure</link> in the editors window user_data field.</td>
      </tr>

      <tr>
        <td><link linkend="sisotoolAddHistory">
        <function>sisotoolAddHistory</function> </link></td>

        <td>adds a new change in the changes history.</td>
      </tr>

      <tr>
        <td><link linkend="sisotoolSetCElement">
        <function>sisotoolSetCElement</function> </link></td>

        <td>updates the specified compensator element.</td>
      </tr>

      <tr>
        <td><link linkend="sisotoolRedraw">
        <function>sisotoolRedraw</function> </link></td>

        <td>updates the all the opened GUIs.</td>
      </tr>

      <tr>
        <td><link linkend="sisotoolUpdateOptimGui">
        <function>sisotoolUpdateOptimGui</function> </link></td>

        <td>updates the optimization GUI according to the compensator or
        requirements changes.</td>
      </tr>
    </informaltable>
  </refsection>
</refentry>
