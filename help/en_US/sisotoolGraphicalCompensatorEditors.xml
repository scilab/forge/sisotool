<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xmlns="http://docbook.org/ns/docbook" 
          xmlns:xlink="http://www.w3.org/1999/xlink" 
          xmlns:svg="http://www.w3.org/2000/svg"  
          xmlns:mml="http://www.w3.org/1998/Math/MathML" 
          xmlns:db="http://docbook.org/ns/docbook" 
          xmlns:scilab="http://www.scilab.org" 
          xml:lang="en" xml:id="sisotoolGraphicalCompensatorEditors">
  <refnamediv>
    <refname>Graphical compensator editors</refname>
    <refpurpose>Interactive graphical compensator editor with dynamical display of the
    root locus, Bode or  Nichols plots</refpurpose>
  </refnamediv>
  <refsection>
    <title>Description</title>
    <para>
      This is the master window of the sisotool function. It consist
      of a canvas, a menubar and a toolbar. The canvas may display the
      selected editors as shown in the figure below
    </para>
    <para>
      <mediaobject>
        <imageobject>
          <imagedata fileref="../images/Editors.png" align="center"/>
        </imageobject>
      </mediaobject>
      A view of the editors window with all the available editors the
      system is defined by the default architecture, the plant
      <latex>P=\frac{1}{s(s+5)(s+2)}</latex>, and the compensator
      <latex>C=64\frac{s+175439}{s+8}</latex>, the prefilter is a
      notch filter
      <latex>F=\frac{98696-14.336+s^{2}}{98696-453.338s+s^{2}}</latex>
      which has  cutoff frequency at 50Hz and a width equal to 30dB.
    </para>
  
    <refsection>
    <title>Available editors</title>
    <para> 
      The choice of the editors to be displayed can be made using the  <link
      linkend="sisotoolEditorsSelector">editors selector</link>.
    </para>
    <itemizedlist>
      <listitem>
        <para>
          The <link linkend="sisotoolRootLocusEditor">Evans root locus
          editor</link> of the open loop system,
        </para>
      </listitem>
      <listitem>
        <para>
          the <link linkend="sisotoolOpenLoopBodeEditor">open loop Bode plot
          editor</link> of the open loop system,
        </para>
      </listitem>
      <listitem>
        <para>
          the <link linkend="sisotoolOpenLoopNicholsEditor">Nichols chart
          editor</link> of the open loop system,
        </para>
      </listitem>
      <listitem>
        <para>
          the <link linkend="sisotoolClosedLoopBodeEditor">Closed loop
          Bode plot editor</link> of a selection of closed loop transfer
          functions.
        </para>
      </listitem>
    </itemizedlist>
    
    <para>
      By default  the canvas opens with the root locus editors.
    </para>
    </refsection>
    <tip>
      <para>
        The open loop transfer functions correspond to the given system
        where the connector issued output of the compensator block is
        cut the upstream part becomes the open loop output while the
        downstream part becomes the open loop input.
      </para>
    </tip>
  </refsection>
 <refsection>
      <title>Layout of sub windows </title>
      <para>The sub windows are arranged in the following way according to their number <literal>n</literal>:</para>
      <itemizedlist>
      <listitem>
        <para>Case <literal>n</literal>=1</para>

        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../images/case1.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>

      <listitem>
        <para>Case <literal>n</literal>=2</para>
        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../images/case2.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>

      <listitem>
        <para>Case <literal>n</literal>=3</para>

        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../images/case3.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>

      <listitem>
        <para>Case <literal>n</literal>=4</para>

        <inlinemediaobject>
          <para>
            <imageobject>
              <imagedata fileref="../images/case4.png"/>
            </imageobject>
          </para>
        </inlinemediaobject>
      </listitem>

      <listitem>
        <para>Case <literal>n</literal>=5</para>

        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../images/case5.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>

      <listitem>
        <para>Case <literal>n</literal>=6</para>

        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../images/case6.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>
    </itemizedlist>

    <para>In case of bode plots the corresponding area is then split in two
    parts.</para>
    </refsection>
</refentry>  
