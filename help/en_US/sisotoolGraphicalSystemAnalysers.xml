<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2016 - INRIA - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xml:id="sisotoolGraphicalSystemAnalysers" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:scilab="http://www.scilab.org"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>Graphical system analysers</refname>

    <refpurpose>Graphical system analysers</refpurpose>
  </refnamediv>

  <refsection>
    <title>Description</title>

    <para>This is the window dedicated to analyser plots. It consist of a
    canvas, a menubar and a toolbar.</para>
     

    <para>The canvas may display the selected analysers (see <link
    linkend="sisotoolAnalyserSelector">analysers selector GUI</link>) in sub
    windows:</para>

    <itemizedlist>
      <listitem>
        <para>The <link linkend="sisotoolStepResponseAnalyser">step responses
        analysers</link></para>
      </listitem>

      <listitem>
        <para>the <link linkend="sisotoolImpulseResponseAnalyser">impulse
        response analyser</link></para>
      </listitem>

      <listitem>
        <para>the <link linkend="sisotoolBodeAnalyser">Bode plot frequency
        response analyser</link> functions,</para>
      </listitem>

      <listitem>
        <para>the <link linkend="sisotoolNicholsAnalyser"> Nichols plot
        frequency response analyser</link></para>
      </listitem>

      <listitem>
        <para>the <link linkend="sisotoolNyquistAnalyser"> Nyquist plot
        frequency response analyser</link></para>
      </listitem>

      <listitem>
        <para>the <link linkend="sisotoolPZAnalyser"> pole zero map
        analyser</link></para>
      </listitem>
    </itemizedlist>

     All these analysers can be applied to one or more specified transfer functions in the 

    <link linkend="sisotoolAnalyserSelector">analysers selector GUI</link>. 
    <refsection>
      <title>Analysers window view</title>
    </refsection>
   <refsection>
      <title>Layout of sub windows </title>
      <para>The sub windows are arranged in the following way according to their number <literal>n</literal>:</para>
      <itemizedlist>
      <listitem>
        <para>Case <literal>n</literal>=1</para>

        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../images/case1.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>

      <listitem>
        <para>Case <literal>n</literal>=2</para>
        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../images/case2.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>

      <listitem>
        <para>Case <literal>n</literal>=3</para>

        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../images/case3.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>

      <listitem>
        <para>Case <literal>n</literal>=4</para>

        <inlinemediaobject>
          <para>
            <imageobject>
              <imagedata fileref="../images/case4.png"/>
            </imageobject>
          </para>
        </inlinemediaobject>
      </listitem>

      <listitem>
        <para>Case <literal>n</literal>=5</para>

        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../images/case5.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>

      <listitem>
        <para>Case <literal>n</literal>=6</para>

        <para><inlinemediaobject>
            <imageobject>
              <imagedata fileref="../images/case6.png"/>
            </imageobject>
          </inlinemediaobject></para>
      </listitem>
    </itemizedlist>

    <para>In case of bode plots the corresponding area is then split in two
    parts.</para>
    </refsection>
  </refsection>
</refentry>
