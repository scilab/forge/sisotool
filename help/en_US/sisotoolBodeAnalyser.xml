<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of sisotool module
 * Copyright (C) 2017 - Serge Steer
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * pursuant to article 5.3.4 of the CeCILL v.2.1.
 * For more information, see the COPYING file which you should have received
 * along with this program.
-->
<refentry xmlns="http://docbook.org/ns/docbook" 
          xmlns:xlink="http://www.w3.org/1999/xlink" 
          xmlns:svg="http://www.w3.org/2000/svg"  
          xmlns:mml="http://www.w3.org/1998/Math/MathML" 
          xmlns:db="http://docbook.org/ns/docbook" 
          xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="sisotoolBodeAnalyser">
  <refnamediv>
    <refname>Bode frequency response analyser</refname>
    <refpurpose>Displays the Bode plots  of the selected
    transfer function together with the user requirements and main
    characteristics.</refpurpose>
  </refnamediv>
  <refsection>
    <title>Description</title>
    <para>
     
    </para>
  </refsection>
 <refsection id="BodeAnalyserView">
      <title>Analyser view</title> 
      <para>
        <mediaobject>
          <imageobject>
            <imagedata align="center" fileref="../images/BodeAnalyser.svg"/>
          </imageobject>
      </mediaobject>
      <!-- G=syslin("c",1/(%s*(10+7*%s+%s^2)));C=syslin("c",(2+%s)/(13+4*%s+%s^2));F=2; -->
      This example represents the open loop Bode plot for the system given by the plant
      <latex>G=\frac{1}{10s+7s^{2}+s^{3}}, </latex> the compensator
      <latex>C=\frac{2+s}{13+4s+s^{2}}</latex> and the sensor
      <latex>H=0.1</latex> . The "Min stability margins" characteristics
      (see <link linkref="bodeCharacteristics">below</link>)
      </para>
    </refsection>
 <refsection>
    <title>Description</title>
    <para>
      This sub window may displays the bode plots of the selected transfer
    functions together with the user requirements and main
    characteristics.
    </para>
    <para>
      The choice of transfer function can be made either in the <link
      linkend="sisotoolAnalyserSelector">graphical analyser
      selector</link> or throught the <link
      linkend="sisotoolAnalyserContextualMenus">contextual
      menu</link>.
    </para>
    <refsection id="bodeCharacteristics">
    <title>Characteristics</title>
    <para>
      The choice of characteristics to be displayed can be made
      throught the <link
      linkend="sisotoolAnalyserContextualMenus">contextual
      menu</link>.
    </para>
    <para>
      The available characteristics for the bode responses are:
    </para>

    <variablelist>
          <varlistentry>
            <term>Peak response</term>
            <listitem>
              <para>This charateristic is visualized with a dot mark
              located at the maximum value of gain plot and
              horizontal and vertical extension lines issued from this
              point.</para>
            </listitem>
          </varlistentry>
          <varlistentry>
            <term>Min stability margins</term>
            <listitem>
              <para>This characteristics displays (if they exist) the
              worst case (minimum) <link
              linkend="scilab.help/g_margin">gain margin</link> and
              <link linkend="scilab.help/p_margin">phase margin</link>
              (see the <link linkend="BodeAnalyserView">figure</link>
              above).</para>
              <para>
                The gain margin is visualized with square mark on the
                gain plot curve and a thick vertical segment located
                at the first frequency where the phase plot crosses the
                -180° value. An extension line reports the 0dB value.
              </para>
              <para>
                The phase margin is visualized with square mark on the
                phase plot curve and a thick vertical segment located
                at the first frequency where the gain plot crosses the
                0dB value. An extension line reports the -180° value.
              </para>
            </listitem>
          </varlistentry>
          <varlistentry>
            <term>All stability margins</term>
            <listitem>
              <para>This characteristics displays (it they exist) the
              gain and phase margins.</para>
              <note>
                <para>
                  the gain margins is characterized by all points of the
                  frequency responses where the phase crosses
                  -180°. The phase margins is characterized by all points of
                  the frequency response where the modulus crosses
                  0dB.
                </para>
              </note>
            </listitem>
          </varlistentry>

        </variablelist>
        <note>
          <para> The selected characteristics are displayed for all
          displayed transfer functions.  The marks and extension lines
          inherits the response curve color.</para>
        </note>
        <tip>
          <para>
            if one clicks on a mark, a datatip is displayed which gives
            the transfert function name and numerical values related to
            the characteristic. A second click hides the datatip.
             <mediaobject>
              <imageobject>
                <imagedata align="center" fileref="../images/bodeAnalyserTip.svg"/>
              </imageobject>
            </mediaobject>
          </para>
        </tip>
    </refsection>
    <refsection>
      <title>Requirements</title>
      <para>
      The choice of requirements to be set is made
      throught the <link
      linkend="sisotoolAnalyserContextualMenus">contextual
      menu</link>.</para>
      <para>
        Requirements defines areas where the step response should not
        enter. These areas are painted in pink.
      </para>
      <para>
        The available requirements for the gain plot are:
      </para>

    <variablelist>
          <varlistentry>
            <term>Upper gain limit</term>
            <listitem>
              <para>Selecting this requirement in the contextual menu
              opens a GUI which allows to define the upper bound limit
              as a polyline.</para>
              <para>
                Example:
                <mediaobject>
                  <imageobject>
                    <imagedata align="center" fileref="../images/stepRequirementBound.png"/>
                  </imageobject>
                </mediaobject>
              </para>
            </listitem>
          </varlistentry>
          <varlistentry>
            <term>Lower gain limit</term>
            <listitem>
              <para>Selecting this requirement in the contextual menu
              opens a GUI similar to the one for the upper bound.</para>
            </listitem>
          </varlistentry>
          <varlistentry>
            <term>Minimum gain margin</term>
            <listitem>
              <para>Selecting this requirement in the contextual menu
              opens a GUI where the user can enter the minimum
              expected gain margin. </para>
            </listitem>
          </varlistentry>
    </variablelist>
      <para>
        The available requirements for the phase plot is:
      </para>
      <variablelist>
        <varlistentry>
            <term>Minimum phase margin</term>
            <listitem>
              <para>Selecting this requirement in the contextual menu
              opens a GUI where the user can enter the minimum
              expected phase margin.</para>
            </listitem>
          </varlistentry>
      </variablelist>

    <note>
      <para> The selected  requirements applies to all
      displayed transfer functions.  </para>
    </note>
    <para>Once a requirement has been created it is possible to modify
    or delete it.</para>
    <itemizedlist>
      <listitem>
        <para>
          using a press-drag-realease action on a requirement limit,
        </para>
      </listitem>
      <listitem>
        <para>
          calling the requirement specification GUI by a left click on a requirement limit
        </para>
      </listitem>
      <listitem>
        <para>
          using the contextual menu described above.
        </para>
      </listitem>
    </itemizedlist>


    </refsection>
  </refsection>
</refentry>
