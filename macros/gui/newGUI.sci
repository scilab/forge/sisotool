// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [fig,fig_id]=newGUI(w,h, name)
  fig_id=max(winsid())+1
  fig=scf(fig_id)
  clearMenubar(fig)
  fig.axes_size       = [w h];
  fig.background      = addcolor([0.8 0.8 0.8]);
  fig.figure_name     = name+"("+string(fig_id)+")";
  ax=fig.children;
  ax.background= fig.background ;
  fig.resize="off";
endfunction
