// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [r,v]=sisotoolCheckFreqRange()
  h=gcbo
  if execstr("v=("+h.string+")","errcatch")<>0 then
    sisotoolSetError(h,%t,_("Expression cannot be evaluated"))
    r=%f,v=[]
  elseif type(v)<>1|~isreal(v)|size(v,"*")==0 then
    sisotoolSetError(h,%t,_("Real vector expected"))
    r=%f
  elseif or(v<=0)|or(diff(v)<=0) then
    msg=msprintf(_("Positive increasing values expected"))
    sisotoolSetError(h,%t,msg)
    r=%f
  else
    sisotoolSetError(h,%f)
    r=%t
  end
endfunction
                                                                                                                       pc
