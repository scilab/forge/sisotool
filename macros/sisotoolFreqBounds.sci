// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [Fmin,Fmax]=sisotoolFreqBounds(TF)
  ff=[]
  if typeof(TF)=="state-space" then
    for l=1:size(TF,1)
      for k=1:size(TF,2)
        [z,p]=ss2zp(TF(l,k))
         ff=[ff;damp([p;z],TF.dt)/(2*%pi)];
      end
    end
  else
    for l=1:size(TF,1)
      for k=1:size(TF,2)
        z=roots(TF.num(l,k))
        p=roots(TF.den(l,k))
        ff=[ff;damp([p;z],TF.dt)/(2*%pi)];
      end
    end
  end
  ff=ff(ff<>0);
  if ff==[] then
    Fmin=0.01;
    Fmax=100;
  else
    Fmin=(10^(floor(log10(min(ff)))-2))
    Fmax=(10^(ceil (log10(max(ff)))+2))
  end
  if TF.dt<>"c" then Fmax=min(Fmax,1/(2*TF.dt));end
endfunction
