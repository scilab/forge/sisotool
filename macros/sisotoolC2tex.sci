// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function t=sisotoolC2tex(C)
 
  dtype="zpk"
  f=1000;
  G=C.gain
  t=''
  dt=C.dt
  if dt=="c" then vn="s";else vn="z";end
  Z=C.zeros(:)
  P=C.poles(:)
  num="1";den="1";
  if Z<>[] then
    num=fmt_real(Z(imag(Z)==0))+fmt_complex(Z(imag(Z)<>0))
    if num=="" then num="1";end
  end
  if P<>[] then
    den=fmt_real(P(imag(P)==0))+fmt_complex(P(imag(P)<>0))
    if den=="" then den="1";end
  end
  if Z<>[]|P<>[] then
    t=t+"\frac{"+num+"}{"+den+"}"
  end
  L=C.leadslags
  if L<>[] then
    num=fmt_real(L(1,:))
    den=fmt_real(L(2,:))
    t=t+"\frac{"+num+"}{"+den+"}"
  end
 
  N=C.notches
  if N<>[] then
    t=t+"\frac{"+fmt_complex(N(1,:))+"}{"+fmt_complex(N(2,:))+"}"
  end
  G=msprintf("%g\n",round(G*f)/f)
  if t=="" then
    t="$\scalebox{0.75}{"+G+"}$"
    //t="$\small "+G+"$"
  else
    //t="$\small "+G+"\times"+t+"$"
    t="$\scalebox{0.75}{"+G+"\times"+t+"}$"
  end

endfunction

function t=fmt_complex(Z)
//formats a real polynomial given by a set of complex roots (only one root
//for each complex pair)
  if Z<>[] then
    w0=abs(Z)
    if dtype=="zpk" then
      c1=msprintf("%+g\n",-round(f*2*real(Z))/f);
      c2=msprintf("%g\n",round(f*w0.^2)/f);     
      t=strcat("("+c2+c1+vn+"+"+vn+"^2)")
    else
      zeta=real(Z)./w0
      c1=msprintf("%+g\n",-round(f*2*zeta./w0)/f);
      c2=msprintf("%+g\n",round(f*1 ./w0.^2)/f);
      c1(c1=="+1")="+";
      c1(c1=="-1")="-";
      c2(c2=="+1")="+";
      c2(c2=="-1")="-";
      t=strcat("(1"+c1+vn+c2+vn+"^2)")
    end
  else
    t=''
  end
endfunction
function t=fmt_real(R)
   //formats a real polynomial given by a set of real roots
  t="";
  z=R(R==0);nz=size(z,'*')
  if nz==0  then
    t=""
  elseif nz==1 then
    t=vn
  else
    t=t+vn+"^"+string(size(z,'*'))
  end
  R=R(R<>0)
  if R<>[] then
    if dtype=="zpk" then
      c1=msprintf("%g\n",round(-f.*R)/f);
      t=t+strcat("("+c1+"+"+vn+")")
    else
      c1=msprintf("%+g\n",round(f./-R)/f);
      c1(c1=="+1")="+";
      c1(c1=="-1")="-";
      t=t+strcat("(1"+c1+vn+")")
    end
  end
endfunction 
