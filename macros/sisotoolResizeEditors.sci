// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolResizeEditors()
 S=sisotoolGetSession(gcbo)
 subWins=S.EditorsSettings.subWins
 handles=S.EditorsSettings.handles
 for sub=1:size(subWins,"*")
    subWin=subWins(sub);
    EditorHandle=handles(sub)
    EditorType=subWin.type
    if or(EditorType==["bode", "filter"])
      axm=EditorHandle(1)
      axp=EditorHandle(2)
      xt=axp.x_ticks;xt.labels=emptystr(xt.labels)
      axm.x_ticks=xt
      
    end
 end
endfunction
