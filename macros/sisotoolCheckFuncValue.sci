// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function r=sisotoolCheckFuncValue(lhs,rhs)
  h=gcbo  
  if execstr("v=("+h.string+")","errcatch")<>0 then
    sisotoolSetError(h,%t,_("Expression cannot be evaluated"))
    r=%f
  elseif type(v)==13 then
    vars = macrovar(v)
    r=size(vars(2),"*")==lhs&size(vars(1),"*")==rhs
    sisotoolSetError(h,~r,_("Given function has an incompatible prototype"))
  else
    r=sisotoolSetError(h,%f)
  end
 
endfunction
