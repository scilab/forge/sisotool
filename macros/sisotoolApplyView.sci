// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolApplyView()
  guiHandle=gcbo.parent;
  ud=guiHandle.user_data;
  Analysers=ud.Analysers;
  axesIndex=ud.axesIndex;
  mainH=Analysers.user_data.mainH;
  S=sisotoolGetSession(mainH);
  
  ax=gca(); //current axes has been set by sisotoolAnalysersEvents
  ad=sisotoolGetAnalyserData(ax);
  sub=ad.subWinIndex;
  if ax.tag=="gainplot" then
    //remove also the associated phase plot
    delete(S.AnalysersSettings.handles(sub));
    delete(S.AnalysersSettings.handles(sub+1));
    S.AnalysersSettings.subWins(sub+1)=[];
    S.OptimSettings.Analysers(sub+1)=null();
    S.AnalysersSettings.gridded(sub+1)=[];
    S.AnalysersSettings.refreshed(sub+1)=[];
  elseif  ax.tag=="phaseplot" then
    //remove also the associated gain plot
    sub=sub-1;
    delete(S.AnalysersSettings.handles(sub));
    delete(S.AnalysersSettings.handles(sub+1));
    S.AnalysersSettings.subWins(sub+1)=[];
    S.OptimSettings.Analysers(sub+1)=null();
    S.AnalysersSettings.gridded(sub+1)=[];
    S.AnalysersSettings.refreshed(sub+1)=[];
  else
    delete(S.AnalysersSettings.handles(sub));
  end
  
  t=convstr(gcbo.string(gcbo.value));
  if t=="bode" then
    subWins=S.AnalysersSettings.subWins;
    optSettings=S.OptimSettings.Analysers;
    subWins_new=struct();
    optSettings_new=list();
    for k=1:sub;
      subWins_new(k)=subWins(k);
      optSettings_new(k)=optSettings(k);
    end
    subWins_new(sub).type="gainplot";
    subWins_new(sub).characteristics_settings=[%f %f %f %f];
    optSettings_new(sub)=struct();
    
    subWins_new(sub+1)=subWins(sub);
    subWins_new(sub+1).type="phaseplot";
    subWins_new(sub+1).characteristics_settings=[%f %f %f %f];
    optSettings_new(sub+1)=struct();

    for k=sub+1:size(subWins,"*"),
      subWins_new(k+1)=subWins(k);
      optSettings_new(k+1)=optSettings(k);
    end
    S.AnalysersSettings.gridded=[S.AnalysersSettings.gridded(1:sub-1),...
                    %f %f,...
                    S.AnalysersSettings.gridded(sub+1:$)];
    S.AnalysersSettings.refreshed=[S.AnalysersSettings.gridded(1:sub-1),...
                    %f %f,...
                    S.AnalysersSettings.gridded(sub+1:$)];
    
    S.AnalysersSettings.subWins=subWins_new;
    S.OptimSettings.Analysers=optSettings_new;
    sub=[sub sub+1];
  else
    subWin=S.AnalysersSettings.subWins(sub);
    subWin.type=t;
    //reset characteristics_settings
    subWin.characteristics_settings=[%f %f %f %f];
    subWin.reqProps=sisotoolRequirementTypes(subWin.type);
    S.AnalysersSettings.gridded(sub)=%f;
    S.AnalysersSettings.refreshed(sub)=%f;
    S.AnalysersSettings.subWins(sub)=subWin;
    S.OptimSettings.Analysers(sub)=struct();
  end
  delete(guiHandle);
  S=sisotoolCreateAnalysers(mainH,S,sub);

  sisotoolSetSession(S,mainH);
  sisotoolRedrawAnalysers(mainH,%f,sub);
  
endfunction
