// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function scaleNicholsChart(h_grid,phaseUnit,magUnit)
  if phaseUnit=="rd"&magUnit=="dB" then
    deff("data=%scale(data)","data(:,1)=data(:,1)*%pi/180")
  elseif phaseUnit=="rd"&magUnit=="absolute" then
    deff("data=%scale(data)",["data(:,1)=data(:,1)*%pi/180"
                    "data(:,2)=10.0.^(data(:,2)/20)"])
  elseif phaseUnit<>"rd"&magUnit=="absolute" then
    deff("data=%scale(data)","data(:,2)=10.0.^(data(:,2)/20)") 
  end
  
  for k=1:size(h_grid.children,'*')
    h=h_grid.children(k);
    if h.type=="Compound" then
      for i=1:size(h.children,"*")
        h.children(i).data=%scale(h.children(i).data);
      end
    else
      h.data=%scale(h.data)
    end
  end
endfunction
