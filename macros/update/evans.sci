// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 1985 - 2016 - INRIA - Serge STEER
//
// This file is hereby licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// This file was originally licensed under the terms of the CeCILL v2.1,
// and continues to be available under such terms.
// For more information, see the COPYING file which you should have received
// along with this program.
function evans(num,den,kmax)
// Seuil maxi et mini (relatifs) de discretisation en espace
// Copyright INRIA
  
  smax=0.01;smin=smax/3;//boundaries on relative closed roots distances
                         //for 2 consecutive gain values.
  nptmax=5000 //nbre maxi de pt de discretisation en k
  
  //analyse de la liste d'appel
  
  [lhs,rhs]=argn(0)
  
  if rhs <= 0 then   // demonstration
    num=real(poly([0.1-%i 0.1+%i,-10],'s'));
    den=real(poly([-1 -2 -%i %i],'s'));
    evans(num/den,80);
    return
  end
  
  kmax_ind=0; //for error messages
  typ= typeof(num)
  if or(typ==["constant" "polynomial"]) then
    if rhs<2 then
      error(msprintf(_("%s: Wrong number of output arguments: At least %d expected.\n"),..
                     "evans",2))
    end
    if and(type(den)<>[1 2]) then
      error(msprintf(_("%s: Wrong type for argument %d: Polynomial"+...
                       " expected.\n"),"evans",2));
    end
    sys=syslin('d',num,den)
    if rhs==2 then //evans(num,den)
      kmax=0,
    else  //evans(num,den,kmax)
      kmax_ind=3;
    end

  elseif or(typ==["rational" "state-space"])  then
    if rhs>2 then
      error(msprintf(_("%s: Wrong number of output arguments: At most %d expected.\n"),..
                       "evans",2))
    end
    if rhs==2 then kmax_ind=2;kmax=den,else kmax=0,end
    sys=num
  else 
    error(msprintf(gettext("%s: Wrong type for input argument #%d: A linear dynamical system or a polynomial expected.\n"),"evans",1));
  end
  if size(sys,'*')<>1 then
    error(msprintf(gettext("%s: Wrong value for input argument #%d: Single input, single output system expected.\n"),"evans",1));
  end
  if kmax_ind<>0 then
    if type(kmax)<>1|~isreal(kmax)|kmax<0 then
       error(msprintf(gettext("%s: Wrong type for input argument #%d: A positive real expected.\n"),"evans",kmax_ind));
    end
  end
  [kk,ol_poles,ol_zeros,cl_poles,AsymptotesOrigin,AsymptotesAngles]=rlocus(sys,kmax)
  
  
  //draw the axis
  x1 =matrix(cl_poles,-1,1);
  if x1<>[] then
    xmin=min(real(x1));xmax=max(real(x1))
    ymin=min(imag(x1));ymax=max(imag(x1))
  else
    xmin=-1;xmax=+1;
    ymin=-1;ymax=+1;
  end
  dx=abs(xmax-xmin)*0.05
  dy=abs(ymax-ymin)*0.05
  if dx<1d-10, dx=0.01,end
  if dy<1d-10, dy=0.01,end
  
  legs=[],lstyle=[];lhandle=[]
  rect=[xmin-dx;ymin-dy;xmax+dx;ymax+dy];
  
  fig=gcf();
  immediate_drawing = fig.immediate_drawing;
  fig.immediate_drawing = 'off';
  a=gca();
  if a.children==[]
    a.data_bounds=[xmin-dx ymin-dy;
                   xmax+dx ymax+dy];
    a.axes_visible="on";
    a.title.text=_("Evans root locus");
    a.x_label.text=_("Real axis");
    a.y_label.text=_("Imaginary axis");
    //a.clip_state = "clipgrf";
  else //enlarge the boundaries
       a.data_bounds=[min(a.data_bounds(1,1:2),[xmin-dx ymin-dy]);
                      max(a.data_bounds(2,1:2),[xmax+dx ymax+dy])];
  end
  //plot the open loop zeros locations
  if ol_zeros<>[] then 
    xpoly(real(ol_zeros),imag(ol_zeros))
    e=gce();e.line_mode='off';e.mark_mode='on';
    e.mark_size_unit="point";e.mark_size=8;e.mark_style=9;
    legs=[legs; _("open loop zeros")]
    lhandle=[lhandle; e];
  end
  //plot the open loop poles locations
  if ol_poles<>[] then 
    xpoly(real(ol_poles(:,1)),imag(ol_poles(:,1)))
    e=gce();e.line_mode='off';e.mark_mode='on';
    e.mark_size_unit="point";e.mark_size=8;e.mark_style=2;
    legs=[legs;_("open loop poles")]
    lhandle=[lhandle; e];
  end

  dx=max(abs(xmax-xmin),abs(ymax-ymin));

  //draw the asymptotic lines
  if AsymptotesAngles<>[] then
    x1=dx*cos(AsymptotesAngles);
    y1=dx*sin(AsymptotesAngles);
    
    // if degree(den)==2,
    //   if coeff(den,2)<0 then
    //     x1=zeros(1,2),y1=x1
    //   end
    // end

    i1=real(AsymptotesOrigin);i2=imag(AsymptotesOrigin);
    xpoly(i1,i2);es=gce();es.line_style = 3
    lhandle=[lhandle;gce()];
    legs=[legs;_("asymptotic directions")]
    for i=1:size(AsymptotesAngles,'*')
      xsegs(i1+[0,x1(i)],i2+[0,y1(i)]),
      es=gce();es.clip_state = "clipgrf";es.line_style = 3;
    end,
  end

  //Evans root locus (closed loop poles depending on k)
  // assign the colors for each root locus
  [n1,n2]=size(cl_poles);
  cmap=fig.color_map;cols=1:size(cmap,1);
  if a.background==-2 then
    cols(and(cmap==1,2))=[]; //remove white
  elseif a.background==-1 then
    cols(and(cmap==0,2))=[]; //remove black
  else
    cols(a.background)=[];
  end
  cols=cols(modulo(0:n1-1,size(cols,"*"))+1);
  xpolys(real(cl_poles)',imag(cl_poles)',cols)
  e=gce();
  if cl_poles<>[] then e.children.clip_state="clipgrf";;end

  //set info for datatips
  E=gce();
  for k=1:size(E.children,"*")
    E.children(k).display_function = "formatEvansTip";
    E.children(k).display_function_data=kk
  end
  c=captions(lhandle,legs($:-1:1),"in_upper_right")
  c.background=a.background;
  fig.immediate_drawing = immediate_drawing;
endfunction
