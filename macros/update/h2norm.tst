// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2017 -  Serge Steer
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================
//
h=syslin("c",1/(1+2*%s));ref=0.5;
assert_checkalmostequal(h2norm(h),ref);
assert_checkalmostequal(h2norm(tf2ss(h)),ref);

hd=syslin(0.01,1/(1+2*%z));ref=5.773502691896;
assert_checkalmostequal(h2norm(hd),ref);
assert_checkalmostequal(h2norm(tf2ss(hd)),ref);

hd=syslin(0.01,2/(1-2*%z));ref=11.54700538379
assert_checkalmostequal(h2norm(hd),ref);
assert_checkalmostequal(h2norm(tf2ss(hd)),ref);

h=syslin("c",(%s+1)/(1+2*%s+3*%s^2));ref=0.57735026919;
assert_checkalmostequal(h2norm(h),ref);//crash de residu
assert_checkalmostequal(h2norm(tf2ss(h)),ref);

hd=dscr(h,0.01);ref=0.577350001898;
assert_checkalmostequal(h2norm(hd),ref);
assert_checkalmostequal(h2norm(ss2tf(hd)),ref);


h=syslin("c",-1/(1+2*%s+3*%s^2));ref=0.5;
assert_checkalmostequal(h2norm(h),ref);
assert_checkalmostequal(h2norm(tf2ss(h)),ref);

hd=dscr(h,0.01);ref=0.499999305559;
assert_checkalmostequal(h2norm(hd),ref);
assert_checkalmostequal(h2norm(ss2tf(hd)),ref);


h=[syslin("c",1/(1+2*%s)), syslin("c",(%s+1)/(1+2*%s+3*%s^2))];ref=0.763762615826;
assert_checkalmostequal(h2norm(h),ref);//crash de residu
assert_checkalmostequal(h2norm(tf2ss(h)),ref);
hd=dscr(h,0.01);ref=0.763762072808 ;
assert_checkalmostequal(h2norm(hd),ref);
assert_checkalmostequal(h2norm(ss2tf(hd)),ref);

