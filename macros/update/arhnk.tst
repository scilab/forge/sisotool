// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2017 -  Serge Steer
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================
//
eps=1e-10;
//state space 
H=syslin("c",[0 1;-1/3 -2/3],[0;1],[1 1]/3);

Ha=arhnk(H,1);
assert_checkalmostequal(Ha.A,-0.5773502691896);
assert_checkalmostequal(Ha.B,1.3495803810985);
assert_checkalmostequal(Ha.C,0.5843854472402);
assert_checkalmostequal(hankelsv(H-Ha)(1),hankelsv(H)(2));

Ha2=arhnk(H,2);
assert_checktrue(abs(hankelsv(H-Ha2))<eps);

Hd=dscr(H,0.01);
Hda=arhnk(Hd,1);
assert_checkalmostequal(Hda.A,0.9942541289875);
assert_checkalmostequal(Hda.B,0.0268658625892);
assert_checkalmostequal(Hda.C,0.2918012022116);
assert_checkalmostequal(hankelsv(Hd-Hda)(1),hankelsv(Hd)(2));
Hda2=arhnk(Hd,2);
assert_checktrue(abs(hankelsv(Hd-Hda2))<eps);


//transfer function
assert_checkequal(arhnk(ss2tf(H),1),arhnk(ss2tf(H),1));
assert_checkequal(arhnk(ss2tf(Hd),1),arhnk(ss2tf(Hd),1));



//mimo 
H=syslin("c",[0 1;-1/3 -2/3],[0 1;1 0],[1 1;0 2]/3);
Ha=arhnk(H,1);
assert_checkalmostequal(hankelsv(H-Ha)(1),hankelsv(H)(2));
Hd=dscr(H,0.01);
Hda=arhnk(Hd,1);
assert_checkalmostequal(hankelsv(Hd-Hda)(1),hankelsv(Hd)(2));
