// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) INRIA -
//
// Copyright (C) 2012 - 2016 - Scilab Enterprises
//
// This file is hereby licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// This file was originally licensed under the terms of the CeCILL v2.1,
// and continues to be available under such terms.
// For more information, see the COPYING file which you should have received
// along with this program.

function slm=arhnk(sl,ordre,tol)
//http://web.mit.edu/6.242/www/images/lec10_6242_2004.pdf
//AAK theorem
//K. Glover, "All Optimal Hankel Norm Approximation of Linear Multivariable Systems, and Their L∝- error Bounds," Int. J. Control, vol. 39, no. 6, pp. 1145-1193, 1984. 
    [lhs,rhs]=argn(0),
    if and(argn(2)<>[2 3])then
        error(msprintf(_("%s: Wrong number for input argument: %d or %d expected.\n"),"arhnk",2,3))
    end
    
    intype=typeof(sl)
    select intype
    case "state-space"
    case "rational"
      sl=tf2ss(sl);
    case "zpk" 
      sl=zpk2ss(sl);
    else
      error(msprintf(_("%s: Wrong type for input argument #%d: Linear dynmical system expected.\n"),"arhnk",1))
    end
    if type(ordre)<>1|~isreal(ordre)|size(ordre,"*")<>1|int(ordre)<>ordre|ordre<=0 then 
      error(msprintf(_("%s: Wrong size for input argument #%d: Positive integer expected.\n"),"arhnk",2))
    end
    if argn(2)==3 then
      if type(tol)<>1|~isreal(tol)|size(tol,"*")<>1|tol<=0 then  
        error(msprintf(_("%s: Wrong value for input argument #%d: A positive scalar expected.\n"),"arhnk",3))
      end
    else
      tol=[];
    end
    dt=sl.dt;
    if dt<>"c" then
      //use Tustin bilinear transform from discrete to continuous time
      if dt=="d"|dt==[] then dt=1;end
      sl=bilin(sl,[dt 2 -dt 2]);
    end

    [a,b,c,d]=abcd(sl);
    if max(real(spec(a))) > 0 then
        error(msprintf(_("%s: Wrong values for input argument #%d: Stable system expected.\n"),"arhnk",1));
    end
    //compute balanced realization
    wc=lyap(a',-b*b',"c");
    wo=lyap(a,-c'*c,"c");
    if tol==[] then
      [t,nn]=equil1(wc,wo);
    else 
      [t,nn]=equil1(wc,wo,tol);
    end;
    
    //remove unobservable or uncontrollable modes
    n1=nn(1);
    ti=inv(t);a=t*a*ti;b=t*b;c=c*ti;
    wc=t*wc*t';wo=ti'*wo*ti;
    
    if ordre>=n1 then
      //requested order greater than dimension of the minimal part
      a=a(1:n1,1:n1);b=b(1:n1,:);c=c(:,1:n1);
      slm=syslin("c",a,b,c,d,0*ones(n1,1)),
    else
      sigma=wc(ordre+1,ordre+1);
      r=max(n1-ordre-1,1);
      n=n1;
      sel=[1:ordre ordre+r+1:n];
      seleq=ordre+1:ordre+r;
      b2=b(seleq,:);
      c2=c(:,seleq);
      u=-c(:,seleq)*pinv(b(seleq,:)');
      a=a(sel,sel);
      b=b(sel,:);
      c=c(:,sel);
      wo=wo(sel,sel);
      wc=wc(sel,sel);
      Gamma=wc*wo-sigma*sigma*eye();
      a=Gamma\(sigma*sigma*a'+wo*a*wc-sigma*c'*u*b');
      b1=Gamma\(wo*b+sigma*c'*u);
      c=c*wc+sigma*u*b';b=b1;
      d=-sigma*u+d //d does not change hankel norm
      //
      [n,n]=size(a)
      //compute Schur ordered decomposition with stable eigenvalues first
      [u,m]=schur(a,"c")
      a=u'*a*u;b=u'*b;c=c*u;
      
      if m<n then
        //bloc diagonalize (stable /instable)
        t=sylv(a(1:m,1:m),-a(m+1:n,m+1:n),-a(1:m,m+1:n),"c");
        a=a(1:m,1:m);
        b=b(1:m,:)-t*b(m+1:n,:);
        c=c(:,1:m);
      end;
      slm=syslin("c",a,b,c,d,0*ones(m,1));
    end
    if dt<>"c" then
     //use Tustin bilinear transform from continuous to discrete time
     slm=bilin(slm,[2 -2 dt dt]);
     slm.dt=dt;
    end
    select intype
    case "rational" 
      slm=ss2tf(slm);
    case "zpk"
      slm=zpk(slm);
    end
endfunction
