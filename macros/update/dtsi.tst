// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2017 -  Serge Steer
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================
//
//siso cases
eps=1e-10;
H=syslin("c",[0,1,1;-1/3,-2/3,2;0,0,3],[0;0;1],[1 1 1]/3,1);
[Ha,Hs,Hi]=dtsi(H);
assert_checkequal(Hi,1);
assert_checkequal(Ha,syslin("c",3,1,2/3,0));
assert_checkequal(minss(H-(Ha+Hs+Hi)),syslin("c",[],[],[],0));

h=syslin("c",poly([-1/9 -4/9 -2 1],"s","c")/poly([-1 -5/3 -7/3 1],"s","c"));
[ha,hs,hi]=dtsi(h);
assert_checkequal(hi,1);
e=ha-syslin(H.dt,(2/3)/(%s-3));
assert_checkequal(coeff(e.den),1);
assert_checkalmostequal(coeff(e.num),0,eps,eps);
e=h-(ha+hs+hi);
assert_checkequal(coeff(e.den),1);
assert_checkalmostequal(coeff(e.num),0,eps,eps);


Hd=dscr(H,0.01);
[Hda,Hds,Hdi]=dtsi(Hd);
assert_checkequal(Hdi,1);
assert_checkequal(minss(Hda-syslin(Hd.dt,exp(3*Hd.dt),(exp(3*Hd.dt)-1)/3,2/3,0)),syslin(Hd.dt,[],[],[],0));
assert_checkequal(minss(H-(Hda+Hds+Hdi)),syslin(Hd.dt,[],[],[],0));

hd=ss2tf(Hd);
[hda,hds,hdi]=dtsi(hd);
assert_checkequal(hdi,1);
e=hda-ss2tf(syslin(Hd.dt,exp(3*Hd.dt),(exp(3*Hd.dt)-1)/3,2/3,0));
assert_checkequal(coeff(e.den),1);
assert_checkalmostequal(coeff(e.num),0,eps,eps);
e=hd-(hda+hds+hdi);
assert_checkequal(coeff(e.den),1);
assert_checkalmostequal(coeff(e.num),0,eps,eps);

//mimo state-space cases
H=syslin("c",[0,1,1;-1/3,-2/3,2;0,0,3],[0 1;0 0;1 1],[1 1 1;1 0 0]/3,[1 0;0 1]);
[Ha,Hs,Hi]=dtsi(H);
assert_checkalmostequal(Hi,H.D);
assert_checkalmostequal(spec(Ha.A),3);
assert_checkalmostequal(spec(Hs.A),spec(H.A(1:2,1:2)));
assert_checkequal(minss(H-(Ha+Hs+Hi)),syslin("c",[],[],[],zeros(2,2)));


Hd=dscr(H,0.01);
[Hda,Hds,Hdi]=dtsi(Hd);
assert_checkalmostequal(Hdi,Hd.D);
assert_checkalmostequal(spec(Hda.A),exp(3*Hd.dt));
assert_checkalmostequal(spec(Hds.A),exp(spec(H.A(1:2,1:2))*Hd.dt));
assert_checkequal(minss(Hd-(Hda+Hds+Hdi)),syslin(Hd.dt,[],[],[],zeros(2,2)));
