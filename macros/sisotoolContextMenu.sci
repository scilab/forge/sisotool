// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolContextMenu(ax,x,y)
//probleme : le popup n'est pas attache a la fenetre, si l'on deplace
//la fenetre il reste au meme endroit....
  win=ax.parent.figure_id
  mainH=get_figure_handle(win)
  S=sisotoolGetSession(mainH)
  EditorsSettings=S.EditorsSettings
  editorData=sisotoolGetEditorData(ax)
  [entityType,i,k,pt,loc]=sisotoolFindSelected(editorData,[x,y])
  if entityType==[] then
    reqTypes=sisotoolRequirementTypes(ax.tag)
    
    
    
    if EditorsSettings.gridded(editorData.subWinIndex) then G="✔";else G="";end
    if EditorsSettings.refreshed(editorData.subWinIndex) then R="✔";else R="";end
    componentSubMenus=list(_("Add Component"),...
                               [_("Real Pole"),"sisotoolAddRealPole("+string(win)+")"],...
                               [_("Complex Pole"),"sisotoolAddComplexPoles("+string(win)+")"],...
                               [_("Integrator"),"sisotoolAddIntegrator("+string(win)+")"],...
                               [_("Real Zero"),"sisotoolAddRealZero("+string(win)+")"],...
                               [_("Complex Zero"),"sisotoolAddComplexZeros("+string(win)+")"],...
                               [_("Differentiator"),"sisotoolAddDiff("+string(win)+")"],...
                               [_("Lead"),"sisotoolAddLead("+string(win)+")"],...
                               [_("Lag"),"sisotoolAddLag("+string(win)+")"],...
                               [_("Notch"),"sisotoolAddNotch("+string(win)+")"])
    if size(reqTypes,"*")==0 then
      listOfMenus = list(componentSubMenus,...
                          "-",...
                          [G+_("Grid"),"sisotoolToggleGrid("+string(win)+")"],...
                          [R+_("Refreshed"),"sisotoolToggleRefresh("+string(win)+")"]);
    else
      Reqs=list(_("Design Requirements"));
      for l=1:size(reqTypes,"*")
        Reqs($+1)=[reqTypes(l).name,"sisotoolRequirementsGui("+string(win)+","+string(l)+")"];
      end

      listOfMenus = list(componentSubMenus,...
                          Reqs,...
                          "-",...
                          [G+_("Grid"),"sisotoolToggleGrid("+string(win)+")"],...
                          [R+_("Refreshed"),"sisotoolToggleRefresh("+string(win)+")"]);
    end
    sisotoolCreatePopupMenu(listOfMenus) 
    
  elseif entityType=="requirements" then
    req=ax.children($).children(i)
    lab=req.tag+"     " //spaces added because popup box may be too small
    ud=ax.user_data
    subWinIndex=ud.subWinIndex
    requirementIndex=i
    argsedit=strcat([string(win),sci2exp(requirementIndex)],",")
    argsdelete=strcat([string(win),sci2exp(ax.tag),sci2exp(req.tag),...
                       sci2exp(requirementIndex),sci2exp(ud.subWinIndex)],",")
    listOfMenus = list([_("Edit")+" "+lab,"sisotoolRequirementsGui("+argsedit+")"],...
                       [_("Delete")+" "+lab,"sisotoolDeleteReq("+argsdelete+")"])
    sisotoolCreatePopupMenu(listOfMenus) 
    
  elseif or(entityType==["poles","zeros","leadslags","notches"])then
    lab=sisotoolEntityType2Label(entityType)+"     "//spaces added because popup box may be too small
    args=strcat([sci2exp(entityType),string(k)],",")
    listOfMenus = list([_("Delete")+" "+lab,"sisotoolDeleteComponent("+args+")"], ...
                       [_("Edit")+" "+lab,"sisotoolEditComponent("+args+")"])
    sisotoolCreatePopupMenu(listOfMenus) 
  end
endfunction
