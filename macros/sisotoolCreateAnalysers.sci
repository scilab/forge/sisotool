// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function  S=sisotoolCreateAnalysers(mainH,S,sel)
//(re)-create the Analysers subwindows and polyline entities 
  sisoguilib=lib(sisotoolPath()+"/macros/gui")
  
  subWins=S.AnalysersSettings.subWins;
  nsubwins=size(subWins,'*');
  if argn(2)<3 then sel=1:nsubwins;end
  if nsubwins==0 then return;end
  units=S.Preferences.units
  if type(S.Analysers)<>9 then
    cm=[0    0    1 
        0    0.5  0 
        1    0    0 
        0    0.75 0.75  
        0.75 0    0.75  
        0.75 0.75 0    
        0.25 0.25 0.25
        lightgray()  //for figure background
        [255 255 238]/255  //for datatips background (see the tip display_functions)
       [251,251,221]/255 //for requirements background
       ]; 
    [path, fname] = fileparts(S.Path)
    winHandle=figure("menubar", "none", ...
                     "toolbar", "none", ...
                     "infobar_visible", "on",  ... 
                     "figure_name",msprintf(_("%s: Control design Analysers (%%d)"),fname),...
                     "color_map",cm,...
                     "background",8,...
                     "default_axes","off",...
                     "event_handler","sisotoolAnalysersEvents",...
                     "event_handler_enable", "off",...
                     "visible","on",...
                     "icon",sisotoolPath()+"/icons/sisotool32.png",...
                     "closerequestfcn","sisotoolGuiClose",...
                     "tag","Analysers",...
                     "user_data",struct("mainH",mainH,"type","Analysers"));
    
    sisotoolAnalysersMenuBar(winHandle)
    sisotoolToolBar(winHandle)
    S.Analysers=winHandle
  else
    winHandle=S.Analysers
     if argn(2)<3 then  
       delete(winHandle.children(winHandle.children.type=="Axes"))
     end
    //TBD ne pas détruire mais repositionner 
  end
  scf(winHandle) //for color_map
  winHandle.immediate_drawing="off"
  sys=S.Designs;
  plants=sisotoolGetArchiSpecs(sys.archi,"plant");
  dt=sys(plants(1)).dt
  [wsplit,wmargin]=sisotoolSubwinSettings(list2vec(subWins.type));
  AnalysersHandles=list();
  
  for sub=sel
    subWin=subWins(sub);

    TF_names=subWin.transfer_names
    n=size(TF_names,"*")
    loci=[];
    select subWin.type //type names are defined in sisotoolAnalysersGui and sisotoolSelectView
    case "step"
      ax=newaxes(winHandle);
      ax.tag="step";
      ad=sisotoolNewAnalyserData(sub);
      ad.reqTypes=sisotoolRequirementTypes(ax.tag)
      ax.axes_bounds=wsplit(sub,:)
      ax.margins=wmargin(sub,:)
      ax.axes_visible="on";
      ax.clip_state='clipgrf';
      ax.background=-2;
      ax.tight_limits = ["on","on","off"];
      ax.x_label.text=msprintf(_("Time (%s)"),units.time);
      ax.y_label.text=_("Amplitude");
      ax.title.text=_("Step responses");
      rtyps=list2vec(ad.reqTypes.type);
      ad.requirements=sisoCreateRequirements(rtyps,ones(rtyps));
      sisotoolSetAnalyserData(ad,ax);
      
    case "impulse"
      ax=newaxes(winHandle);
      ax.tag="impulse";
      ad=sisotoolNewAnalyserData(sub);
      ad.reqTypes=sisotoolRequirementTypes(ax.tag)
      ax.axes_bounds=wsplit(sub,:)
      ax.margins=wmargin(sub,:)
      ax.axes_visible="on";
      ax.clip_state='clipgrf';
      ax.background=-2;
      ax.x_label.text=msprintf(_("Time (%s)"),units.time);
      ax.y_label.text=_("Amplitude");
      ax.title.text=_("Impulse responses");
     
      rtyps=list2vec(ad.reqTypes.type);
      ad.requirements=sisoCreateRequirements(rtyps,ones(rtyps));
      sisotoolSetAnalyserData(ad,ax);

    case "gainplot"
      //Gain plot
      axm=newaxes(winHandle);
      axm.tag="gainplot";
      ad=sisotoolNewAnalyserData(sub);
      ad.reqTypes=sisotoolRequirementTypes(axm.tag)
 
      axm.title.text=_("Bode diagram"),
      axm.axes_bounds=wsplit(sub,:);
      axm.margins=wmargin(sub,:);
      axm.axes_visible="on";
      axm.clip_state='clipgrf';
      axm.background=-2;
      axm.x_label.text='';
      axm.y_label.text=msprintf(_("Magnitude (%s)"),units.magnitude);
      axm.data_bounds=[0.1 -10;100 10];
      axm.log_flags = "lnn";
      axm.ticks_format(1)=" "; //Bug: ignored in log mode (bug 14078)
      axm.tight_limits(2)="on";
      rtyps=list2vec(ad.reqTypes.type)
      ad.requirements=sisoCreateRequirements(rtyps,[1 1 2]);
      sisotoolSetAnalyserData(ad,axm); 
      ax=axm;     
    case "phaseplot"
      v180=phaseconv(_("degrees"),units.phase,180);
      axp=newaxes(winHandle);
      axp.tag="phaseplot";
      ad=sisotoolNewAnalyserData(sub);
      ad.reqTypes=sisotoolRequirementTypes(axp.tag);
      axp.axes_visible="on";
      axp.axes_bounds=wsplit(sub,:);
      axp.margins=wmargin(sub,:);
      axp.background=-2;
      axp.clip_state='clipgrf';
      axp.x_label.text=msprintf(_("Frequency (%s)"),units.frequency);
      axp.y_label.text=msprintf(_("Phase (%s)"),units.phase);;
      axp.data_bounds=[0.1 0;100 v180];
      axp.log_flags = "lnn";
      rtyps=list2vec(ad.reqTypes.type)
      ad.requirements=sisoCreateRequirements(rtyps,2);
      sisotoolSetAnalyserData(ad,axp); 
      ax=axp;
    
    case "nyquist" 
      ax=newaxes(winHandle);
      ax.tag="nyquist";
      ad=sisotoolNewAnalyserData(sub);
      ad.reqTypes=sisotoolRequirementTypes(ax.tag)
      ax.axes_bounds=wsplit(sub,:)
      ax.margins=wmargin(sub,:)
      ax.background=-2;
      ax.axes_visible="on";
      ax.x_label.text=_("Real part")
      ax.y_label.text=_("Imaginary part");
      ax.title.text=_("Nyquist diagram");
      ad.requirements=[]
      //main grids
      xpoly([],[]),e1=gce();e1.line_style=4;e1.clip_state="clipgrf";
      xpoly([],[]),e2=gce();e2.line_style=4;e2.clip_state="clipgrf";
      xpoly(-1,0,"marks")
      e3=gce();
      set(e3,"mark_style",1,"mark_foreground",5,"mark_size",1,"thickness",2);
      ad.mainGrid=[e1,e2,e3];
      sisotoolSetAnalyserData(ad,ax);

    case "nichols"
      ax=newaxes(winHandle);
      ax.tag="nichols";
      ad=sisotoolNewAnalyserData(sub);
      ad.reqTypes=sisotoolRequirementTypes(ax.tag)
      ax.axes_bounds=wsplit(sub,:)
      ax.margins=wmargin(sub,:)
      ax.background=-2;
      ax.axes_visible="on";
      ax.x_label.text=msprintf(_("Phase (%s)"),units.phase);
      ax.y_label.text=msprintf(_("Magnitude (%s)"),units.magnitude);
      ax.title.text=_("Nichols chart");
      rtyps=list2vec(ad.reqTypes.type)
      
      //take care, the order matters
      ad.requirements=sisoCreateRequirements(rtyps,[2 2 1 1 1]);
    
      //main grids
      xpoly([],[]),e1=gce();e1.line_style=4;e1.clip_state="clipgrf";
      //xpoly([],[]),e2=gce();e2.line_style=4;e2.clip_state="clipgrf";
      xsegs([0;0],[0;0]);e2=gce();e2.line_style=4;e2.clip_state="clipgrf";
      xpoly(phaseconv(_("degrees"),units.phase,-180),magconv("dB",units.magnitude,0),"marks")
      e3=gce();
      set(e3,"mark_style",1,"mark_foreground",5,"mark_size",1,"thickness",2);
      ad.mainGrid=[e1,e2,e3];
      
      sisotoolSetAnalyserData(ad,ax);

    case "pole/zero"
      ax=newaxes(winHandle);
      ax.tag="rlocus";
      ad=sisotoolNewAnalyserData(sub);
      ad.reqTypes=sisotoolRequirementTypes(ax.tag)
      ax.axes_bounds=wsplit(sub,:)
      ax.margins=wmargin(sub,:)
      ax.background=-2;
      ax.x_label.text=_("Real axis (s−¹)");
      ax.y_label.text=_("Imaginary axis (s−¹)");
      ax.title.text=_("Pole-Zero map");
      rtyps=list2vec(ad.reqTypes.type)
      rtyps(rtyps=="overshoot")=[] //overshoot is similar to damping with different unit
      ad.requirements=sisoCreateRequirements(rtyps,ones(rtyps));
       //Place holder for grid
      xpoly([],[])
      h=glue(gce());//h.tag="grid" 
      
      //Place Holder for mainGrid
      xpoly([],[]);h=gce();set(h,"foreground",2,"line_style",3,"clip_state","clipgrf");
      xpoly([],[]);v=gce();set(v,"foreground",2,"line_style",3,"clip_state","clipgrf");
      if S.Designs.dt<>"c" then
        //unit circle
        t=linspace(0,2*%pi,100)
        xpoly(sin(t),cos(t));c=gce();set(c,"foreground",2,"line_style","clip_state","clipgrf");
        ad.mainGrid=[h,v,c]
      else
        ad.mainGrid=[h,v]
      end
      
      sisotoolSetAnalyserData(ad,ax);
    else
      mprintf("unknown subwindow type: %s\n",subWin.type); pause
    end
    sisotoolManageAnalyserLoci(ax,subWin,dt,"create")
    ax.axes_visible="on";
    ax.clip_state="clipgrf"
    AnalysersHandles(sub)=ax
  end
  S.AnalysersSettings.handles=AnalysersHandles;
  winHandle.immediate_drawing="on"
  winHandle.axes_size=winHandle.axes_size+1; //to fix a bug (the
                                             //subwindows are not shown)
  winHandle.event_handler_enable="on"                                
endfunction

