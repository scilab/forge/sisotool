// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function e=sisoDrawNotches(notches)
  //draw the markers of the image of the Controller poles
  blue=2
  xpoly(real(notches(1,:)),imag(notches(1,:)),"marks")
  z=gce();
  z.mark_size_unit="point";
  z.mark_size=9;
  z.mark_style=2;
  z.mark_foreground=blue;
  z.thickness=2;
  xpoly(real(notches(2,:)),imag(notches(2,:)),"marks")
  p=gce();
  p.mark_size_unit="point";
  p.mark_size=7;
  p.mark_style=2;
  p.mark_foreground=blue;
  p.thickness=2;
  e=[z,p]
endfunction
