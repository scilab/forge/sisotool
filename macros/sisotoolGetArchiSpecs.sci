// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function varargout=sisotoolGetArchiSpecs(archi, sel)
//  in      : input port names
//  out     : output port names
//  tunable : tunable bloc names
//  tunable_desc : tunable block decription
//  out_of  : cutting point for open loop definitions
//  plant   : plant bloc names
//  plant_desc   : plant description
//  Nums : formal expression of transfer function numerators
//  Den : formal expression of transfer function common denominator
  load(sisotoolPath()+"/architectures/data/"+archi+".sod")
  if argn(2)==1 then 
    varargout=list(archiSpecs);
  else
    //to be done Check validity of sel
    varargout=list(archiSpecs(sel));
  end
endfunction
