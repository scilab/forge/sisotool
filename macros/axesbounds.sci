function bounds=axesbounds(ax)
//returns the true 2D axes bounds
  bounds=ax.data_bounds(:,1:2);
  if ax.tight_limits(1)<>"on"&ax.auto_ticks(1)=="on" then
    bounds(:,1)=[min(bounds(1,1),min(ax.x_ticks.locations))
                 max(bounds(2,1),max(ax.x_ticks.locations))]
  end
   if ax.tight_limits(2)<>"on"&ax.auto_ticks(2)=="on" then
    bounds(:,2)=[min(bounds(1,2),min(ax.y_ticks.locations))
                 max(bounds(2,2),max(ax.y_ticks.locations))]
  end
 
endfunction
