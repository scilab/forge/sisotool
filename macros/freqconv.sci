// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function r=freqconv(From,To,r)
  if r==[] then return;end
  from=convstr(From,"l")
  to=convstr(To,"l")
  if from==to then return;end
  choices=["hz";"rd/s";"khz";"mhz";"ghz"];
  if and(from<>choices)|and(to<>choices) then
    error(msprintf(_("%s: Conversion from %s to %s not implemented\n"),"freqconv",From,To))
  end   

  select [from,to]
  case ["hz","rd/s"]
    r=r*(2*%pi)
  case ["rd/s" "hz"]
    r=r/(2*%pi);
  case ["khz" "hz"]
    r=r*1000
  case ["mhz" "hz"]
    r=r*1e6
  case ["mhz" "khz"]
    r=r*1e3
  case ["khz" "mhz"]
    r=r*1e-3
  case ["ghz" "hz"]
    r=r*1e9
  case ["hz" "khz"]
    r=r*1e-3
  case ["hz" "mhz"]
    r=r*1e-6
  case ["hz" "ghz"]
    r=r*1e-9
  case ["khz" "rd/s"]
    r=r/(1000*2*%pi)
  case ["mhz" "rd/s"]
    r=r/(1e6*2*%pi)
  case ["ghz" "rd/s"]
    r=r/(1e9*2*%pi)
  case ["rd/s" "khz"]
    r=r/(1000*2*%pi)
  case ["rd/s" "mhz"]
    r=r/(1e6*2*%pi)
  case ["rd/s" "ghz" ]
    r=r/(1e9*2*%pi)
  end
endfunction
