// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolManageAnalyserLoci(ax,subWin,dt,op)
//creates the entities for loci and special grids. The data fields of
//these entities are left empty. They will be set by sisotoolRedrawAnalysers
// Called by sistoolCreateAnalysers and sisotoolApplyTransfer (for update)
  TF_names=subWin.transfer_names
  n=size(TF_names,"*")
  units=S.Preferences.units
  if op=="update" then
    for ka=1:size(ax,"*")
      ad=sisotoolGetAnalyserData(ax(ka));
      n_old=size(ad.loci,"*"); //same for all subwindows (bode)
      if n<n_old then
        if or(subWin.type==["step","impulse"])
          delete(ad.mainGrid(n+1:$));
          ad.mainGrid=ad.mainGrid(1:n);
        end
        delete(ad.loci(n+1:$));
        ad.loci=ad.loci(1:n);
        sisotoolSetAnalyserData(ad,ax(ka));
      end
      //update the transfer names for kept loci
      for k=1:size(ad.loci,"*")
        ad.loci(k).display_function_data.TF_name=TF_names(k);
      end
    end
  else //create
    n_old=0
  end

  select subWin.type 
  case "step"
    sca(ax);
    ad=sisotoolGetAnalyserData(ax);
    for k=n_old+1:n
      //steady state grid (one for each transfer function)
      xpoly([],[]),g=gce();g.line_style=4;g.clip_state="clipgrf";
      ad.mainGrid=[ad.mainGrid,g];
      //loci
      xpoly([],[]);p=gce();
      dfd=struct("TF_name",TF_names(k),"units",units,"tipType",[]);
      set(p,"display_function_data",dfd,...
          "display_function","sisotoolTRTips",...
          "foreground",modulo(k-1,7)+1,...
          "clip_state","clipgrf");
      ad.loci=[ad.loci p];
    end
    sisotoolSetAnalyserData(ad,ax);
    
  case "impulse"
    sca(ax);
    ad=sisotoolGetAnalyserData(ax);
    for k=n_old+1:n
      //steady state grid (one for each transfer function)
      xpoly([],[]),g=gce();g.line_style=4;g.clip_state="clipgrf";
      ad.mainGrid=[ad.mainGrid,g];
      //loci
      xpoly([],[]);p=gce();
       dfd=struct("TF_name",TF_names(k),"units",units,"tipType",[]);
      set(p,"display_function_data",dfd,...
          "display_function","sisotoolTRTips",...
          "foreground",modulo(k-1,7)+1,...
          "clip_state","clipgrf");
      ad.loci=[ad.loci p];
    end
    sisotoolSetAnalyserData(ad,ax);
    
  case "gainplot" 
    //gainplot
    sca(ax);
    ad=sisotoolGetAnalyserData(ax);
    for k=1+n_old:n
      xpoly([],[]);p=gce();
      dfd=struct("TF_name",TF_names(k),"units",units,"tipType",[]);
      set(p,"display_function_data",dfd,...
          "display_function","sisotoolBodeTips",...
          "foreground",modulo(k-1,7)+1,...
          "clip_state","clipgrf");
      ad.loci=[ad.loci p];
    end
    sisotoolSetAnalyserData(ad,ax); 
  case "phaseplot" 
    //phase plot
    sca(ax);
    ad=sisotoolGetAnalyserData(ax);
    for k=n_old+1:n
      xpoly([],[]);p=gce();
      dfd=struct("TF_name",TF_names(k),"units",units,"tipType",[]);
      set(p,"display_function_data",dfd,...
          "display_function","sisotoolBodeTips",...
          "foreground",modulo(k-1,7)+1,...
          "clip_state","clipgrf");
      ad.loci=[ad.loci p];
    end
    sisotoolSetAnalyserData(ad,ax); 
    
  case "nyquist"  
    sca(ax)
    ad=sisotoolGetAnalyserData(ax);
    for k=n_old+1:n
      xpoly([],[]);p=gce();
      dfd=struct("TF_name",TF_names(k),"freq",freq,"units",units,"tipType",[]);
      set(p,"display_function_data",dfd,...
          "display_function","sisotoolNyqTips",...
          "foreground",modulo(k-1,7)+1,...
          "clip_state","clipgrf");
      ad.loci=[ad.loci p];
    end

    sisotoolSetAnalyserData(ad,ax);
    
  case "nichols"
    sca(ax)
    ad=sisotoolGetAnalyserData(ax);
    for k=n_old+1:n
      xpoly([],[]);p=gce();
      dfd=struct("TF_name",TF_names(k),"freq",freq,"units",units,"tipType",[]);
      set(p,"display_function_data",dfd,...
          "display_function","sisotoolNicTips",...
          "foreground",modulo(k-1,7)+1,"clip_state","clipgrf");
      ad.loci=[ad.loci p];
    end
    sisotoolSetAnalyserData(ad,ax);
    
  case "pole/zero"
    sca(ax)
    ad=sisotoolGetAnalyserData(ax);
    
    //poles
    for k=n_old+1:n
      xpoly([],[],"marks");p=gce();
      dfd=struct("TF_name",TF_names(k),"pz","poles","units",units,"tipType",[]);
      set(p,"display_function_data",dfd,...
          "display_function","sisotoolPZTips",...
          "datatip_display_mode","mouseclick",...
          "mark_style",2,...
          "mark_size_unit","point",...
          "mark_size",11,...
          "mark_foreground",modulo(k-1,7)+1,...
          "mark_background",modulo(k-1,7)+1,...
          "clip_state","clipgrf",...
          "user_data",dt);
      ad.loci=[ad.loci p];
    end
    //zeros
    for k=n_old+1:n
      xpoly([],[],"marks");p=gce();
      dfd=struct("TF_name",TF_names(k),"pz","zeros","units",units,"tipType",[]);
      set(p,"display_function_data",dfd,...
          "display_function","sisotoolPZTips",...
          "datatip_display_mode","mouseclick",...
          "mark_style",9,...
          "mark_size_unit","point",...
          "mark_size",11,...
          "mark_foreground",modulo(k-1,7)+1,...
          "clip_state","clipgrf",...
          "user_data",dt);
      ad.loci=[ad.loci p];
    end
  
    sisotoolSetAnalyserData(ad,ax);
  end

endfunction
