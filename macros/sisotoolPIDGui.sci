// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolPIDGui(mainWin)
  
  if type(mainWin)==9 then
    mainH=mainWin;
  else
    mainH=get_figure_handle(mainWin); //the sisotool root window handle
  end
  S=sisotoolGetSession(mainH);
 [path, fname] = fileparts(S.Path);
  guiHandle=S.PIDGui;
  sisoguilib=lib(sisotoolPath()+"/macros/gui");
  if  type(guiHandle)==9 then 
    sisotoolSetPIDGui(guiHandle);
    return;
  end
  
  guiHandle=figure("dockable", "off", ...
                   "menubar", "none", ...
                   "toolbar", "none", ...
                   "infobar_visible", "off",  ...
                   "figure_name",msprintf(_("%s: PID Settings (%%d)"),fname),...
                   "default_axes","off",...
                   "event_handler_enable", "off",...
                   "visible","off",...
                   "resize","off",...
                   "icon",sisotoolPath()+"/icons/sisotool32.png",...
                   "closerequestfcn","sisotoolGuiClose()");
  
  S.PIDGui=guiHandle
  margin_x     = 5;      // Horizontal margin between each elements
  margin_y     = 5;      // Vertical margin between each elements
  button_w     = 140;
  button_h     = 30;
  label_h      = 20;
  label_w      = 50;
  label1_w     = 200;
  edit_w       = 100;
  list_w       = label1_w+edit_w+margin_x;
  list_h       = 100;
  listC_w      = 40;
  rbutton_w    = label_h;
  bg=lightgray();
  frame_w=max(list_w+3*margin_x,4*(2*margin_x+rbutton_w+label_w))+2*margin_x
  frameC_h=2*margin_y+3*label_h;
  framePIDType_h=2*(margin_y+label_h);
  framePIDMethod_h=4.5*label_h+5*margin_y;
  
  axes_w=frame_w+2*margin_x;
  axes_h= frameC_h+framePIDType_h+framePIDMethod_h+button_h+6*margin_y;
  guiHandle.axes_size=[axes_w,axes_h];

  //Compensator frame
  y=axes_h-margin_y-frameC_h
  frame_border=createBorder("titled",createBorder("etched"),...
                            _("Compensator"),...
                            "left","top",createBorderFont("Sans Serif",12,"bold"),"blue")
  frameCompensator=uicontrol( ...
      "parent"              , guiHandle,...
      "style"               , "frame",...
      "position"            , [margin_x y frame_w frameC_h],...
      "fontsize"            , 12,...
      "fontWeight"          , "bold",...
      "background"          , bg, ...
      "border"              , frame_border,...
      "tag"                 , "Compensator",...
      "visible"             , "on");

  //PID type frame
  y=y-margin_y-framePIDType_h
  frame_border=createBorder("titled",createBorder("etched"),...
                            _("PID type"),...
                            "left","top",...
                            createBorderFont("Sans Serif",12,"bold"),"blue")
  framePIDType=uicontrol( ...
      "parent"              , guiHandle,...
      "style"               , "frame",...
      "position"            , [margin_x y frame_w framePIDType_h],...
      "fontsize"            , 12,...
      "fontWeight"          , "bold",...
      "background"          , bg, ...
      "border"              , frame_border,...
      "tag"                 , "PIDType",...
      "visible"             , "on");

  //PID method frame
  y=y-margin_y-framePIDMethod_h
  frame_border=createBorder("titled",createBorder("etched"),...
                            _("PID method and parameters"),...
                            "left","top",...
                            createBorderFont("Sans Serif",12,"bold"),"blue")
  framePIDMethod=uicontrol( ...
      "parent"              , guiHandle,...
      "style"               , "frame",...
      "position"            , [margin_x y frame_w framePIDMethod_h],...
      "fontsize"            , 12,...
      "fontWeight"          , "bold",...
      "background"          , bg, ...
      "border"              , frame_border,...
      "tag"                 , "PIDMethod",...
      "visible"             , "on");
  
  //FrameCompensator ---------------------------------------------------
  x=margin_x
  y=frameC_h-margin_y-2*label_h;
  uicontrol( ...
      "parent"              , frameCompensator,...
      "style"               , "popupmenu",...
      "position"            , [x,y,listC_w,label_h],...
      "fontsize"            , 12,...
      "string"              , "C",...
      "value"               , 1,...
      "max"                 , 1,...
      "horizontalalignment" , "left", ...
      "BackgroundColor"     , [1 1 1], ...
      "ForegroundColor"     , [1 1 1]*0, ...
      "visible"             , "on", ...
      "callback"            , "");
  
  uicontrol( ...
      "parent"              , frameCompensator,...
      "style"               , "text",...
      "string"              , "",...
      "position"            , [x+listC_w+margin_x,y-label_h/2,frame_w-3*margin_x-listC_w,2*label_h ],...
      "fontsize"            , 12,...
      "fontWeight"          , "bold",...
      "horizontalalignment" , "left", ...
      "background"          , bg, ...
      "tag"                 , "label",...
      "visible"             , "on");
  
  //FramePIDType ---------------------------------------------------
  xr=margin_x;
  y=framePIDType_h-margin_y-1.5*label_h;
  xl=xr
  P=CreateRadio(framePIDType,xl,y,rbutton_w,rbutton_w,"sisotoolPIDCallback();","selector");
  P.tag="P";
  xl=xr+margin_x+rbutton_w;
  CreateLabel(framePIDType,xl,y,label_w,label_h,"P");
  xl=xl+margin_x+label_w
  PI=CreateRadio(framePIDType,xl,y,rbutton_w,rbutton_w,"sisotoolPIDCallback();","selector");
  PI.tag="PI";
  xl=xl+margin_x+rbutton_w
  CreateLabel(framePIDType,xl,y,label_w,label_h,"PI")
  xl=xl+margin_x+label_w
  PID=CreateRadio(framePIDType,xl,y,rbutton_w,rbutton_w,"sisotoolPIDCallback();","selector")
  PID.tag="PID";
  xl=xl+margin_x+rbutton_w
  CreateLabel(framePIDType,xl,y,label_w,label_h,"PID")

  xl=xl+margin_x+label_w
  PID=CreateRadio(framePIDType,xl,y,rbutton_w,rbutton_w,"sisotoolPIDCallback();","selector")
  PID.tag="PIDF";
  xl=xl+margin_x+rbutton_w
  CreateLabel(framePIDType,xl,y,label_w,label_h,"PIDF")

  //FramePIDMethod---------------------------------------------------
  xr=margin_x;
  y=framePIDMethod_h-margin_y-1.5*label_h;
  xl=xr; xl=xr+margin_x+label1_w
  Meth=uicontrol( ...
      "parent"              , framePIDMethod,...
      "style"               , "popupmenu",...
      "position"            , [margin_x,y,list_w,label_h],...
      "fontsize"            , 12,...
      "string"              , "",...
      "value"               , 3,...
      "max"                 , 1,...
      "horizontalalignment" , "left", ...
      "BackgroundColor"     , [1 1 1], ...
      "ForegroundColor"     , [1 1 1]*0, ...
      "visible"             , "on", ...
      "tag"                 , "methods",...
      "user_data"           , "",...
      "callback"            , "sisotoolPIDCallback();");
  //For PIDF
  y=y-margin_y-label_h
  uicontrol( ...
      "parent"              , framePIDMethod,...
      "style"               , "text",...
      "string"              , "N",...
      "position"            , [margin_x,y,label1_w,label_h],...
      "fontsize"            , 12,...
      "horizontalalignment" , "left",...
      "visible"             , "off");
  N=uicontrol( ...
      "parent"              , framePIDMethod,...
      "style"               , "edit",...
      "string"              , "10",...
      "position"            , [xl,y,edit_w,label_h],...
      "fontsize"            , 12,...
      "BackgroundColor"     ,[1 1 1],...
      "ForegroundColor"     ,[0 0 0],...
      "horizontalalignment" , "left", ...
      "callback"            , "sisotoolCheckNumValue([2 100])",...
      "visible"             , "off");
  //For PP
  // wn 
  ys=y
  y=ys-margin_y-label_h
  uicontrol( ...
      "parent"              , framePIDMethod,...
      "style"               , "text",...
      "string"              , "Natural pulsation (rd/s)",...
      "position"            , [margin_x,y,label1_w,label_h],...
      "fontsize"            , 12,...
      "horizontalalignment" , "left", ...
      "visible"             , "off");
  wn=uicontrol( ...
      "parent"              , framePIDMethod,...
      "style"               , "edit",...
      "string"              , "1",...
      "position"            , [xl,y,edit_w,label_h],...
      "fontsize"            , 12,...
      "BackgroundColor"     ,[1 1 1],...
      "ForegroundColor"     ,[0 0 0],...
      "horizontalalignment" , "left", ...
      "callback"            , "sisotoolCheckNumValue([0 %inf])",...
      "visible"             , "off");
  //zeta
  y=y-margin_y-label_h
  uicontrol( ...
      "parent"              , framePIDMethod,...
      "style"               , "text",...
      "string"              , "Damping factor",...
      "position"            , [margin_x,y,label1_w,label_h],...
      "fontsize"            , 12,...
      "horizontalalignment" , "left", ...
      "visible"             , "off");
  zeta=uicontrol( ...
      "parent"              , framePIDMethod,...
      "style"               , "edit",...
      "string"              , "0.707",...
      "position"            , [xl,y,edit_w,label_h],...
      "fontsize"            , 12,...
      "BackgroundColor"     ,[1 1 1],...
      "ForegroundColor"     ,[0 0 0],...
      "horizontalalignment" , "left", ...
      "callback"            , "sisotoolCheckNumValue([0 1])",...
      "visible"             , "off");
  //For CHR
 
  y=ys-margin_y-label_h
  w=(frame_w-3*margin_x)/2
  uicontrol( ...
      "parent"              , framePIDMethod,...
      "style"               , "popupmenu",...
      "string"              , [_("Set-point regulation");_("Disturbance rejection")],...
      "value"               , 1,...
      "position"            , [margin_x,y,w,label_h],...
      "fontsize"            , 12,...
      "horizontalalignment" , "left", ...
      "visible"             , "off");
  uicontrol( ...
      "parent"              , framePIDMethod,...
      "style"               , "popupmenu",...
      "string"              , [_("No overshoot");_("20% overshoot")],...
      "value"               , 1,...
      "position"            , [2*margin_x+w,y,w,label_h],...
      "fontsize"            , 12,...
      "BackgroundColor"     ,[1 1 1],...
      "ForegroundColor"     ,[0 0 0],...
      "horizontalalignment" , "left", ...
      "visible"             , "off");
 
  C=uicontrol( ...
      "parent"              , guiHandle,...
      "relief"              , "groove",...
      "style"               , "pushbutton",...
      "string"              , _("Compute"),...
      "position"            , [margin_x,margin_y,button_w,button_h],...
      "fontsize"            , 12,...
      "horizontalalignment" , "center", ...
      "callback"            , "sisotoolPIDTuning();");
   set(guiHandle,"user_data",struct("mainH",mainH,...
                                   "type","PIDGui","handles",[]))
   sisotoolSetPIDGui(guiHandle)
   guiHandle.visible="on";
   
endfunction
