// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolToFile()
//callback for sisotoolExportGui
  path=uigetfile("*.dat")
  if path=="" then return,end
  gui=gcbo.parent
  ud=gui.user_data
  mainH=ud.mainH
  exportAs=ud.exportAs
  exportselection=ud.exportselection
  exprs=ud.expr
  
  exportSel=find(exportselection.value==1)
  
  sys=sisotoolGetSys(ud.mainH);
  
  names=[];D=list();
  for i=exportSel
    D($+1)=evstr(exprs(i))
  end
  
  args=strcat(exportAs(exportSel),',');
  execstr('['+args+']=D(:)');
  args=strcat(""""+exportAs(exportSel)+"""",',')
  execstr("save(path, "+args+")")
endfunction
