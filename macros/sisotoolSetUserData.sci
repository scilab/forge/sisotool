// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolSetUserData(D,fig)
  if argn(2)==1 then fig=gcf(),end
  ud=fig.user_data
  ud.sisotool=D
  set(fig,"user_data",ud)
endfunction

    
