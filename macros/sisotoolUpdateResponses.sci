// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolUpdateResponses(mainH)
//TBD: renommer en sisotoolUpdate
//TBD: passer S en parametre plutot que mainH lorque la fenetre d'edition
//     aura le meme statut que la fenetre d'analyse
  S=sisotoolGetSession(mainH)
  sisotoolRedrawEditors(mainH,%t)
  sisotoolUpdateAnalysers(mainH)
  if S.CompensatorEditor<>[] then
    sisotoolSetCGUI(S.CompensatorEditor.children)
  end
  sisotoolSetLQGGui(S.LQGGui)
 endfunction
