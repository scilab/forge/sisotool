// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function  sisotoolRedraw(mainH)
  S=sisotoolGetSession(mainH)
  sisotoolRedrawEditors(mainH)
  sisotoolRedrawAnalysers(mainH)
  sisotoolRedrawReqs(mainH)  
  if S.CompensatorEditor<>[] then
    if type(S.CompensatorEditor)==10 then
      S.CompensatorEditor=[];
      sisotoolSetSession(S,mainH)
      sisotoolCreateCGUI(mainH.figure_id)
      S=sisotoolGetSession(mainH)
    end
    sisotoolSetCGUI(S.CompensatorEditor.children)
  end
  if S.LQGGui<>[] then
    sisotoolLQGGui(mainH)
  end
  if S.PIDGui<>[] then
    sisotoolPIDGui(mainH)
  end
  if S.IMCGui<>[] then
    sisotoolIMCGui(mainH)
  end
  if S.OptimGui<>[] then
    if type(S.OptimGui)==10 then
      sisotoolOptimGui(mainH)
    else
      //TBD verifier 
      sisotoolOptimGui(mainH,%t)
    end
  end

endfunction
