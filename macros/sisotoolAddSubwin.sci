// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAddSubwin(win)
  
//
  error('not yet implemented')
//TBD
  analysersHandle=get_figure_handle(win);
  ud=analysersHandle.user_data;
  mainH=ud.mainH
  S=sisotoolGetSession(mainH)
 
  [selectedView,selectedIn,selectedOut]=sisotoolAnalyserGui(S.Designs.archi)
  
  Analysers=S.AnalysersSettings.Analysers
  N=size(Analysers,'*')+1
  if N>6 then return;end
  [axes_bounds,axes_margins]=sisotoolSubwinSettings(N)
  //update curently opened subwins
  for k=1:N-1
    ax=Analysers(k).handle
    bounds=axes_bounds(k,:);
    margins=axes_margins(k,:);
    if Analysers(k).type=="bode" then
      //module
      bounds(4)=bounds(4)/2;
      margins(3)=margins(3)+2/8;
      margins(4)=0.05;
      ax(1).axes_bounds=bounds;
      ax(1).margins=margins; 
      //phase
      margins=axes_margins(k,:);
      bounds(2)=bounds(2)+bounds(4);
      margins(4)=margins(4)+2/8;
      margins(3)= 0.05;
      ax(2).axes_bounds=bounds;
      ax(2).margins=margins;
    else
      ax.axes_bounds=bounds;
      ax.margins=margins;
    end
  end
  bounds=axes_bounds($,:);
  margins=axes_margins($,:);

  ax=newaxes(analysersHandle);
 
  ax.axes_visible="off";
  ax.clip_state='clipgrf';
  ax.box="on";
  ax.axes_bounds=bounds;
  ax.margins=margins;
  //ud.N=N
  //analysersHandle.user_data=ud;
  Analysers(N).handle=ax;  
  Analysers(N).type="???"
  S.AnalysersSettings.Analysers=Analysers
  sisotoolSetSession(S,mainH)
endfunction
