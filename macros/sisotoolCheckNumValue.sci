// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [r,v]=sisotoolCheckNumValue(bounds)
  h=gcbo
  //Retrieve the OkButton handle
  guiHandle=h;
  while guiHandle.type<>"Figure" then guiHandle=guiHandle.parent;end
  if guiHandle.children(1).string==_("Cancel") then
    OkButton=guiHandle.children(2)
  else
    OkButton=guiHandle.children(3)
  end
  
  if execstr("v=("+h.string+")","errcatch")<>0 then
    sisotoolSetError(h,%t,_("Expression cannot be evaluated"))
    r=%f,v=[]
  elseif type(v)<>1|size(v,"*")<>1|~isreal(v) then
    sisotoolSetError(h,%t,_("Real scalar expected"))
    r=%f
  else
    if argn(2)<1 then
      sisotoolSetError(h,%f)
      r=%t
    else
      r=bounds(1)<=v&v<=bounds(2)
      if r then
        sisotoolSetError(h,~r)
      else
        msg=msprintf(_("Expected value must be in the interval [%g %g]"),bounds(1),bounds(2))
        sisotoolSetError(h,~r,msg)
      end
    end
  end
  OkButton.enable=r;
endfunction
