// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolIMCCompute()
  guiHandle=gcbo.parent
  ud=guiHandle.user_data
  mainH=ud.mainH
  handles=ud.handles
  CGui=handles(1);
  InputGui=handles(2);
  LambdaGui=handles(3);
  lambda=evstr(LambdaGui.string)
  out_of=CGui.string(CGui.value)
  Input=InputGui.string(InputGui.value)
  
  S=sisotoolGetSession(mainH);
  sys=S.Designs
 
  plants=sisotoolGetArchiSpecs(sys.archi,"plant");

  // Improve ArchiSpecs data to be able to distinguish plant, plant model
  // and measure instead of using the architecture name
  if sys.archi=="archi5" then
    G=zpk(sys(plants(2)));
  else
    G=zpk(sys(plants(1)));
  end
  if G.dt=="c" then
    V=zpk([],zeros(InputGui.value,1),1,"c")
  else
    V=zpk([],ones(InputGui.value,1),1,P.dt)
  end
  try
    [C1,Q]=IMC(G,lambda,G*V)
  catch
    messagebox([_("IMC algorithm failed:");lasterror()],"","error","modal")
    return
  end
  S.IMCSettings=[CGui.value,InputGui.value,lambda,0];
  if sys.archi=="archi5" then C1=Q;end
  C=sisotoolNewController(G.dt)
  Z=C1.Z{1}.';Z=Z(imag(Z)>=0);
  C.zeros=Z;
  P=C1.P{1}.';P=P(imag(P)>=0);
  C.poles=P;
  C.gain=C1.K;
  sys(out_of)=C;
  compensatorDisplay=handles(4)
  compensatorDisplay.visible="off"; //see bug report 13909
  compensatorDisplay.string=sisotoolC2tex(C)
  compensatorDisplay.visible="on";
  S.Designs=sys
  sisotoolSetSession(S,mainH)
  sisotoolRedraw(mainH);
endfunction
