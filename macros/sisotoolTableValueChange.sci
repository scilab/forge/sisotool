// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolTableValueChange()
  h=gcbo
  tableFrame=h.parent
  guiHandle=tableFrame.parent
  if guiHandle.children(1).string==_("Cancel") then
    OkButton=guiHandle.children(2)
  else
    OkButton=guiHandle.children(3)
  end
  [ok,v]=sisotoolCheckNumValue();
  if ~ok then return;end
  
  index=h.user_data;l=index(1);k=index(2)
  Inf=%inf
  
  ncols=tableFrame.user_data
  tableHandles=matrix(tableFrame.children($:-1:1),ncols+2,-1)'//the last 2 column contains the handles on + - buttons
  nrows=size(tableHandles,1)
  
  if k==1 then  //abscissae changed check for order
    msg=_("Abscissae must be in increasing order")
    if l>1 then
      if l<nrows then
        v=evstr(tableHandles(l-1:l+1,1).string)
        if v(2)<v(1)|v(2)>v(3)  then
          sisotoolSetError(h,%t,msg)
          OkButton.enable=%f;
          return;
        end
      else
         v=evstr(tableHandles(l-1:l,1).string)
         if v(2)<v(1) then
           sisotoolSetError(h,%t,msg) 
           OkButton.enable=%f;
           return
         end
      end
    else
      v=evstr(tableHandles(l:l+1,1).string)
      if v(1)>v(2) then
        sisotoolSetError(h, %t,msg) 
        OkButton.enable=%f;
        return
      end
    end
  end
  if ncols>2 then //update other values
    if l<nrows then
      v2=evstr(matrix(tableHandles(l:l+1,1:2).string,2,2))
      dx=diff(v2(:,1))
      dy=diff(v2(:,2))
      select k
      case 1 //abscissae changed
        if or(dx<0) then
          sisotoolSetError(h,%t,msg)
          OkButton.enable=%f;
          return
        end
        dv=dy./dx
        tableHandles(l,3).string=msprintf("%.3g",dv)
      case 2 //ordinate changed
        dv=dy./dx
         tableHandles(l,3).string=msprintf("%.3g",dv)
      case 3 //slope changed
        dv=evstr(tableHandles(l,3).string)
        y1=v2(2,2)-dv*dx
        tableHandles(l,2).string=msprintf("%.3g",y1)
      end
    else
      v2=evstr(matrix(tableHandles(l-1:l,1:2).string,2,2))
      select k
      case 1 //abscissae changed
        dx=diff(v2(:,1))
        if dx<0 then
          sisotoolSetError(h,%t,msg)
          OkButton.enable=%f;
          return
        end
        dv=diff(v2(:,2))./dx
        tableHandles(l-1,3).string=msprintf("%.3g",dv)
      case 2 //ordinate changed
        dx=diff(v2(:,1))
        dv=diff(v2(:,2))./dx
        tableHandles(l-1,3).string=msprintf("%.3g",dv)
      end
    end
  end
  sisotoolSetError(h,%f)
  OkButton.enable=%t;
endfunction
