// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolLoadSession(win,path)
  if type(win)==1 then
    mainH=get_figure_handle(win);
  else
    mainH=win
  end
  old_S=sisotoolGetSession(mainH)
  if old_S.History.current<> old_S.History.ref then
    if messagebox(_("System has been modified do you want to save it?"),...
                  "Sisotool message","info",[_("Yes"),_("No")],"modal")==1 then
      sisotoolSaveSession(mainH)
    end
  end
  if argn(2)<2 then
    path=uigetfile("*.siso")
    if path=="" then return,end
  end
  if execstr("load(path)","errcatch")<>0|exists("S","local")==0|typeof(S)<>"st"|S.type<>"sisotoolSession" then 
    messagebox(_("Incorrect file"),"modal")
    return
  end
  
  S=sisotoolUpdateVersion(S)
  if S==[] then return;end
  S.Path=path
  //Update Editors
  mainH.immediate_drawing="off";
  if mainH.children<>[] then
    delete(mainH.children(mainH.children.type=="Axes"))
  end
  S=sisotoolCreateEditors(mainH,S)
  sisotoolSetSession(S,mainH)
  mainH.immediate_drawing="on"
  [path, fname] = fileparts(S.Path)
  mainH.figure_name=msprintf(_("%s: Graphical compensator editors (%%d)"),fname);

 
  //Reconnect and Update EditorsSelector if it is opened
  if S.EditorsSelector<>[] then
    if old_S.EditorsSelector<>[] then //the editor selector is currently opened
      S.EditorsSelector=old_S.EditorsSelector
    end
    sisotoolEditorsGui(mainH.figure_id)
  else
    delete(old_S.EditorsSelector)
  end

  //Reconnect and Update Analysers if it is open
  if  S.Analysers<>[] then
    if  old_S.Analysers<>[] then
      S.Analysers=old_S.Analysers
      delete( S.Analysers.children(S.Analysers.children.type=="Axes"))
    end
    S=sisotoolCreateAnalysers(mainH, S)
    
    sisotoolSetSession(S,mainH)
  else
    delete(old_S.Analysers)
  end
  //Reconnect and Update Analysers Selector if it is open  
  if S.AnalysersSelector<>[] then 
    if old_S.AnalysersSelector<>[] then 
      S.AnalysersSelector=old_S.AnalysersSelector
    end
    sisotoolAnalysersGui(mainH.figure_id)
  else
    delete(old_S.AnalysersSelector)
  end
  
  //Reconnect and Update textual compensator editor if it is open
  if S.CompensatorEditor<>[] then
    if old_S.CompensatorEditor<>[] then
      S.CompensatorEditor=old_S.CompensatorEditor
    end
    sisotoolCreateCGUI(mainH.figure_id)
  else
    delete(old_S.CompensatorEditor)
  end
  
  //Reconnect and Update Architecture Selector if it is opened
  if S.ArchitectureSelector<>[] then
    if old_S.ArchitectureSelector<>[] then
      S.ArchitectureSelector=old_S.ArchitectureSelector
    else
      //S.ArchitectureSelector=sisotoolCreateArchiGUI(mainH)
      S.ArchitectureSelector=sisotoolArchiGui(mainH)
    end
    ud=S.ArchitectureSelector.user_data
    ud.meth.value=S.Designs.archi
  else
    delete(old_S.ArchitectureSelector)
  end
  
  //delete OPtimGui if it is opened
  if old_S.OptimGui<>[] then
    delete(old_S.OptimGui)
  end
  //reopen it is required

  if S.OptimGui<>[] then sisotoolOptimGui(mainH);end    
  if S.PrefGui=="opened" then
    if type(old_S.PrefGui)==9 then delete(old_S.PrefGui);end
    sisotoolPrefGui(mainH);
  end    

  sisotoolRedraw(mainH)
  sisotoolRedrawReqs(mainH)

endfunction

