// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolRedrawReqs(mainH)
  S=sisotoolGetSession(mainH)
  units=S.Preferences.units
  //Redraw editors constraints
  subWins=S.EditorsSettings.subWins
  if size(subWins,"*")>0 then
    c=mainH.children(mainH.children.type=="Axes")
    for sub=1:size(subWins,"*")
      subWin=subWins(sub);
      ax=c($+1-sub)
      reqProps=subWin.reqProps
      if ax.children<>[]&ax.children($).tag=="requirements" then
        reqHandles=ax.children($).children
        for k=1:size(reqProps,'*')
          sisotoolDrawRequirement(ax,reqProps(k).type,reqProps(k).data)
        end 
      end
    end
  end
  if S.Analysers<>[] then
    //Redraw analysers constraints if any
    if size(subWins,"*")>0 then
      subWins=S.AnalysersSettings.subWins
      if size(subWins,"*")<>0 then
        figureHandle=S.Analysers
        c=figureHandle.children(figureHandle.children.type=="Axes")
        for sub=1:size(subWins,"*")
          reqProps=subWins(sub).reqProps
          ax=c($+1-sub)
          if ax.children<>[]&ax.children($).tag=="requirements" then
            reqHandles=ax.children($).children
            for k=1:size(reqProps,'*')
              sisotoolDrawRequirement(ax,reqProps(k).type,reqProps(k).data)
            end 
          end
        end
      end
    end
  end
endfunction
