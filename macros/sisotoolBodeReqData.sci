// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function d=sisotoolBodeReqData(Type,p,bnds)
//returns the polyline data used to show the requirement constraints for
//Rootlocus
  bnds=bnds+[-1 -1;1 1],
  select Type
  case "gmax"
    d=[p(1,1) bnds(2,2);p;p($,1) bnds(2,2);p(1,1) bnds(2,2)];
  case "gmin"
      d=[p(1,1) bnds(1,2);p;p($,1) bnds(1,2);p(1,1) bnds(1,2)];
  case "pmargin" 
    mprintf("%s not yet implemented\n",Type)
    d=[];
  case "gmargin"
    mprintf("%s not yet implemented\n",Type)
   d=[];
  end
endfunction
