// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAddRemoveRow()
  l=gcbo.user_data
  op=gcbo.string
  tableFrame=gcbo.parent
  n=tableFrame.user_data;//the number of columns

  H=matrix(tableFrame.children($:-1:1),n+2,-1)'//the last 2 column contains the handles on + - buttons
  m=size(H,1);
  t=matrix(H(:,1:n).string,m,n)
  
  select op
  case "+" then //insert after current row
    c_edit=H(1,1).constraints
    c_btn=H(1,4).constraints
    callback=H(1,1).callback
    tl=t(l,:);
    tl(1)=msprintf("%.3g",evstr(tl(1))+1);
    t=[t(1:l,:);tl;t(l+1:$,:)]
    //add a row of edit at the end
   
    for k=1:n
      c_edit.grid=[k m+1 1 1];
           e=uicontrol("parent",tableFrame,"style","edit",...
                       "string","",...
                       "constraints",c_edit,...
                       "BackgroundColor",[1 1 1],...
                       "ForegroundColor",[0 0 0],...
                       "callback", callback, ...
                       "user_data",[m+1,k],...   
                       "fontsize",12);
    end
    
    
    //Add the + and - buttons

    c_btn.grid=[4 m+1 1 1];
    e=uicontrol("parent",tableFrame,"style","pushbutton",...
                "string","+",...
                "relief","groove",...
                "constraints",c_btn,...
                "BackgroundColor",[1 1 1],...
                "ForegroundColor",[0 0 0],...
                "callback", "sisotoolAddRemoveRow()", ...
                "user_data",m+1,...
                "fontsize",12);

    c_btn.grid=[5 m+1 1 1];
    e=uicontrol("parent",tableFrame,"style","pushbutton",...
                "string","-",...
                "relief","groove",...
                "constraints",c_btn,...
                "BackgroundColor",[1 1 1],...
                "ForegroundColor",[0 0 0],...
                "callback", "sisotoolAddRemoveRow()", ...
                "user_data",m+1,...
                "fontsize",12);
    //change the content of the edits
    H=matrix(tableFrame.children($:-1:1),n+2,-1)'
    for k=1:n
      for l=1:m+1
        H(l,k).string=t(l,k);
      end
    end
  
  case "-" then //delete current row
    t(l,:)=[]
    //remove the last row
    delete(H($,:))
    for k=1:n
      for l=1:m-1
        H(l,k).string=t(l,k);
      end
    end
  end
endfunction
