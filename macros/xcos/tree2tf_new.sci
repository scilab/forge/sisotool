function T = tree2tf(Net,Start,Stop,Debug)
  T=[]
  if argn(2)<4 then Debug=%f;end
  if type(Net)==10 then // file
    fd=mopen(Net);
    data=mfscanf(-1,fd,"%d %d %s\n");
    mclose(fd);
  else
    data=Net
  end
  Net=data.arcs
  Coeff_Names=data.blknames;
 

  //*** Determine Number of Coefficients in net file ***
  Number_Coeff=size(Net,1);


  //*** Determine all the first-Order loops ***
  LoopCoeffList=[];			// Initialise list of coefficients in for each loop
  LoopNodeList=[];			// Initialise list of nodes for each loop found

  for index=1:Number_Coeff; 
    // Find list of loops originating from each node
    // Get the matrix describing all the loops from at node #index
    [FoundLoops,FoundNodes]=findpaths(index,index,[],[],Net);
    LoopCoeffList=[LoopCoeffList;FoundLoops];	// Append Coefficients of loops
    LoopNodeList=[LoopNodeList;FoundNodes];	// Append nodes visited for each loop
  end

  // Remove duplicate loops
  [LoopCoeffList,LoopNodeList]=RemoveDuplicateLoops(LoopCoeffList,LoopNodeList);

  
  // *** Make a cell array of the first order paths, each row a different order ***
  // *** The first column contains the number of paths of that order
  // L(LoopOrder).NumberLoops = number of loops of this order
  // L(LoopOrder).Coeff(n) = Coefficients of nth loop
  // L(LoopOrder).Node(n)  = Nodes of nth loop

  NumberLoops=size(LoopCoeffList,1);
  C=list();N=list();
  for index=1:NumberLoops                    // Do each loop seperately
    Coeff=LoopCoeffList(index,:);            // Read coefficients for that loop
    C(index)=Coeff(1:sum(Coeff>0));          // Strip Trailing zeros and put in struct
    Node=LoopNodeList(index,:);	             // Read Node list for loop
    N(index)=[Node(1:sum(Coeff>0)),Node(1)]; // Strip trailing zeros and put in struct
  end
  L=list(struct("NumberLoops",NumberLoops,"Coeff",C,"Node",N));
  //*** Determine nth order loops ***
  n=1;      // n is the order of loops we are finding
  while %t  // Loop until an order of loop is reached that is empty
    n=n+1;  // Count which order we are on
    // Assume no loops of this order                                            
    L(n)=struct("NumberLoops",0,"Coeff",list(),"Node",list())
    // compare each first order loop with each n-1th loop. If non touching add to the 
    // two combined to the nth loop.
    for first=1:L(1).NumberLoops	        // Take each first order loop
      for second=1:L(n-1).NumberLoops		// Compare with each n-1th loop
        if ~AreTouchingLoops(L(1).Node(first),L(n-1).Node(second)) then
          // Non Touching loops found
          // Determine if loop is a duplicate
          Duplicate=%f                       // A flag to indicate loop found is duplicate(0=not dup)
          for index=1:L(n).NumberLoops       // Add this loop if it is not a duplicate entry
            if IsSameLoop([L(1).Coeff(first), L(n-1).Coeff(second)], L(n).Coeff(index)) then
              //Duplicate found
              Duplicate=%t;
            end
          end
          if ~Duplicate then                     
            // Add the loop 
            
            // Increment the number of loops of that order
            L(n).NumberLoops=L(n).NumberLoops+1;
            // For Node and Coeff structs. Append a new array describing the loop of order n found
            L(n).Coeff(L(n).NumberLoops)=[L(1).Coeff(first), L(n-1).Coeff(second)];
            L(n).Node(L(n).NumberLoops)=[L(1).Node(first), L(n-1).Node(second)];
          end
        end 
      end
    end
    
    if L(n).NumberLoops==0 then	// If no loops of order n where found, then break
      break			// There will be no loops of order n+1
    end
  end

  // Determine common Denominator symbolic expression
  Den=1;
  for order=1:size(L)-1	        
    //Add or subtract all the loops
    if modulo(order,2)==1 then
      Den=Den-SumsNotTouching(L,Coeff_Names,order);
    else
      Den=Den+SumsNotTouching(L,Coeff_Names,order);
    end
  end
  //Den=string(Den)
  Num=[]
  for k=1:size(Start,"*")
    for l=1:size(Stop,"*")
      //*** Find all the paths connecting the start and end nodes, along which ***
      //*** no node is crossed more than once:                                 *** 
      //    PathCoeffList and PathNodeList are matrices 
      //    each row lists the number of the coefficients or the nodes visited respectively. 
      //   Each row is padded with zeros to make them the same length.
      [PathCoeffList,PathNodeList]=findpaths(Start(k),Stop(l),[],[],Net);
      //*** Convert to nomenclature used by Pozars RF Book  ***
      // P(n) represents the nth path found connecting start to stop
      // P(n).Coeff is an array with a list of the coefficients passed through in order
      // P(n).Nodes is an array listing the nodes passed through in order. Including the end nodes

      // *** Make a cell array of P the different length paths ***
      NumberPaths=size(PathCoeffList,1);
      if (NumberPaths==0);
        Num(l,k)="0";
      else
        P=struct();
        for index=1:NumberPaths// Do each path separately
          Coeff=PathCoeffList(index,:); // Read Coefficients for a path
          Node=PathNodeList(index,:);	  // Read node list for a path
          P(index)=struct("Coeff",Coeff(1:sum(Coeff>0)),"Node",[Node(1:sum(Coeff>0)),Stop(l)]);
        end
        // Determine Numerator symbolic expression
//        if l==2&k==4 then pause,end
        num=0;	        // Assume Numerator is 0 to start
        for pathn=1:size(P,1) // Add Each path and related            
          v=1;
          for order=1:size(L)-1       
            // Append each order of sums of non-touching loops that  don't touch the current path
            if (modulo(order,2)==1)
              v=v-SumsNotTouching(L,Coeff_Names,order,P(pathn).Node);
            else
              v=v+SumsNotTouching(L,Coeff_Names,order,P(pathn).Node);
            end
          end
          v=CoeffToExpr(Coeff_Names,P(pathn).Coeff)*v;
          num=num+v;
        end
        T(l,k)=string(sym_div(num,Den))

      end
    end
  end
endfunction

function expr=SumsNotTouching(L,Coeff_Names,order,Pnodes)
//*************************************************************************************************
// expr=PrintSumsNotTouching(Coeff_Names,L,path,Pnodes)
// L is the array of structs containing all the first and higher order loops.
// Pnodes is the array of nodes that describing a path
//
// The function returns a symbolic expression with the sum of all the loops of order order
// that do not touch the path given by Pnodes. 
  expr=0;				
  if argn(2)==4 then
    for n=1:L(order).NumberLoops		// Look at each loop of thet order
      if ~AreTouchingLoops(Pnodes,L(order).Node(n))  then //The loop doesn't touch the path
        expr=expr+CoeffToExpr(Coeff_Names,L(order).Coeff(n))// So add its coefficients
      end
    end
  else
    for n=1:L(order).NumberLoops // Look at each loop of that order
      expr=expr+CoeffToExpr(Coeff_Names,L(order).Coeff(n))// add its coefficients
    end
  end
endfunction

function [PathUp,NodesUp]=findpaths(StartNode,StopNode,Path,Nodes,Net)
//*************************************************************************************************
//[PathUp,NodesUp]=findpaths(StartNode,StopNode,Path,Nodes,Net)
//
//Iterative function to find path between StartNode and StopNode. Net is the array with the network
//list in it. Path is the single path to date for a given route through the tree. PathUp is a
//list of all paths terminated below that node that are sucesfull.
//Nodes is the list of nodes tvaersed so far on the way down

// Determine number of coefficients in net
  NumberCoeff=size(Net,1);

  PathUp=[];
  NodesUp=[];

  // Terminate branch and return nothing if the Nodes to date contains repetitions.
  for index=1:NumberCoeff
    if ~isempty(Nodes)  // Only compare if the the Path has data in it
      if (sum(Nodes==index)>1)
        PathUp=[];
        return
      end
    end
  end

  // Terminate branch and return path if start and stop nodes are the same
  if ((StartNode==StopNode) & (length(Path>1)))
    PathUp=Path;
    NodesUp=Nodes;
    return
  end
  // Check for all branches leaving StartNode, and call another iteration for them
  for index=1:NumberCoeff
    if (StartNode==Net(index,1)) 
      // Iterate with appended coeff to path and new startnode
      //[FoundPath,FoundNodes]=findpaths(Net(index,3),StopNode,[Path,Net(index,1)],[Nodes,StartNode],Net);
      [FoundPath,FoundNodes]=findpaths(Net(index,2),StopNode,[Path,index],[Nodes,StartNode],Net);
      if ~isempty(FoundPath)  //Only append if not empty
        PathUp=[PathUp;[FoundPath,zeros(1,NumberCoeff+1-length(FoundPath))]];
        NodesUp=[NodesUp;[FoundNodes,zeros(1,NumberCoeff+1-length(FoundPath))]];
      end
    end
  end
endfunction

function Touching=AreTouchingLoops(Nodes1,Nodes2)
//*************************************************************************************************
// This function takes two arrays describing two sets of nodes visited(each padded with zeros).
// Return 1 if they are they are touching loops.

// Determine length of loop arrays with zeros removed
  Loop1Length=sum(Nodes1>0);
  Loop2Length=sum(Nodes2>0);

  for first=1:Loop1Length
    for second=1:Loop2Length
      if (Nodes1(first)==Nodes2(second))
        Touching=1;
        return;
      end
    end
  end
  Touching=0;
endfunction

function m=CoeffToExpr(Coeff_Names,Coefficients)
//*************************************************************************************************
// m=CoeffToExpr(Coefficients)
// Coefficients is an array with coefficients c1,c2..cN
  m=1;f=1
  for k=1:size(Coefficients,'*')
    c=Coeff_Names(Coefficients(k));
    if type(c)==10 then
      m=m*sym_expr(c);
    else
      f=f*c;
    end
  end
  m=f*m
endfunction

function Same=IsSameLoop(Loop1,Loop2)
//*************************************************************************************************
// This function takes two arrays describing two loops(Can be padded with zeros if desired).
// Return 1 if they are they describe the same circular loop.

// Determine length of loop arrays with zeros removed
  Loop1Length=sum(Loop1>0);		// Count all the non zero terms
  Loop2Length=sum(Loop2>0);

  //Return 0 if different length since the loops can't be the same!
  if (Loop1Length~=Loop2Length)
    Same=0;
    return
  end

  //They are the same length so see if the contain the same nodes, but in any order.

  // sort the nodes and subtract the two vectors. 
  //The resulting vector componments will all be zero, 
  //only if the lists contains the same values.

  if (sum(abs(gsort(Loop1)-gsort(Loop2)))==0)
    Same=1;				// Loops are the same
  else
    Same=0;				// Loops are different
  end
endfunction
//*************************************************************************************************
function [LoopList,NodeList]=RemoveDuplicateLoops(LoopList,NodeList);
//*************************************************************************************************
// [LoopList,NodeList]=RemoveDuplicateLoops(LoopList,NodeList)
// Returns a subset of the LoopList matrix, where each duplicate row has been removed
// This function works on the initial loops description where each loop is a row in the
// the matrix padded with zeros to make them all the same length

  NumberLoops=size(LoopList,1)

  // Compare each loop with all the loops after it. And remove its duplicates from after it.
  first=1;			// The first loop
  while first<=NumberLoops     // Loop until all the loops have been used as first loops
    
    second=first+1;		// Start the second loop to compare as being one ahead of first loop
    while second<=NumberLoops   // choose the second loop to compare as all the ones after first
      
      // Remove the extra loop found at the second loop index.
      // NOTE: A for loop was not used since the number of loops is not constant
      if IsSameLoop(LoopList(first,:),LoopList(second,:))==1 then 
        //Remove row at second loop
        LoopList=[LoopList(1:second-1,:);LoopList(second+1:NumberLoops,:)];
        NodeList=[NodeList(1:second-1,:);NodeList(second+1:NumberLoops,:)];
        NumberLoops=NumberLoops-1;       // Decrement the number o loops
      else
        second=second+1;	// Only increment second if no loop was removed
                                // Otherwise a new loop is move to second
      end
    end
    first=first+1;				// increment the first loop pointer
  end	
endfunction
