function r=xcos2TF(scs_m,out_of)
  if type(scs_m)==10 then
    importXcosDiagram(scs_m)
  end
  [tree,ok]=xcos2tree(scs_m)
 
  blktypes=tree.blktypes
  
  sel=find(blktypes=="tunable");
  tunables=[];
  for i=sel
    tunables=[tunables;
              tree.blknames(i) tree.blkdesc(i)];
  end
  
  sel=find(blktypes=="fixed");
  plants=[];
  for i=sel
    plants=[plants;
            tree.blknames(i) tree.blkdesc(i)];
  end
  i=find(tree.arcs(:,1)==0);
  inputNames=tree.blknames(i)'
  inputIndex=tree.arcs(i,2);

  o=find(tree.arcs(:,2)==0); 
  outputNames=tree.blknames(o)';
  outputIndex=tree.arcs(o);

  del=[i,o]
  tree.arcs(del,:)=[];
  for k=gsort(del,"g","d")
    tree.blknames(k)=null();
  end
  tree.blktypes(del)=[];
  tree.blkdesc(del)=[];
  
  
  CL=tree2tf(tree,inputIndex,outputIndex);

  if argn(2)==2 then
    //computing open loop transfer
    OL=[]
    for k=1:size(out_of,"*")
      arcs=tree.arcs;
       n=max(arcs)+1;
      outputIndex=[]
      for i=1:size(tree.blknames)
        if tree.blknames(i)==out_of(k) then
          outputIndex=arcs(i,2)
          break;
        end
      end
      if outputIndex==[] then
        error("requested out_of block does not exists")
      end
      
     
      OLtree=tree

      j=find(arcs(:,1)==outputIndex)

      if size(j,"*")==1 then
        OLtree.arcs(j,1)=n
      else
        //add a unit gain block connected to the downstream blocks of
        //out_of(k)
         OLtree.arcs($+1,:)=[n n+1];
         OLtree.blknames($+1)=1
         OLtree.blkdesc($+1)=""
         OLtree.blktypes($+1)="gain"
        
         //connect the downstream blocks of out_of(k) to the gain
         OLtree.arcs(j,1)=n+1
      end
      inputIndex=n
      T=tree2tf(OLtree,inputIndex,outputIndex);
      OL=[OL;T];
    end
   
    r=struct("CL",CL,...
             "out_of",out_of(:),"OL",OL,...
             "out",outputNames,"in",inputNames,...
             "tunable",tunables(:,1)',"tunable_desc",tunables(:,2)',...
             "plant",plants(:,1)',"plant_desc",plants(:,2)'),
  else
    r=struct("CL",CL,...
             "out",outputNames,"in",inputNames,...
             "tunable",tunables(:,1)',"tunable_desc",tunables(:,2)',...
             "plant",plants(:,1)',"plant_desc",plants(:,2)'),
  end
endfunction

//x=mlist("cblock",arcs,blknames')
function [tree,ok]=xcos2tree(scs_m)
  loadXcosLibs();
  if type(scs_m)==10 then
     importXcosDiagram(scs_m);
  end
  function warnBlockByUID(uid,mess)
    error(msprintf(_("A block has unconnected input port\n")))
   endfunction
  [blklst,cmat,ccmat,cor,corinv,ok,scs_m,flgcdgen,freof]=c_pass1(scs_m);
  
  if ~ok then  error("c_pass1 failed");end
  clear ccmat cor corinv scs_m flgcdgen freof
  
  blkindex=[];blknames=list();blktypes=[];blkdesc=[];
  inputindex=[];inputnames=[];
  outputindex=[];outputnames=[];
  sumindex=[];sumsigns=list()
  nblk=size(blklst);
  for k=1:size(blklst)
    blk=blklst(k);
    if type(blk.sim)==15 then
      if blk.sim(2)==5 then //sisotool special blocs
        if  blk.sim(1)=="output"  then
          outputnames=[outputnames,blk.opar(1)];
          outputindex=[outputindex,k];
          blknames($+1)=blk.opar(1);
          blkdesc=[blkdesc,""];
          blkindex=[blkindex,k];
          blktypes=[blktypes blk.sim(1)];
        elseif  blk.sim(1)=="input"  then
          inputnames=[inputnames,blk.opar(1)];
          inputindex=[inputindex,k];  
          blknames($+1)=blk.opar(1);
          blkdesc=[blkdesc,""];
          blkindex=[blkindex,k];
          blktypes=[blktypes blk.sim(1)];
        else
          blknames($+1)=blk.opar(1);
          blkdesc=[blkdesc,blk.opar(2)];
          blkindex=[blkindex,k];
          blktypes=[blktypes blk.sim(1)];
        end
      elseif blk.sim(1)=="sum" then
        sumsigns($+1)=blk.rpar;
        sumindex(1,$+1)=k;
        blknames($+1)="sum";
        blkdesc=[blkdesc,""];
        blkindex=[blkindex,k];
        blktypes=[blktypes blk.sim(1)];
      else //shoud not occur
        error(_("%s,Unexpected block %s\n"),"xcos2TF",blk.sim(1))
      end
    else//shoud not occur error("%s,Unexpected block %s\n","xcos2TF",blk.sim(1))
       error(_("%s,Unexpected block %s\n"),"xcos2TF",blk.sim(1))
    end
  end
 
  //prise en compte des signes des blocs sum en introduisant des blocs +1
  //et -1 en amont
  for i=1:size(sumindex,'*')
    ind=sumindex(i)
    li=find(cmat(:,3)==ind);//entrees du sommateur
    if size(li,"*")<>size(sumsigns(i),"*") then
      error(_("unconnected sum input port"))
    end
    //L=find(sumsigns(i)==-1);
    for j=1:size(sumsigns(i),"*")
      nblk=nblk+1;
      blkindex=[blkindex,nblk];
      blknames($+1)=sumsigns(i)(j);
      blkdesc=[blkdesc,""]
      blktypes=[blktypes,"gain"]
      
      cmat($+1,:)=[nblk 1 cmat(li(j),3:4)];
      cmat(li(j),3:4)=[nblk 1];
    end
  end
  for ind=sumindex
    //remplacement du bloc sum par un bloc "1" et connection des liens
    //d'entree sur le port 1 du bloc 1
    k=find(blkindex==ind);
    blknames(k)=1;
    li=find(cmat(:,3)==k);//connections d'entree du bloc sum
    cmat(li,4)=1;
  end
  //now all block are siso
  cmat=cmat(:,[1 3]); //block# -->block#
  arcs=dual(cmat)
 
  tree=struct("arcs",arcs,"blknames",blknames,"blkdesc",blkdesc(:),"blktypes",blktypes(:))
 
endfunction

function arcs=dual(cmat)
 //x=mlist("cblock",arcs,blknames')
   na=max(cmat);
   //one row per block
   mxn=0; arcs=zeros(na,2);
  //la sortie du bloc cmat(k,1) est connectee en l'entree du bloc cmat(k,2)
  for k=1:na
    // build the arc going from the input of bloc k to its output
    if arcs(k,1)==0 then 
      //The upstream node of the block #k has not been set:
      //find if there is a block whose output is connected to the input
      //of the block k and whose downstream node has been set
      lo=cmat(find(cmat(:,2)==k),1);
      if lo<>[] then 
        // there is at least one upstream block
        i=find(arcs(lo,2)<>0) //find those whose  downstream node has been set
        if i<>[] then
          //all the upstream block must have the same downstream node
          node_name=arcs(lo(i(1)),2)
          arcs(k,1)=node_name
          arcs(lo,2)=node_name
         else
          //the output node of the upstream block(s) has not been set 
          mxn=mxn+1;
          arcs(k,1)=mxn
          arcs(lo,2)=arcs(k,1);
        end
      else
        //there is no upstream block (block #k is an input block)
      end
    end
    if arcs(k,2)==0 then 
      //The downstream node of the block #k has not been set:
      //find if there is a block whose input is connected to the output
      //of the block k and whose upstream node has been set
      lo=cmat(find(cmat(:,1)==k),2);
      if lo<>[] then
        // there is at least one downstream block
        i=find(arcs(lo,1)<>0) //find those whose upstream node has been set
        if i<>[] then
          //all the downstream block must have the same upstream node
          node_name=arcs(lo(i(1)),1)
          arcs(k,2)=node_name
          arcs(lo,1)=node_name
        else
          //the input node of the downstream block(s) has not been set 
          mxn=mxn+1;
          arcs(k,2)=mxn
          arcs(lo,1)=arcs(k,2);
        end
      else
         //there is no downstream block (block #k is an output block)
      end
    end
 
  end
endfunction
