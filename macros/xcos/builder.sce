function main_builder()
NAME = "sisotoolxcos";
TITLE = "sisotoolxcos";
path = get_absolute_file_path("builder.sce");
[status, msg] = mkdir(path+"images/h5");
if or(status == [-2 0]) then error(msg);end
tbx_build_macros(NAME, path);
//tbx_builder_help(module_path);
tbx_build_loader(NAME, path);
tbx_build_cleaner(NAME, path);

block_Interfaces=["fixed_sym","tunable_sym","in_sym","out_sym"]
tbx_build_blocks(path,block_Interfaces,path );
tbx_build_pal_loader("sisotool",block_Interfaces,path,path);

endfunction
main_builder();
clear main_builder; // suppression de la variable main_builder
