function c=%s_i_cblock(varargin)
  if size(varargin)<>4|varargin($-1)<>[] then
    error("Not yet implemented")
  end
  c=varargin($)
  v=varargin($-1)
  n=size(definedfields(c),"*")
  for k=2:n
    v=getfield(k,c)
    i=varargin(1)
    v(i,:)=[]
    setfield(k,v,c)
  end
endfunction
