function loadxcos()
  NAME  = "sisotoolxcos";
  TITLE = "sisotoolxcos";
  
  script_path  = get_absolute_file_path("loadxcos.sce");  
  module_path = script_path 
  
  mprintf(_("Loading %s module\n"),TITLE);

  if isdef(NAME+"lib") then
    warning(msprintf(_("Module %s is already loaded"),TITLE));
    //return;
  end

  // force to load some requested libraries
  loadScicos();

  // Add blocks to the Xcos palette
  // =============================================================================
  xpal = xcosPal("sisotool");
  images_path = script_path+"images/";
  interfacefunctions =["fixed_sym","tunable_sym","in_sym","out_sym"]
  for i=1:size(interfacefunctions,"*")
    h5_instances  = ls(images_path + "h5/"  + interfacefunctions(i) + ".sod");
    if h5_instances==[] then
      error(msprintf(_("%s: block %s has not been built.\n"),"loader_pal.sce",interfacefunctions(i)))
    end
    pal_icons     = ls(images_path + "gif/" + interfacefunctions(i) + "." + ["png" "jpg" "gif"]);
    if pal_icons==[] then
      error(msprintf(_("%s: block %s has no palette icon.\n"),"loader_pal.sce",interfacefunctions(i)))
    end
    graph_icons   = ls(images_path + "svg/" + interfacefunctions(i) + "." + ["svg" "png" "jpg" "gif"]);
    if graph_icons==[] then
      error(msprintf(_("%s: block %s has no editor icon.\n"),"loader_pal.sce",interfacefunctions(i)))
    end
    //import_from_hdf5(h5_instances);
    load(h5_instances)
    xpal = xcosPalAddBlock(xpal, scs_m, pal_icons(1) , graph_icons(1));
  end
  xcosPalAdd(xpal);
  // Load and add help chapter
  // =============================================================================
  if or(getscilabmode() == ["NW";"STD"]) then
    //mprintf("\tLoad help\n");

    //add_help_chapter(TITLE, module_path+"jar/", %F);
  end

  // Load functions library and return
  // =============================================================================
  mprintf("\tLoad macros\n");
  execstr(NAME+"lib=resume(lib(module_path))")
endfunction
loadxcos()
clear loadxcos
