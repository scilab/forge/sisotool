// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [sisotoolSetSysVars,x0,bnds,g,bndsg,data]=sisotoolOptimSettings(guiHandle)
  if argn(2)<1 then 
    guiHandle=gcbo.parent
  end
  ud=guiHandle.user_data
  mainH=ud.mainH
  S=sisotoolGetSession(mainH);
  units=S.Preferences.units
  sys=S.Designs;
  OptimSettings=S.OptimSettings
  [tunables,out_of]=sisotoolGetArchiSpecs(sys.archi,["tunable","out_of"]);
 
  //create the function use to set 
  //  - the compensator according to the optimization variables
  //  - the initial value of the optimization variables
  //  - the bounds of the optimization variables
  codeset=["sys=sisotoolSetSysVars(x);";
           "  sys;"]
  x0=[]
  bnds=[];
  for tunable=tunables
    C=sys(tunable)
    optC=OptimSettings.Controller(tunable)
    codeset=[codeset
             "  C=sys."+tunable];
    F=fieldnames(C)';F(F=="dt")=[];
    ix=0;
    for i=1:size(F,'*')
      p=[];ip=[];
      sel=find(optC(F(i)).enabled)
      if sel==[] then continue;end
      initial=optC(F(i)).initial(:,sel)
      lowbound=optC(F(i)).lowbound(:,sel)
      highbound=optC(F(i)).highbound(:,sel)
      style=optC(F(i)).style(sel)
      if F(i)=="gain" then
        x0=[x0;
            optC(F(i)).initial]
        p="x("+sci2exp(ix+1)+");"
        ip=1;
        bnds=[bnds
              optC(F(i)).lowbound optC(F(i)).highbound];
        ix=ix+1;
      elseif or(F(i)==["poles","zeros"]) then
        iRI=find(style==1)//[realPart;imagPart]
        iR=iRI(find(initial(2,iRI)==0))
        if iR<>[] then
          m=size(iR,'*');
          x0=[x0;
              initial(1,iR)']
          p=[p;
             "x("+sci2exp(ix+(1:m),0)+")"];
          ip=[ip,iR]
          ix=ix+m;
          bnds=[bnds
                lowbound(1,iR)' highbound(1,iR)'];   
        end
        iC=iRI(find(initial(2,iRI)<>0));
        if iC<>[] then
          m=size(iC,"*");
          x0=[x0;
              matrix(initial(:,iC),-1,1)]; //sequence [R;I;R;I;...]
          bnds=[bnds
                matrix(lowbound(:,iC),-1,1),matrix(highbound(:,iC),-1,1)];
          R="x("+sci2exp(ix+1)+":2:"+sci2exp(ix+2*m)+")"
          I="-abs(x("+sci2exp(ix+2)+":2:"+sci2exp(ix+2*m)+"))"
          p=[p;"complex("+R+","+I+");"]
          ip=[ip,iC]
          ix=ix+2*m;
        end
        iWZ=find(style==2)//Wn zeta
        if iWZ<>[] then
          m=size(iWZ,"*");
          x0=[x0;
              matrix(initial(:,iWZ),-1,1)]; //sequence [w;zeta; w;zeta;...]
          bnds=[bnds
                matrix(lowbound(:,iWZ),-1,1),matrix(highbound(:,iWZ),-1,1)];
          
          wn="x("+sci2exp(ix+1)+":2:"+sci2exp(ix+2*m)+")";
          zeta="x("+sci2exp(ix+2)+":2:"+sci2exp(ix+2*m)+")";
          p=[p
             wn+".*complex("+zeta+",-sqrt(1-"+zeta+".^2))"];
          ip=[ip,iWZ]
          ix=ix+2*m   ;
        end
      elseif F(i)=="leadslags" then
        iZP=find(style==1) //Zero;Pole
        if iZP<>[] then
          m=size(iZP,"*")
          x0=[x0;
              matrix(initial(:,iZP),-1,1)]; //sequence [zero;pole; zero;pole;...]
          bnds=[bnds
                matrix(lowbound(:,iZP),-1,1),matrix(highbound(:,iZP),-1,1)];
          Z="x("+sci2exp(ix+(1:2:2*m),0)+")";
          P="x("+sci2exp(ix+(2:2:2*m),0)+")";
          p=[p;Z+","+P];
          ip=[ip,iZP]
          ix=ix+2*m;
        end
        iWPHI=find(style==2)  //w;phi
        if  iWPHI<>[] then 
          m=size(iWPHI ,'*');
          x0=[x0;
              matrix(initial(:,iWPHI),-1,1)]; //sequence [w;phi; w;phi;...]
          bnds=[bnds
                matrix(lowbound(:,iWPHI),-1,1),matrix(highbound(:,iWPHI),-1,1)];
          w= "-x("+sci2exp(ix+1)+":2:"+sci2exp(ix+2*m)+")";
          phi="x("+sci2exp(ix+2)+":2:"+sci2exp(ix+2*m)+")";

          codeset=[codeset;
                   "alpha"+string(i)+"=(1-sind("+phi+"))./(1+sind("+phi+"))"];
          p=[p;
             w+".*sqrt(alpha"+string(i)+"),"+ w+"./sqrt(alpha"+string(i)+");" 
            ]
          ip=[ip,iWPHI]
          ix=ix+2*m;
        end
      else //notches (w zetaz zetap)
        x0=[x0;
            matrix(initial,-1,1)]; //sequence [w;zz;zp; w;zz;zp;...]
        bnds=[bnds
              matrix(lowbound,-1,1),matrix(highbound,-1,1)];
        m=size(V,2)
        wn="x("+sci2exp(ix+(1:m),0)+")";
        zetaz="x("+sci2exp(ix+m+(1:m),0)+")";
        zetap="x("+sci2exp(ix+2*m+(1:m),0)+")";
        p=[p
           wn+".*complex("+zetaz+",-sqrt(1-"+zetaz+".^2)),"+wn+".*complex("+zetap+",-sqrt(1-"+zetap+".^2));"];
        ix=ix+3*m
        ip=sel
      end
      if size(p,'*')<> 1 then
        p(1)="C."+F(i)+"("+sci2exp(ip,0)+")=["+p(1);
        p($)=p($)+"].''"
      else
        p="C."+F(i)+"("+sci2exp(ip,0)+")="+p
      end
      codeset=[codeset;
               "  "+p]

    end
    codeset=[codeset;
             "  sys."+tunable+"=C;"]
  end

  deff(codeset(1),codeset(2:$))
  //mputl(["function "+codeset(1);codeset(2:$);"endfunction"],"/tmp/SetSysVars.sci")
  //Create the function which computes the constraints
  OL=sisotoolSys2OL(sys,out_of) 
  if typeof(OL)=="state-space" then
    cl_roots=spec(OL.A-(OL.B*OL.C)/(1+OL.D))
  else
    cl_roots=roots(OL.den+OL.num,"e")
  end
  
  head=[];
  codeg=[];
  bndsg=[];
  OL_requested=%f;

  
  EditorsSubwins=S.EditorsSettings.subWins;
  handles=S.EditorsSettings.handles;
  opts=S.OptimSettings.Editors;
  data=[];
  for sub=1:size(opts)
    if size(opts(sub),"*")==0 then continue;end
    opt=opts(sub);
    req=EditorsSubwins(sub).reqProps;
    ind1=sisotoolDefinedReqs(req)
    if ind1==[] then continue;end
    fn=fieldnames(opt);
    ind=[];
    for i=1:size(ind1,"*")
      if opt(fn(i)) then ind=[ind ind1(i)];end
    end
    if ind<>[] then 
      select EditorsSubwins(sub).type      
      case "rlocus"
        OL_requested=%t
        [head,codeg,bndsg,data]=rlocuscode(head,codeg,bndsg,data)
      case "bode"
        OL_requested=%t
        [head,codeg,bndsg,data]=bodecode(head,codeg,bndsg,data)
      case "nichols"  
        OL_requested=%t
        [head,codeg,bndsg,data]=nicholscode(head,codeg,bndsg,data)
        if codeg==[] then g=[];return;end
      case "filter"
        [head,codeg,bndsg,data]=filtercode(head,codeg,bndsg,data)
      end
      
    end
  end
  if size(EditorsSubwins,"*")<>0 then
    head=["OL=sisotoolSys2OL(sys,"+sci2exp(out_of)+");"
          head];
  end
  opts=S.OptimSettings.Analysers;
  AnalysersSubwins=S.AnalysersSettings.subWins;
  
  //generate code to compute all transfer function needed for all
  //analysers requirements evaluations
  
  TF_exprs=[];TF_sel=list();
  for sub=1:size(AnalysersSubwins,"*")
    TF_sel(sub)=[];
    opt=opts(sub); 
    if size(opt,"*")==0 then continue;end
    fn=fieldnames(opt);
    for i=1:size(fn,"*")
      if or(opt(fn(i))) then 
        TF_exprs=[TF_exprs;AnalysersSubwins(sub).expr(opt(fn(i)))];
        TF_sel(sub)=[TF_sel(sub),find(opt(fn(i)))];
      end
    end 
  end
  TF_exprs=unique(TF_exprs);
  ntf=size(TF_exprs,'*');
  if ntf<>0 then
    head=[head;
          "TF"+string(1:ntf)'+"="+TF_exprs];
  end
 
 
  for sub=1:size(AnalysersSubwins,"*")
    opt=opts(sub);
    if size(opt,"*")==0 then continue;end
    sel_tf=TF_sel(sub)
    if sel_tf==[] then continue;end

    //find active requirement in this subwindow
    req=AnalysersSubwins(sub).reqProps;
    ind1=sisotoolDefinedReqs(req); //the requirements defined in this subwindow
   
    //find the active ones
    fn=fieldnames(opt);
    sel_req=[]; 
    for i=1:size(ind1,"*")
      if or(opt(fn(i))) then 
        sel_req=[sel_req, ind1(i)];
      end
    end 
    
    if sel_tf<>[] then
      select AnalysersSubwins(sub).type  
      case "step"
        [head,codeg,bndsg,data]=stepcode(head,codeg,bndsg,data)
      case "impulse"
        [head,codeg,bndsg,data]=impulsecode(head,codeg,bndsg,data)
      case "gainplot"
        [head,codeg,bndsg,data]=analyserbodecode(head,codeg,bndsg,data)
      case "phaseplot"
        [head,codeg,bndsg,data]=analyserbodecode(head,codeg,bndsg,data)
      case "nyquist"
        //No requirements can be defined for nyquist plots
      case "nichols"
        [head,codeg,bndsg,data]=analysernicholscode(head,codeg,bndsg,data)
        if codeg==[] then g=[];return;end
      case "pole/zero"
        [head,codeg,bndsg,data]=pzcode(head,codeg,bndsg,data)
      end
    end
  end

  deff("v=g(x,x_new,data)",["sys=sisotoolSetSysVars(x);"
                    head
                    "v=[";"   "+codeg;"];"])
  mputl(["function v=g(x,x_new,data)";"sys=sisotoolSetSysVars(x);";head;"v=[";"   "+codeg;"];";"endfunction"],"/tmp/g.sci")
endfunction 

function [head,codeg,bndsg,data]=rlocuscode(head,codeg,bndsg,data)
  head=[head;
  "if typeof(OL)==""state-space"" then"
  "  cl_roots=spec(OL.A-(OL.B*OL.C)/(1+OL.D))"
  "else"
  "  cl_roots=roots(OL.den+OL.num,""e"")"
  "end"];

  for k=ind
    rk=req(k)
    select rk.type
    case "settlingtime"
      codeg=[codeg;
             "-real(cl_roots)/4"]
      bndsg=[bndsg;
             ones(cl_roots)*[0 rk.data]];
    case "damping"
      codeg=[codeg;
             "-real(cl_roots)./abs(cl_roots)"]
      bndsg=[bndsg;
             ones(cl_roots)*[rk.data 1]];
    case "wmin"
      codeg=[codeg;
             "abs(cl_roots)"]  
      bndsg=[bndsg;
             ones(cl_roots)*[rk.data %inf]];     
    case "wmax"
      codeg=[codeg;
             "abs(cl_roots)"]  
      bndsg=[bndsg;
             ones(cl_roots)*[0 rk.data]];     
    end
  end
endfunction 

function [head,codeg,bndsg,data]=gaincode(head,codeg,bndsg,data)
  db_needed=%f
  for k=ind
    rk=req(k)
    select rk.type
    case "gmin"
      db_needed=%t
      head=[head;
            "req_data_gmin="+sci2exp(rk.data,0)]
      codeg=[codeg;
             "evalPolylineRequirement(data.frq_bode,db_OL,req_data_gmin,""lower"")"];
      bndsg=[bndsg;
             -%inf,0];
    case "gmax"
      db_needed=%t
      head=[head;
            "req_data_gmax="+sci2exp(rk.data,0)]
      codeg=[codeg;
             "evalPolylineRequirement(data.frq_bode,db_OL,req_data_gmax,""upper"")"];
      bndsg=[bndsg;
             -%inf 0];
    case "gmargin"
      codeg=[codeg;
             "g_margin1(OL)"]
      bndsg=[bndsg;
             rk.data, %inf ];
    end
  end
  if db_needed then
    editorData=sisotoolGetEditorData(handles(sub)(1))
    d=editorData.loci.data
    data.frq_bode=d(:,1)
    head=[head;
          "db_OL=20*log10(abs(repfreq(OL,data.frq_bode)));"
         ]
  end
endfunction
function [head,codeg,bndsg,data]=phasecode(head,codeg,bndsg,data)
  db_needed=%f
  for k=ind
    rk=req(k)
    select rk.type
    case "pmargin"
      codeg=[codeg;
             "p_margin1(OL)"]
      bndsg=[bndsg;
             rk.data, %inf ];
     end
  end
  if db_needed then
    editorData=sisotoolGetEditorData(handles(sub)(1))
    d=editorData.loci.data
    data.frq_bode=d(:,1)
    head=[head;
          "db_OL=20*log10(abs(repfreq(OL,data.frq_bode)));"
         ]
  end
endfunction


function  [head,codeg,bndsg,data]=nicholscode(head,codeg,bndsg,data)
  db_requested=%f
  for k=ind
    rk=req(k)
    select rk.type
    case "pmargin"
      codeg=[codeg;
             "p_margin1(OL)"]
      bndsg=[bndsg;
             rk.data, %inf ];
    case "gmargin"
      codeg=[codeg;
             "g_margin1(OL)"]
      bndsg=[bndsg;
             rk.data, %inf ];
    case "clpeak" 
      db_requested=%t;
      codeg=[codeg;
             "dBdist2isogain("+sci2exp((rk.data,0))+","+db+","+phi+")"];
      bndsg=[-%inf 0];
    case "gplow"
      db_requested=%t
      head=[head;
            "req_data_gplow="+sci2exp(rk.data,0)]
      codeg=[codeg;
             "evalPolylineRequirement2(phi,db_nich,req_data_gplow,""lower"")"];

      bndsg=[bndsg;
             -%inf,0];
    case "gphigh"
      db_requested=%t
      head=[head;
            "req_data_gphigh="+sci2exp(rk.data,0)]
      codeg=[codeg;
             "evalPolylineRequirement2(phi,db_nich,req_data_gphigh,""upper"")"];
      bndsg=[bndsg;
             -%inf 0];
    end
  end

  if db_requested then
    ud=handles(sub).user_data;
    data.frq_nich=ud.loci.display_function_data;
    head=[head
          "repf=repfreq(OL,data.frq_nich);"
          "[phi,db_nich]=phasemag(repf);db_nich=db_nich(:);"]
  end
endfunction
function [head,codeg,bndsg,data]=filtercode(head,codeg,bndsg,data)
  in=subWins(sub).in
  out=subWins(sub).out
  //several "filter" window may be present for different imput-output couples
  vn="CL_"+in+"_"+out
  head=[head;
        vn+"=sisotoolClosedLoop(sys,["+sci2exp(in)+","+sci2exp(out)+"]);"];
  editorData=sisotoolGetEditorData(handles(sub)(1))

  db_needed=%f
  for k=ind
    rk=req(k)
    select rk.type
    case "gmin"
      db_needed=%t
      codeg=[codeg;
             "min(db_"+vn+");"]
      bndsg=[bndsg;
             rk.data, %inf ];
    case "gmax"
      db_needed=%t
      codeg=[codeg;
             "max(db_"+vn+");"]
      bndsg=[bndsg;
             -%inf,rk.data ];
    case "pmargin"
      codeg=[codeg;
             "p_margin1("+vn+")"]
      bndsg=[bndsg;
             rk.data, %inf ];
    case "gmargin"
      codeg=[codeg;
             "p_margin1("+vn+")"]
      bndsg=[bndsg;
             rk.data, %inf ];
    end
  end
  if db_needed then 
    d=editorData.loci.data
    execstr("data.frq_"+vn+"=d(:,1)")
    head=[head;
          "db_"+vn+"=20*log10(abs(repfreq(CL,data.frq_"+vn+")));"]
    
  end
endfunction
       
function [head,codeg,bndsg,data]=stepcode(head,codeg,bndsg,data)
  u_tf=unique(sel_tf)
  u_req=unique(sel_req)
  expr=AnalysersSubwins(sub).expr(u_tf);
  if size(expr,'*')==1 then
    TF="TF"+string(find(TF_exprs==expr))
  else
    TF=[];
    for k=1:size(expr,"*")
      TF=[TF,"TF"+string(find(TF_exprs==expr(k)))]
    end
    TF="["+strcat(TF,';')+"]"
  end
  t="t_step_"+string(sub);
  y="y_step_"+string(sub);
  head=[head;
        "[tfinal,dt] =sim_horizon("+TF+");";
        t+"=0:dt:tfinal;"
        y+"=csim(""step"","+t+","+TF+");"]
  
  for k=1:size(u_req,"*")
    rk=req(u_req(k))
    select rk.type
    case "upbound"
      upbound="req_data_upbound_"+string(sub)
      head=[head;
            upbound+"="+sci2exp(rk.data,0)+";"]
    case "lowbound"
      lowbound="req_data_lowbound_"+string(sub)
      head=[head;
            lowbound+"="+sci2exp(rk.data,0)+";"];
    case "tunnel" 
      bnds=[0 0;%inf 1000];
      d="req_data_tunnel_"+string(sub)
      lowbound="req_data_lowbound_"+string(sub)
      upbound="req_data_upbound_"+string(sub)
      head=[head;
            d+"=sisotoolRequirementArea(""step"",""tunnel"","+sci2exp(rk.data,0)+",[0 0;"+t+"($),max("+y+")])"];
    end
  end
  for k=1:size(sel_req,"*")
    i=find(sel_tf(k)==u_tf);
    rk=req(sel_req(k));
    select rk.type
    case "upbound"
      codeg=[codeg;
             "evalPolylineRequirement("+t+","+y+"("+string(i)+",:),"+upbound+",""upper"")"];
      bndsg=[bndsg;
             -%inf 0];
    case "lowbound"
      
      codeg=[codeg;
             "evalPolylineRequirement("+t+","+y+"("+string(i)+",:),"+lowbound+",""lower"")"];
      bndsg=[bndsg;
             -%inf,0];
    case "tunnel"
       codeg=[codeg;
             "evalPolylineRequirement("+t+","+y+"("+string(i)+",:),"+d+".low(2:7,:),""lower"")"
              "evalPolylineRequirement("+t+","+y+"("+string(i)+",:),"+d+".high(2:5,:),""upper"")"
              ];
              
      bndsg=[-%inf 0
             -%inf,0];     
    end
  end
endfunction 
function [head,codeg,bndsg,data]=impulsecode(head,codeg,bndsg,data)
  u_tf=unique(sel_tf)
  u_req=unique(sel_req)
  expr=AnalysersSubwins(sub).expr(u_tf);
  if size(expr,'*')==1 then
    TF="TF"+string(find(TF_exprs==expr))
  else
    TF=[];
    for k=1:size(expr,"*")
      TF=[TF,"TF"+string(find(TF_exprs==expr(k)))]
    end
    TF="["+strcat(TF,';')+"]"
  end
  
  t="t_impulse_"+string(sub);
  y="y_impulse_"+string(sub);
  head=[head;
        expr
        "[tfinal,dt] =sim_horizon("+TF+");";
        t+"=0:dt:tfinal;"
        y+"=csim(""impulse"","+t+","+TF+");"];

    for k=1:size(u_req,"*")
     rk=req(u_req(k))
     select rk.type
     case "upbound"
       upbound="req_data_upbound_"+string(sub)
       head=[head;
             upbound+"="+sci2exp(rk.data,0)]
     case "lowbound"
       lowbound="req_data_lowbound_"+string(sub)
       head=[head;
             lowbound+"="+sci2exp(rk.data,0)] 
     case "tunnel" //TBD?
     end
  end
  for k=1:size(sel_req,"*")
    i=find(sel_tf(k)==u_tf);
    rk=req(sel_req(k))
    select rk.type
    case "upbound"
      codeg=[codeg;
             "evalPolylineRequirement("+t+","+y+"("+string(i)+",:),"+upbound+",""upper"")"];
      bndsg=[bndsg;
             -%inf 0];
   
    case "lowbound"
      codeg=[codeg;
             "evalPolylineRequirement("+t+","+y+"("+string(i)+",:),"+lowbound+",""lower"")"];
      bndsg=[bndsg;
             -%inf,0];
     
    end
  end
endfunction
function [head,codeg,bndsg,data]=analyserbodecode(head,codeg,bndsg,data)
  u_tf=unique(sel_tf)
  u_req=unique(sel_req)
  expr=AnalysersSubwins(sub).expr(u_tf);
  if size(expr,'*')==1 then
    TF="TF"+string(find(TF_exprs==expr))
  else
    TF=[];
    for k=1:size(expr,"*")
      TF=[TF,"TF"+string(find(TF_exprs==expr(k)))]
    end
    TF="["+strcat(TF,';')+"]"
  end
       
  sub=string(sub);
  for k=1:size(u_req,"*")
    rk=req(u_req(k))
    select rk.type
    case "gmax"
      upbound="req_data_upbound_"+sub
      head=[head;
            upbound+"="+sci2exp(rk.data,0)]
    case "gmin"
      lowbound="req_data_lowbound_"+sub
      head=[head;
            lowbound+"="+sci2exp(rk.data,0)] 
    ;
    end
  end
  
  frq="frq_bode_"+sub;
  db="db_bode_"+sub;
  db_needed=%f
  for k=1:size(sel_req,"*")
    i=find(sel_tf(k)==u_tf);
    rk=req(sel_req(k))
    select rk.type
    case "gmax"
      db_needed=%t
      codeg=[codeg;
             "evalPolylineRequirement("+frq+","+db+"("+string(i)+",:),"+upbound+",""upper"")"];
      bndsg=[bndsg;
             -%inf 0];
    case "gmin"
      db_needed=%t
      codeg=[codeg;
             "evalPolylineRequirement("+frq+","+db+"("+string(i)+",:),"+lowbound+",""lower"")"];
      bndsg=[bndsg;
             -%inf,0];
    case "pmargin"
      codeg=[codeg;
             "p_margin1("+TF+")"]
      bndsg=[bndsg;
             rk.data, %inf ];
    case "gmargin"
      codeg=[codeg;
             "p_margin1("+TF+")"]
      bndsg=[bndsg;
             rk.data, %inf ]
    end
  end
  if db_needed then
    head=[head;
          "["+frq+",repf]=repfreq("+TF+");";
          db+"=20*log10(abs(repf));"];
  end
endfunction

function [head,codeg,bndsg,data]=nyquistcode(head,codeg,bndsg,data)
  //no requirement can be defined for nyquist
endfunction

function [head,codeg,bndsg,data]=analysernicholscode(head,codeg,bndsg,data)
  u_tf=unique(sel_tf)
  u_req=unique(sel_req)
  
  expr=AnalysersSubwins(sub).expr(u_tf);
  sub=string(sub)
  for k=1:size(u_req,"*")
    rk=req(u_req(k))
    select rk.type
    case "gmax"
      gplow="req_data_gplow_"+sub
      head=[head; gplow+"="+sci2exp(rk.data,0)]
    case "gmin"
      gphigh="req_data_gphigh_"+sub
      head=[head; gphigh+"="+sci2exp(rk.data,0)] 
    end
  end
  if size(expr,'*')==1 then
    TF="TF"+string(find(TF_exprs==expr))
  else
    TF=[];
    for k=1:size(expr,"*")
      TF=[TF,"TF"+string(find(TF_exprs==expr(k)))]
    end
    TF="["+strcat(TF,';')+"]"
  end
  
  db="db_nichols_"+sub;
  phi="phi_nichols_"+sub;
  db_requested=%f;
  for k=1:size(u_req,"*")
    rk=req(u_req(k))
    select rk.type
    case "pmargin"
      codeg=[codeg;
             "p_margin1("+TF+")"]
      bndsg=[bndsg;
             rk.data, %inf ];
    case "gmargin"
      codeg=[codeg;
             "g_margin1("+TF+")"]
      bndsg=[bndsg;
             rk.data, %inf ];
    case "clpeak"  
      db_requested=%t;
      codeg=[codeg;
             "dBdist2isogain("+sci2exp((rk.data,0))+","+db+","+phi+")"];
      bndsg=[-%inf 0]
      
    case "gplow"
      db_requested=%t
      codeg=[codeg;
              "evalPolylineRequirement2("+phi+","+db+","+gplow+",""lower"")"];
      bndsg=[bndsg;
             -%inf,0];
    case "gphigh"
      db_requested=%t
      codeg=[codeg;
             "evalPolylineRequirement2("+phi+","+db+","+gphigh+",""upper"")"];
      bndsg=[bndsg;
             -%inf 0];
    end
  end
  if db_requested then
     head=[head;
           "[Fmin,Fmax]=sisotoolFreqBounds("+TF+")"
           "[frq,repf]=repfreq("+TF+",Fmin,Fmax)";
           "["+phi+","+db+"]=phasemag(repf);"]
  end
endfunction
function [head,codeg,bndsg,data]=pzcode(head,codeg,bndsg,data)  
  u_tf=unique(sel_tf)
  u_req=unique(sel_req)
  expr=AnalysersSubwins(sub).expr(u_tf);
  if size(expr,'*')==1 then
    cl_roots="cl_roots=roots(TF"+string(find(TF_exprs==expr))+".den)"
  else
    cl_roots=[];
    for k=1:size(expr,"*")
      cl_roots=[cl_roots;"roots(TF"+string(find(TF_exprs==expr(k)))+".den)"]
    end
    cl_roots="cl_roots=["+strcat(cl_roots,';')+"]"
  end

  head=[head;
        cl_roots];

  for k=1:size(u_req,"*")
    rk=req(u_req(k))
    select rk.type
    case "settlingtime"
      codeg=[codeg;
             "-real(cl_roots)/4"]
      bndsg=[bndsg;
             ones(cl_roots)*[0 rk.data]];
    case "damping"
      codeg=[codeg;
             "-real(cl_roots)./abs(cl_roots)"]
      bndsg=[bndsg;
             ones(cl_roots)*[rk.data 1]];
    case "wmin"
      codeg=[codeg;
             "abs(cl_roots)"]  
      bndsg=[bndsg;
             ones(cl_roots)*[rk.data %inf]];     
    case "wmax"
      codeg=[codeg;
             "abs(cl_roots)"]  
      bndsg=[bndsg;
             ones(cl_roots)*[0 rk.data]];     
    end
  end
  
endfunction
