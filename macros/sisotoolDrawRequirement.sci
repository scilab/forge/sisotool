// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolDrawRequirement(ax,requirementType,requirementData)
  //units is defined in the calling scope.
  fig=ax.parent;
  id=fig.immediate_drawing;
  fig.immediate_drawing='off'
  reqs=ax.children($).children
  editorType=ax.tag
  ind=find(reqs.tag==requirementType)
  if ind==[] then fig.immediate_drawing=id;return,end
  req=reqs(ind)

  if requirementData<>[] then 
    if req.type=="Polyline" then
      //For Bode type plots, Gain and phase margin requirements are
      //located at the crossover frequency if it exists, so so they cannot be treated as usual. See also
      //sisotoolOLBodeEditor, sisotoolRedrawAnalysers and sisotoolUpdateAnalysers
      if or(editorType==["olbm" "clbm"])&requirementType=="gmargin" then
        edm=sisotoolGetEditorData(ax);
        if edm.gmargin(2).data<>[] then 
          //There is a cross over frequency set by sisotoolOLBodeEditor
          frg=edm.gmargin(2).data(1)
          g=magconv("dB",units.magnitude,requirementData);
          req.data=[frg 0;frg -g]
        else
          req.data=[]
        end
      elseif editorType=="gainplot"&requirementType=="gmargin" then
        edm=sisotoolGetEditorData(ax);
        if edm.gmargin<>[] then 
          //There is a cross over frequency (set by sisotoolRedrawAnalysers and sisotoolUpdateAnalysers)
          frg=edm.gmargin(1)
          g=magconv("dB",units.magnitude,requirementData);
          req.data=[frg 0;frg -g]
        else
          req.data=[]
        end
      elseif  or(editorType==["olbp" "clbp"])&requirementType=="pmargin" then
        edp=sisotoolGetEditorData(ax);
        if edp.pmargin(2).data<>[] then 
          //There is a cross over frequency set by sisotoolOLBodeEditor
          frp=edp.pmargin(2).data(1,1)
          p= phaseconv(_("degrees"),units.phase,requirementData)
          v180=phaseconv(_("degrees"),units.phase,180);
          req.data=[frp -v180;frp -v180+p]
        else
          req.data=[]
        end
      elseif editorType=="phaseplot"&requirementType=="pmargin"
        edp=sisotoolGetEditorData(ax)
        if edp.pmargin<>[] then 
          //There is a cross over frequency (set by sisotoolRedrawAnalysers and sisotoolUpdateAnalysers)
          frp=edp.pmargin(1)
          p= phaseconv(_("degrees"),units.phase,requirementData)
          v180=phaseconv(_("degrees"),units.phase,180);
          req.data=[frp -v180;frp -v180+p]
        else
          req.data=[]
        end
      else
        req.data=sisotoolRequirementArea(ax.tag,requirementType,requirementData,axesbounds(ax))
      end
    else
      d=sisotoolRequirementArea(ax.tag,requirementType, requirementData,axesbounds(ax))
      req.children(1).data=d.low
      req.children(2).data=d.high
    end
  else
    if req.type=="Polyline" then
      req.data=[]
      req.user_data=[]
    elseif req.type=="Compound" then
      req.children(1).data=[];
      req.children(2).data=[];
    end
  end
  //req.user_data=requirementData
  fig.immediate_drawing=id
endfunction
