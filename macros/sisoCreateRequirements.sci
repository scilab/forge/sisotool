// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function req=sisoCreateRequirements(Types,style)
//style=1: filled polyline
//style=2: line
  c=color(250,225,225)
  req=[]
  for k=1:size(Types,"*")
    if Types(k)=="tunnel" then
      xpoly([],[]);plow=gce();
      set(plow,...
          "fill_mode","on",...
          "line_mode","off",...//"foreground",c,...
          "background",c,...         
          "clip_state","clipgrf",...
          "tag","low")
        xpoly([],[]);phigh=gce();
      set(phigh,...
          "fill_mode","on",...
          "line_mode","off",...//"foreground",c,...
          "background",c,...         
          "clip_state","clipgrf",...
          "tag","high")
      p=glue([phigh,plow])
      p.tag=Types(k)
    else
      xpoly([],[]);
      p=gce();
      if style(k)==1 then
        set(p,...
            "fill_mode","on",...
            "line_mode","off",...
            "foreground",c,...
            "background",c,...         
            "clip_state","clipgrf",...
            "tag",Types(k))
      else
        set(p,...
            "fill_mode","off",...
            "line_mode","on",...
            "thickness",5,...
            "foreground",c,...
            "background",c,...         
            "mark_mode","on",...
            "mark_style",4,...
            "mark_foreground",c,...
            "mark_background",c,...
            "clip_state","clipgrf",...
            "tag",Types(k))
      end
    end
    req(k)=p
  end
  req=glue(req($:-1:1))
  req.tag="requirements"
endfunction
