// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolEditorsMenu(fig)
  w=string(fig.figure_id);
  Designs=uimenu("parent", fig, "label", _("Editors"),"Handle_Visible", "off")
  uimenu("parent",Designs, ...
         "label", _("Graphical Compensator Editor selector"),...
         "callback" ,list(4,"sisotoolEditorsGui("+w+")"))
  uimenu("parent", Designs, ...
         "label", _("Textual Compensator Editor"),...
         "callback" ,list(4,"sisotoolCreateCGUI("+w+")"))
  uimenu("parent", Designs,...
         "label", _("Architecture selector"),...
         "callback" ,list(4,"sisotoolArchiGui("+w+")"))
endfunction
