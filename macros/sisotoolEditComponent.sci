// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolEditComponent(entityType,k) 
  ax=gca()//current axes has been set by sisotoolEditorsEvents
  mainH=ax.parent
  editorData=sisotoolGetEditorData(ax)
  compensatorName=editorData.tunable

  S=sisotoolGetSession(mainH)
  C=S.Designs(compensatorName)
  if entityType=="poles" then
    v=C(entityType)(:,k)
    if imag(v)==0 then
      while %t
        [ok,v]=getvalue(_("Edit real pole"),...
                        _("Value"),...
                        list("vec",1), ...
                        sci2exp(real(v),0))
        if ~ok then return,end
        if isreal(v) then break;end
      end
    else
      while %t
        [ok,R,I]=getvalue(_("Edit complex pole"),...
                                [_("Real part");("Imaginary part")],...
                                list("vec",1,"vec",1),...
                                [sci2exp(real(v));sci2exp(imag(v))])
        if ~ok then return,end
        if isreal(R)&isreal(I) then break;end
      end
      v=R-imult(abs(I));
    end
  elseif entityType=="zeros" then
    v=C(entityType)(:,k)
    if imag(v)==0  then
      while %t
        [ok,v]=getvalue(_("Edit real zero"),...
                        _("Value"),...
                        list("vec",1), ...
                        sci2exp(real(v),0))
        if ~ok then return,end
        if isreal(v) then break;end
      end
    else
      while %t
        [ok,R,I]=getvalue(_("Edit complex zero"),...
                                [_("Real part");("Imaginary part")],...
                                list("vec",1,"vec",1),...
                                [sci2exp(real(v));sci2exp(imag(v))])
        if ~ok then return,end
        if isreal(R)&isreal(I) then break;end
      end
       v=R-imult(abs(I));
    end
  elseif entityType=="leadslags" then
    z=C(entityType)(1,k)
    p=C(entityType)(2,k)
    while %t
      [ok,z,p]=getvalue(_("Edit lead or lag"),...
                        [_("Zero");("Pole")],...
                        list("vec",1,"vec",1),...
                        [sci2exp(z);sci2exp(p)])
      if ~ok then return,end
      if isreal(z)&isreal(p) then break;end
    end
    v=[z;p]
   
  elseif entityType=="notches" then
    z=C(entityType)(1,k);
    p=C(entityType)(2,k);
    [wn,zetaz]=damp(z,C.dt)
    [wn,zetap]=damp(p,C.dt)
 
    while %t
      [ok,wn,zetaz,zetap]=getvalue(_("Edit lag"),...
                        [_("Natural Frequency (rd/s)");_("Damping (Zero)");_("Damping (Pole)")],...
                        list("vec",1,"vec",1,"vec",1),...
                        [sci2exp(wn);sci2exp(zetaz);sci2exp(zetap)])
      if ~ok then return,end
      if isreal(wn)&isreal(zetaz)&isreal(zetap)&wn>0&abs(zetaz)<=1&abs(zetap)<=1 then break;end
    end
    z=wnzeta2complex(C.dt,wn,zetaz)
    p=wnzeta2complex(C.dt,wn,zetap) 
    v=[z;p]
    
  end
  S=sisotoolAddHistory(S,"Compensator","update",compensatorName,entityType,k,C(entityType)(:,k));
  S=sisotoolUpdateCElement(S,compensatorName,entityType,k,v)
  sisotoolSetSession(S,mainH)
  sisotoolRedraw(mainH);
  sisotoolUpdateOptimGui(mainH)
endfunction
