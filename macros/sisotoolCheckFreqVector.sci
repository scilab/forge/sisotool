// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [r,v]=sisotoolCheckFreqVector(bounds)
  h=gcbo
  if execstr("v=("+h.string+")","errcatch")<>0 then
    r=~sisotoolSetError(h,%t,_("Expression cannot be evaluated"))
  elseif type(v)<>1|~isreal(v)|or(diff(v)<=0) then
    r=~sisotoolSetError(h,%t,_("increasing real vector expected"))
  elseif argn(2)<1 then
      r=~sisotoolSetError(h,%f)
  elseif ~and(bounds(1)<=v&v<=bounds(2)) then
    msg=msprintf(_("elements must be in the interval [%g %g]"),bounds(1),bounds(2))
    r=~sisotoolSetError(h,%f,msg)
  else
    r=~sisotoolSetError(h,%f)
  end
endfunction
