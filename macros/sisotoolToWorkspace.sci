// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolToWorkspace()
//callback for sisotoolExportGui
  gui=gcbo.parent
  ud=gui.user_data
 

  exportAs=ud.exportAs 
  exportselection=ud.exportselection //handle on the checkboxes list
  exprs=ud.expr //the expressions computing the transfer functions
  
  vars=who('get'); //get the current workspace variable names.

  sys=sisotoolGetSys(ud.mainH)
  exportSel=find(exportselection.value==1);
  

  //Check for name conflicts
  conflicts=[];
  vars=who('get'); //get the current workspace variable names.
  [v,conflicts,kb]=intersect(vars,exportAs(exportSel))

  if conflicts<>[] then
    r=messagebox([_("Following variables already exist in workspace")
                  vars(conflicts)
                  "Overwrite them?"],..
                 "Warning","question",[_("Ok"), _("Cancel")],"modal")
    if r==2 then return,end
  end

  D=list();
  for i=exportSel
    D($+1)=evstr(exprs(i))
  end
  args=strcat(exportAs(exportSel),',');
  execstr('['+args+']=resume(D(:))')
endfunction

