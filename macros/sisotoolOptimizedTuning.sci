// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolOptimizedTuning()
  guiHandle=gcbo.parent //The handle on the optimization GUI figure
  [sisotoolSetSysVars,x0,bnds,g,gbnds,data]= sisotoolOptimSettings(guiHandle)
  if g==[] then
     messagebox(msprintf(_("Maximum loop peak gain is not yet handled,\n the optimization cannot be done")),"modal");
    return
  end
  if x0==[] then
    messagebox(msprintf(_("No compensator element selected,\n the optimization cannot be done")),"modal");
    return
  end
  if gbnds==[] then
    messagebox(msprintf(_("No requirement selected,\n the optimization cannot be done")),"modal");
    return
  end
  ud=guiHandle.user_data;
  mainH=ud.mainH;
  S=sisotoolGetSession(mainH);
  sys=S.Designs;
  tunable=sisotoolGetArchiSpecs(sys.archi,"tunable");
  nx=size(x0,"*")
  nbnds=size(gbnds,1);
  ieee_save=ieee();ieee(2)
  solver="fminsearch"
  if solver=="ipopt" then //not used yet
    params=init_param();
    params=add_param(params,'hessian_approximation','limited-memory');
    deff('y=dg(x,x_new,data)','y = numderivative(list(g,x_new,data),x,1e-8)');
    deff("v=f(x,x_new)","v=0")
    deff("v=df(x,x_new)","v=zeros(x)")
    sparse_dg=[ ones(nx,1).*.(1:nbnds)',(1:nx)'.*.ones(nbnds,1)];
    
    constr_lin_type=ones(nbnds,1); 
    var_lin_type=ones(x0);
    [x,fopt,extra] = sciipopt(x0,f,df,list(g,data),list(dg,data),sparse_dg,[],[],var_lin_type, ...
                              constr_lin_type,gbnds(:,2),gbnds(:,1), bnds(:,1),bnds(:,2), ipoptTrace,params);
    disp(extra.constr_viol)
    ok=extra.constr_viol==0
  else //fminsearch
    deff("v=cost(x,data)","gg=g(x,%t,data);v=max([gg-gbnds(:,2);gbnds(:,1)-gg])")
    mputl(["function v=cost(x,data)";"gg=g(x,%t,data);v=max([gg-gbnds(:,2);gbnds(:,1)-gg])";"endfunction"],"/tmp/cost.sci")
    x=x0;
    opt=S.OptimSettings.Options
    if opt.OutputFcn<>[] then opt.OutputFcn=evstr(opt.OutputFcn);end
    while %t
      [x,fopt,exitflag]=fminsearch (list(cost ,data), x,opt );x=x(:);
      ok=fopt<=opt.TolFun
      if ok then break;end
      if exitflag==1 then
        messagebox(msprintf(_("No feasible solution found\nRelax requirements or add controler elements")),"sisotool message","error","modal");
        break
      end;
      if exitflag==0 then 
         S.Designs=sisotoolSetSysVars(x)
         sisotoolSetSession(S,mainH);
         sisotoolRedraw(mainH)
        if messagebox(msprintf(_("No feasible solution found\nMax number of iterations reached\nContinue?")),"sisotool message","question",[_("Yes"),_("No")],"modal")==2 then break;end;
      end
    end
  end
  if ok then
    S=sisotoolAddHistory(S,"Compensator","update",tunable,"all",list(sys(tunable)));
    S.Designs=sisotoolSetSysVars(x)
    sisotoolSetSession(S,mainH);
    sisotoolRedraw(mainH)
  end
  ieee(ieee_save)
endfunction





function m=p_margin1(S)
  m=p_margin(S)
  if m==[] then m=-1;end
endfunction
function m=g_margin1(S)
  m=g_margin(S)
  if m==[] then m=-1;end
endfunction

function ind=ipoptTrace(params)
  ind=1
  mprintf("mode=%d,iter=%d,inf_pr=%g,inf_du=%g,mu=%g,d_norm=%g,reg_size=%g,\nalpha_du=%g,alpha_pr=%g,ls_trials=%d\n\n",...
          params.algorithm_mode,params.iter,params.inf_pr,params.inf_du,params.mu,params.d_norm,...
          params.regularization_size,params.alpha_du,params.alpha_pr,params.ls_trials)
endfunction

function c=evalPolylineRequirement(x,y,req_data,bound_type)
  //for function
   [m,kd]=min(abs(x-min(req_data(:,1))));
   [m,kf]=min(abs(x-max(req_data(:,1))));
   if bound_type=="lower" then
     c=max(interpln(req_data',x(kd:kf))-y(kd:kf))
   else
     c=max(y(kd:kf)-interpln(req_data',x(kd:kf)))
   end
endfunction
function c=evalPolylineRequirement2(x,y,req_data,bound_type)
  vmin=min(req_data(:,1));
  kmin=find((x(1:$-1)<vmin&x(2:$)>vmin)|(x(1:$-1)>vmin&x(2:$)<vmin));
  vmax=max(req_data(:,1));
  kmax=find((x(1:$-1)<vmax&x(2:$)>vmax)|(x(1:$-1)>vmax&x(2:$)<vmax));
  k=unique([1 kmin kmax size(x,'*')]);
  
  xt=x(k(1:$-1)+diff(k)/2);
  c=-%inf
  for i=find(xt>=vmin&xt<=vmax)
    sel=k(i):k(i+1);
    if bound_type=="lower" then
     c=max(c,max(interpln(req_data',x(sel))-y(sel)));
    else
      c=max(c,max(y(sel)-interpln(req_data',x(sel))));
    end
  end
endfunction
