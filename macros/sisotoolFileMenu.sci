// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolFileMenu(fig,mainH)
  if argn(2)==1 then mainH=fig;end
  w=string(mainH.figure_id);
  File=uimenu("parent", fig, "label", "&"+_("File"),"Handle_Visible", "off"),
  Save = uimenu("parent"   , File, ..
                "label"    , _("Save")+" CTRL+S",...
                "callback" ,list(4,"sisotoolSaveSession("+w+",%f)"))

  Import = uimenu("parent"   , File, ..
                  "label"    , _("Import..."),...
                  "Enable"   , "on", ...
                  "callback" ,list(4,"sisotoolImportGui("+w+")"))
  Export = uimenu("parent"   , File,...
                  "label"    , _("Export..."),...
                  "callback" ,list(4,"sisotoolExportGui("+w+")"))
 
  SaveAs = uimenu("parent"   , File, ..
                "label"    , _("Save As..."),...
                "callback" ,list(4,"sisotoolSaveSession("+w+")"))

  Load = uimenu("parent"   , File, ..
                "label"    , _("Load Session...")+" CTRL+O",...
                "callback" ,list(4,"sisotoolLoadSession("+w+")"))


  Print = uimenu("parent"   , File, ..
                 "label"    , _("Print..."),...
                 "callback" ,list(4,"printfigure("+string(fig.figure_id)+")"))

  PrintToFig = uimenu("parent"   , File, ..
                      "label"    , _("Save figure..."),...
                      "callback" ,list(4,"sisotoolSaveFigure("+string(fig.figure_id)+")"))
  Close = uimenu("parent"   , File, ..
                 "label"    , _("Close"),...
                 "callback" ,list(4,"sisotoolCloseSession("+w+")"))
endfunction
