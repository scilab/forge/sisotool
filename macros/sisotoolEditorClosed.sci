// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolEditorClosed()
  //call back for handling standalone Editor/Viewer closure
  fig=gcbo; //the handle on the closed editor
  ud=get(fig,"user_data");
  mainH=ud.mainH;
  S=sisotoolGetSession(mainH);
  S(ud.type)=[]
  sisotoolSetSession(S,mainH)
  delete(fig)
endfunction
