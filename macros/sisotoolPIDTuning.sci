// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolPIDTuning()
  guiHandle=gcbo.parent
  ud=guiHandle.user_data;
  mainH=ud.mainH;

  frames=guiHandle.children(2:$)// [Method Type Compensator]
  S=sisotoolGetSession(mainH);
  PIDSettings=S.PIDSettings
  sys=S.Designs   
  out_of_index=frames($).children(2).value
  PIDSettings.out_of_index=out_of_index
  
  //PIDType
  typeFrameUI=frames(2).children;
  rb=typeFrameUI(typeFrameUI.style=="radiobutton")
  PIDType=rb(rb.value==1).tag
  PIDSettings.PIDType=PIDType
  
  //PID Method
  m=frames(1).children($)
  method=m.user_data(m.value);
  PIDSettings.method=m.value
  //parameters
  if PIDType=="PIDF" then
    N=evstr(frames(1).children($-2).string)
  else
    N=[]
  end
  PIDSettings.N=N
  pars=list();
  if method=="PP" then
    wn=evstr(frames(1).children(5).string)
    zeta=evstr(frames(1).children(3).string)
    pars=list(wn,zeta)
    PIDSettings.parameters=pars
  elseif method=="CHR" then
    which=["setpoint","disturbance"]
   
    overshoot=frames(1).children(1).value==2
    pars=list(which(frames(1).children(2).value),overshoot)
    PIDSettings.parameters=pars
  end
  S.PIDSettings=PIDSettings
  out_of=sisotoolGetArchiSpecs(sys.archi,"out_of");
  out_of=out_of(out_of_index)

  S.Designs(out_of)=sisotoolNewController(sys.dt)
  P=sisotoolSys2OL(S.Designs,out_of)
  C=PIDTuning(P,PIDType,convstr(method),N,pars(:))
  if C==[] then
    //no PID can be found with the selected method
    messagebox(msprintf(_("%s compensator cannot be commputed with the selected method"),PIDType),"modal")
  else
    S.Designs(out_of)=sisotooltf2C(C)
    sisotoolSetSession(S,mainH);
    sisotoolRedraw(mainH)
  end
endfunction

