// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolEditorsGui(mainWin)
  mainH=get_figure_handle(mainWin);
  S=sisotoolGetSession(mainH)
  archi=S.Designs.archi
  guiHandle= S.EditorsSelector
  if  type(guiHandle)==9 then
    ud=guiHandle.user_data
    if ud.archi==archi then 
      return,
    end
  end
 
  sisoguilib=lib(sisotoolPath()+"/macros/gui")

  margin_x     = 5;      // Horizontal margin between each elements
  margin_y     = 3;      // Vertical margin between each elements
  button_w     = 100;
  button_h     = 30;
  label_h      = 20;
  col_w        = 60;
  row_header_h = 1.2*label_h;
  archiSpecs=sisotoolGetArchiSpecs(archi);
  editorsSpecs=sisotoolGetEditorsSpecs();
  OL_rows_names= editorsSpecs.OL_editor_names;
  OL_rows=size(OL_rows_names,'*');
  OL_cols_names=archiSpecs.out_of;
  OL_cols=size(OL_cols_names,'*');
  //put the controller blocks first
  tunable=archiSpecs.tunable;
  [v,ka]=intersect(tunable,OL_cols_names);
  tunable(ka)=[];
  CL_tunable_names= [tunable;OL_cols_names];
  CL_tunable=size(CL_tunable_names,'*');
  CL_cols_names=archiSpecs.out;
  CL_cols=size(CL_cols_names,'*');
  CL_rows_names=archiSpecs.in;
  CL_rows=size(CL_rows_names,'*');


  OL_frame_h      = (OL_rows+1)*(label_h+margin_y)+row_header_h+2*margin_y;
  CL_frame_h      = (CL_rows+1)*(label_h+margin_y)+row_header_h+2*margin_y;
  
  frame_w        = 130+(CL_cols)*(col_w+margin_x)+85;
 
  axes_w       = frame_w+3*margin_x;
  
  axes_h       = 3*margin_y+OL_frame_h +CL_frame_h+button_h+margin_y;
  defaultfont  = "arial"; // Default Font;
  if  type(guiHandle)<>9 then //the editor selector is closed
    [path, fname] = fileparts(S.Path)
    guiHandle=figure("dockable", "off", ...
                     "menubar", "none", ...
                     "toolbar", "none", ...
                     "infobar_visible", "off",  ...
                     "axes_size", [axes_w,axes_h],...
                     "figure_name", msprintf(_("%s: Graphical editors selector (%%d)"),fname),...
                     "default_axes","off",...
                     "event_handler_enable", "off",...
                     "visible","off",...
                     "resize","off",...
                     "icon",sisotoolPath()+"/icons/sisotool32.png",...
                     "closerequestfcn","sisotoolGuiClose");

  else
    delete(guiHandle.children)
    guiHandle.axes_size= [axes_w axes_h];
  end
 
  xf=0;
  yf=guiHandle.axes_size(2)-OL_frame_h-margin_y
  OL_Frame=uipanel('Parent',guiHandle,"Title",_("Open loop editors"),...
                       'Units', 'pixels', 'Position',[xf,yf,frame_w+2*margin_x, OL_frame_h],...
                       'BackgroundColor',lightgray());   
  yf=yf-CL_frame_h-margin_y
  CL_Frame=uipanel('Parent',guiHandle,"Title",_("Closed loop Editors"),...
                       'Units', 'pixels', 'Position',[xf,yf,frame_w+2*margin_x, CL_frame_h],...
                       'BackgroundColor',lightgray());   
  
  
  //OL Editors selection
  OL_h=[];
  x=margin_x
  frame_h= row_header_h 
  y=OL_frame_h-label_h-margin_y-frame_h
  CreateRowLabels(OL_Frame,x,y,[130 col_w*ones(1,OL_cols)],["type\output of ",OL_cols_names'])
  //Build rows
  for l=1:OL_rows
    y=y-label_h-margin_y;
    r=CreateCheckBoxRow(OL_Frame,x,y, OL_rows_names(l),zeros(1,OL_cols),[130 col_w*ones(1,OL_cols)])
    OL_h=[OL_h;r];
  end

  OL_h(1:2,1).value=1;

  //CL Editors selection
  CL_h=[];
  x=margin_x
  frame_h= row_header_h 
  y=CL_frame_h-label_h-margin_y-frame_h
  CreateRowLabels(CL_Frame,x,y,[130 col_w*ones(1,CL_cols) 65],["in\out",CL_cols_names,_("adjustable")])
  //Build rows
  x1=x+130+CL_cols*(margin_x+col_w);

  for l=1:CL_rows
    y=y-label_h-margin_y;
    r=CreateCheckBoxRow(CL_Frame,x,y, CL_rows_names(l),zeros(1,CL_cols),[130 col_w*ones(1,CL_cols)])
    p=r($).position
    c=uicontrol('Parent',r($).parent,"style","popupmenu", "string", CL_tunable_names,...
      "units"               , "pixels",...
      "position"            , [p(1)+col_w p(2) 65 label_h],....
      "fontunits"           , "points",...
      "fontsize"            , 12,...
      "value"               , 1,...
      "callback"            , "",...
      "enable"              , "on",...
      "background"          , lightgray());
    CL_h=[CL_h;r c];
  end
  
  callback="sisotoolApplyEditors()";
  CreateButton(guiHandle,margin_x,margin_y,button_w,button_h,_("Apply"),callback)
  
  //Set current settings if any
  subWins=S.EditorsSettings.subWins
  nsubwins=size(subWins,"*")
  OL_h.value=0;
  CL_h(:,1:$-1).value=0;
  for sub=1:nsubwins
    subwin=subWins(sub)
    if or(subwin.type==["clbm" "clbp"]) then //closed loop
      l=find(subwin.in==archiSpecs.in)
      k=find(subwin.out==archiSpecs.out)
      CL_h(l,k).value=1
      CL_h(l,$).value=find(archiSpecs.tunable==subwin.tunable)
    else
      if or(subwin.type==["olbm" "olbp"]) then
        l=find("bode"==editorsSpecs.OL_editor_types)
      else
        l=find(subwin.type==editorsSpecs.OL_editor_types)
      end
      k=find(subwin.out_of==archiSpecs.out_of)
      OL_h(l,k).value=1
    end
  end
  set(guiHandle,"user_data",struct("mainH",mainH,"type","EditorsSelector","OL_h",OL_h,"CL_h",CL_h,"archi",archi))
  S.EditorsSelector=guiHandle
  sisotoolSetSession(S,mainH);
  guiHandle.axes_size= [axes_w axes_h];
   guiHandle.visible="on";
endfunction
