// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function [wmax,phimax]=LeadLag2Properties(z,p,dt)
//computes the location wmax (in rd/s) and value phimax(in rd) of
//the maximum phase of a lead or lag given the zero (z) and the pole (p)
  
//Cas discret
//Code Maple
// with(CodeGeneration):        
// assume(w>0);assume(0<z,z<1);assume(0<p,p<1);assume(p<z);
// hd:=(cos(w)+I*sin(w)-z)/(cos(w)+I*sin(w)-p):
// phi:=arctan(Im(hd),Re(hd)):
// S:=solve(diff(phi,w)=0,w);
// wmax:=S[1];
// phimax:=simplify(eval(phi,w=wmax));
  
//Cas continu
// assume(w>0);assume(z<0);assume(p<z);
// hc:=(I*w-z)/(I*w-p);
// phi:=arctan(Im(hc),Re(hc)):
// S:=solve(diff(phi,w)=0,w):
// wmax:=S[1];
// phimax:=eval(phi,w=wmax);

  if dt=="c" then
    zp=z*p
    wmax=sqrt(zp);
    phimax=atan(-wmax*(p-z),2*zp)
  else
    z2=z^2;p2=p^2;zp=z*p;
    t=sqrt(p2*z2-p2-z2+1);
    wmax=atan(t,(p+z))/dt;
    phimax=atan(-(p-z)*t/(z*p^3-zp-p2+1), (z2-1)/(zp-1));
  end
endfunction
