// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolApplyAnalysers(guiHandle)
//callback of the "Apply" button created by sisotoolAnalysersGui
//transfer selection change, plot type change, number of subwin change  
  if argn(2)<1 then
    guiHandle=gcbo.parent; //the handle on the gui figure
  end
  ud=guiHandle.user_data;
  viewSelectionHandles=ud.viewSelectionHandles
  viewtypes=viewSelectionHandles(1).string
  mainH=ud.mainH
  S=sisotoolGetSession(mainH);
  AnalysersSettings=S.AnalysersSettings
  transferSelectionHandles=ud.transferSelectionHandles;

  [transfer_names,expr]=sisotoolTransfer(S.Designs.archi)
  
  [l,k]=find(matrix(transferSelectionHandles.value==1,size(transferSelectionHandles)))
  //k points to the selected subwindows, l on the selected transfer
  //functions
  
  if k==[] then //no analyser selected, close the analysers window
    delete(S.Analysers)
    S.Analysers=[]
    S.AnalysersSettings.subWins=struct()
    sisotoolSetSession(S,mainH);
    return
  end
  
  subWins_old=S.AnalysersSettings.subWins
  optSettings_old=S.OptimSettings.Analysers
  if size(subWins_old,"*")<>0 then
    old_types=list2vec(subWins_old.type)
  else
    old_types=[]
  end
  subWins=struct();
  optSettings=list();
  ns=0;
  subs=unique(k)
  for i=subs//loop on the subwindows
    if viewtypes(viewSelectionHandles(i).value)=="Bode" then
      types=["gainplot" "phaseplot"]
    else
      types=convstr(viewtypes(viewSelectionHandles(i).value));
      //types=viewtypes(viewSelectionHandles(i).value);
    end
    for t=types
      ns=ns+1 
      subWins(ns).type=t
      li=l(k==i) //transfer functions to be displayed in current subwindow
      gridded(ns)=%f
      refreshed(ns)=%f
      //transfer names stored for datatips
      subWins(ns).transfer_names=transfer_names(li)
      //expression names stored for fast evaluation of the transfer functions
      subWins(ns).expr=expr(li)
      // memorize transferSelection to be able to reopen AnalysersGui in
      // the same state
      subWins(ns).transferSelection=li
      subWins(ns).characteristics_settings=[%f %f %f %f];
      //try to import requirements characteristics and OptimSettings
      //from old to new analysers (The strategy here can be improved) 
      
      //1-find old analysers with same type as the current new one
      kold=find(subWins(ns).type==old_types) 
      if kold==[] then
        //No analyser of the current type was  opened
        //The new analyser is created without any requirements nor
        //characteristics set
        subWins(ns).reqProps=sisotoolRequirementTypes(subWins(ns).type);
        optSettings(ns)=struct()
      else 
        //there is at least one analyser of this type
        //find the old analyser which best match with this one
        score=[];
        tf=subWins(ns).transfer_names;
        for kk=kold
          [v,iold,inew]=intersect(tf,subWins_old(kk).transfer_names)
          score=[score;size(v,"*"),iold,inew ];
        end
        [m,i]=max(score(:,1))

        if m>0 then
          kk=kold(i)
          //import requirements,characteristics and grid properties
          subWins(ns).reqProps=subWins_old(kk).reqProps
          subWins(ns).characteristics_settings=subWins_old(kk).characteristics_settings
          gridded(ns)=S.AnalysersSettings.gridded(kk)
          refreshed(ns)=S.AnalysersSettings.refreshed(kk)
          //import Optim properties
          iold=score(i,2);inew=score(i,3);
          sold=optSettings_old(kk)
          s=sold;
          for f=fieldnames(sold)'
            s(f)=%f(ones(li));
            s(f)(inew)=sold(f)(iold)
          end
          optSettings(ns)=s
        else 
          //there is no analyser of same type with at least one common
          //transfer function
          //The new analyser is created without any requirements set
          subWins(ns).reqProps=sisotoolRequirementTypes(subWins(ns).type);
          optSettings(ns)=struct()
        end
      end
    end
  end
  S.OptimSettings.Analysers=optSettings;
  S.AnalysersSettings.gridded=gridded
  S.AnalysersSettings.refreshed=refreshed
  S.AnalysersSettings.subWins=subWins
  if  or(subWins<>subWins_old)|S.Analysers==[] then 
    S=sisotoolCreateAnalysers(mainH,S) 
    sisotoolSetSession(S,mainH);
    sisotoolRedrawAnalysers(mainH) 
    sisotoolRedrawReqs(mainH)
    sisotoolUpdateOptimGui(mainH)
  end
  
endfunction
