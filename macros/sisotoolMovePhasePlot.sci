// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sys=sisotoolMovePhasePlot(mainH,editorData,entityType,i,k,pt, loc)
  if entityType=="loci" then sys=[];return;end
  ax=gca();//current axes has been set by sisotoolEventHandler
  figureHandle=ax.parent
  S=sisotoolGetSession(mainH);
  units=S.Preferences.units
  sys=S.Designs

  if entityType=="requirements" then
    //see sisotoolRequirementArea
    req=ax.children($).children(i)
    ds=req.data//keep it in case of no move
    if req.tag=="pmargin" then 
      z=phaseconv(_("degrees"),units.phase,-180)
      while %t
        rep=xgetmouse([%t %t]);
        if rep(3)<>-1 then break,end
        req.data(2,2)=max(z,rep(2));
      end
      p=req.data(2,2);

      if or( req.data<>ds) then
        requirementIndex=i;
        subWinIndex=editorData.subWinIndex;
        windowType=ax.parent.tag;
        Settings= windowType+"Settings";
        subWin= S(Settings).subWins(subWinIndex);
        reqProp=subWin.reqProps(requirementIndex);
        
        opt=S.OptimSettings(windowType)(subWinIndex)(reqProp.type);
        S=sisotoolAddHistory(S,"Requirements","update",...
                             windowType,subWinIndex,requirementIndex,reqProp,opt);
        req.user_data=p
        reqProp.data=phaseconv(units.phase,"degrees",p)+180
        subWin.reqProps(requirementIndex)=reqProp;
        S(Settings).subWins(subWinIndex)=subWin

        sisotoolSetSession(S,mainH)
      end
      
    end
    figureHandle.info_message="";
    return
  end
  
  editorData=sisotoolGetEditorData(ax)
  compensatorName=editorData.tunable
 
  C=sys(compensatorName)
  dt=C.dt
  cur=sisotoolGetCElement(S,compensatorName,entityType,k);
  S=sisotoolAddHistory(S,"Compensator","update",compensatorName,entityType,k,cur);
  if or(entityType==["zeros" "poles"]) then
    if entityType=="zeros" then
      info_message=_("Drag this zero along frequency axis to set its natural frequency.")+" "+ ...
          _("Current value:%s")
    else
      info_message=_("Drag this pole along frequency axis to set its natural frequency." )+" "+ ...
          _("Current value:%s")
    end
    mainH.info_message=msprintf(info_message,string(abs(C(entityType)(k))))
    if imag(C(entityType)(k))==0 then //real case, control wn 
      while %t
        set("current_axes", ax);
        rep=xgetmouse([%t %t]);
        if rep(3)<>-1 then break,end
        z=freqconv("Hz","rd/s",rep(1))
        if dt<>"c" then z=exp(z*dt);end
        z=z*sign(C(entityType)(k))
        C(entityType)(k)=z;
        mainH.info_message=msprintf(info_message,string(z))
        sys(compensatorName)=C;
        sisotoolSetSys(sys,mainH);
        sisotoolUpdateResponses(mainH);
      end
    else 
      //complex pair case horizontal displacement control wn.
      r=C(entityType)(k)
      [w,zeta]=damp(r,dt)
      h=editorData(entityType)

      while %t
        set("current_axes", ax);
        rep=xgetmouse([%t %t]);
        if rep(3)<>-1 then break,end
        w=freqconv(units.frequency,"rd/s",rep(1));
        r=wnzeta2complex(dt,w,zeta)
        C(entityType)(k)=r
        mainH.info_message=msprintf(info_message,string(r))
        sys(compensatorName)=C;
        sisotoolSetSys(sys,mainH);
        sisotoolUpdateResponses(mainH);
      end
    end
    mainH.info_message=""
    if rep==-1000 then sys=[];end //the tool has been closed
  elseif entityType=="gain" then
    //no action
  elseif entityType=="loci" then
    //no action
  elseif entityType=="leadslags" then
    if i==1 then
      info_message=_("Drag this zero along frequency axis to set its natural frequency.")+" "+ ...
          _("Current value:%s")
    else
       info_message=_("Drag this pole along frequency axis to set its natural frequency." )+" "+ ...
          _("Current value:%s")
    end
    mainH.info_message=msprintf(info_message,string(abs(C(entityType)(i,k))))
     while %t
       set("current_axes", ax);
       rep=xgetmouse([%t %t]);
       if rep(3)<>-1 then break,end
       z=freqconv("Hz","rd/s",rep(1));
       if dt<>"c" then z=exp(z*dt);end
       z=z*sign(C(entityType)(i,k));
       mainH.info_message=msprintf(info_message,string(z))
       C(entityType)(i,k)=z;
       sys(compensatorName)=C;
       sisotoolSetSys(sys,mainH);
       sisotoolUpdateResponses(mainH);
     end  
    if rep==-1000 then sys=[];end //the tool has been closed
    sys(compensatorName)=C;    
  elseif entityType=="notches" then
     info_message=_("Drag this notch along frequency axis to set its natural frequency." )+" "+ ...
          _("Current value:%s rd/s")
       
    //changes only affect the pole/zero natural frequency 
    [wn,zeta]=damp(C(entityType)(:,k),C.dt);
   
    mainH.info_message=msprintf(info_message,string(wn(1)))
    while %t
      set("current_axes", ax);
      rep=xgetmouse([%t %t]);
      if rep(3)<>-1 then break,end
      wn=freqconv("Hz","rd/s",rep(1));
      C(entityType)(:,k)=wnzeta2complex(dt,wn,zeta);
      mainH.info_message=msprintf(info_message,string(C(entityType)(:,k)));
      sys(compensatorName)=C;
      sisotoolSetSys(sys,mainH);
      sisotoolUpdateResponses(mainH);
    end
    
  end  
  if or(entityType==["gain","loci" "requirements"]) then return;end
  //update optimSettings
  S=sisotoolUpdateCElement(S,compensatorName,entityType,k,C(entityType)(:,k))
  sisotoolSetSession(S,mainH);
endfunction
