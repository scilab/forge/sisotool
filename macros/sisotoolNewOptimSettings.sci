function optimSettings=sisotoolNewOptimSettings(Designs,EditorsSettings,AnalysersSettings)
  // The controller components 
  tunable=sisotoolGetArchiSpecs(Designs.archi, "tunable")
  ControllerSettings=struct()
  for k=1:size(tunable,"*")
     block=S.Designs(tunable(k))
     s=struct()
     gain=struct("enabled",%f,"style",1,"initial",block.gain,"lowbound",0,"highbound",%inf)
     //poles
     n=size(block.poles,2)
     kreal=find(imag(block.poles)==0)
     lb=[-%inf;0]*ones(1,n);
     hb=[%inf;%inf]*ones(1,n);
     hb(2,kreal)=0;
     Poles=struct("enabled",%f(ones(1,n)),...
                  "style",ones(1,n),...
                  "initial",[real(block.poles);imag(block.poles)],"lowbound",lb,"highbound",hb);
     //zeros
     n=size(block.zeros,2)
     kreal=find(imag(block.zeros)==0)
     lb=[-%inf;0]*ones(1,n);
     hb=[%inf;%inf]*ones(1,n);
     hb(2,kreal)=0;
     Zeros=struct("enabled",%f(ones(1,n)),...
                  "style",ones(1,n),...
                  "initial",[real(block.zeros);imag(block.zeros)],"lowbound",lb,"highbound",hb);
     //leads and lags
     n=size(block.leadslags,2)
     lb=[-%inf;-%inf]*ones(1,n);
     hb=[%inf;%inf]*ones(1,n);
     LeadsLags=struct("enabled",%f(ones(1,n)),...
                  "style",ones(1,n),...
                  "initial",block.leadslags,"lowbound",lb,"highbound",hb);

     //notches
     n=size(block.notches,2)
     lb=[0;-1;-1]*ones(1,n);
     hb=[%inf;1;1]*ones(1,n);
     Notches=struct("enabled",%f(ones(1,n)),...
                  "style",ones(1,n),...
                  "initial",block.notches,"lowbound",lb,"highbound",hb);

     ControllerSettings(tunable(k))=struct("gain",gain,"poles",Poles,"zeros",Zeros,"leadslags",LeadsLags,"notches",Notches);
  end
  //The Editors requirements
  Esettings=list()
  subWins=EditorsSettings.subWins
  for k=1:size(subWins,"*")
    subWin=subWins(k)
    reqProps=subWin.reqProps
    ind=sisotoolDefinedReqs(reqProps);
    s=struct()
    for i=1:size(ind,"*")
      s(reqProps(ind(i)).type)=%f;
    end  
    Esettings(k)=s
  end
    
  //The Analysers requirements
  Asettings=list()
  subWins=AnalysersSettings.subWins
  for k=1:size(subWins,"*")
    subWin=subWins(k)
    reqProps=subWin.reqProps
    ind=sisotoolDefinedReqs(reqProps);
    s=struct()
    for i=1:size(ind,"*")
      s(reqProps(ind(i)).type)=%f(ones(subWin.transferSelection));
    end
     Asettings(k)=s
  end
  optimSettings=struct("Controller",ControllerSettings,"Editors",Esettings,"Analysers",Asettings) 
endfunction
