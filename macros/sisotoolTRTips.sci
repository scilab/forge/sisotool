// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function str=sisotoolTRTips(h)
//for time response tips
  c=h.parent
  dfd=c.display_function_data
  timeUnit=dfd.units.time
  h.background=9; //to fix a bug
  t=h.data(1);a=h.data(2)
  tipType=dfd.tipType
  if tipType==[] then
     str=msprintf(_("%s\nTime: %.3g%s\nAmplitude: %.3g"),...
                  dfd.TF_name,t,timeUnit,a)
  elseif tipType(1)=="step" then
    if tipType(2)=="peakResponse" then
      finalValue=c.user_data
      if finalValue<>[] then
        overshoot=round(100*(a-finalValue)/finalValue)
        str=msprintf(_("%s\nTime: %.3g%s\nPeak amplitude: %g\nOvershoot %g%% "),...
                     dfd.TF_name,t,timeUnit,a,overshoot)
      else
        str=msprintf(_("%s\nTime: %.3g%s\nPeak amplitude: %g"),...
                     dfd.TF_name,t,timeUnit,a)
      end
    elseif tipType(2)=="settlingTime" then
      str=msprintf(_("%s\n%g%% Settling time: %.3g%s"),...
                   dfd.TF_name,dfd.tol*100,t,timeUnit)
    elseif tipType(2)=="riseTime" then  
      str=msprintf(_("%s\n%g%% Rise time: %.3g%s"),...
                   dfd.TF_name,dfd.to*100,t,timeUnit)
    end
  elseif tipType(1)=="impulse" then
    if tipType(2)=="peakResponse" then
      str=msprintf(_("%s\nTime: %.3g%s\nPeak amplitude: %g"),dfd.TF_name,t,timeUnit,a)
    elseif tipType(2)=="settlingTime" then
      str=msprintf(_("%s\nSettling time: %.3g%s"),...
                   dfd.TF_name,t,timeUnit)
    end
  end
endfunction
