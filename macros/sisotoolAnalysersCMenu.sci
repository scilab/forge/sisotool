// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolAnalysersCMenu(ax,x,y,axesIndex)
//ax : handle of the selected axes
//x,y : the coordinates of the clicked point in the axes in pixels
//sub : the index of the selected axes  
  win=ax.parent.figure_id
  analysersHandle=get_figure_handle(win)
  mainH=analysersHandle.user_data.mainH
  S=sisotoolGetSession(mainH);
  AnalysersSettings=S.AnalysersSettings
  editorData=sisotoolGetEditorData(ax)
  [entityType,i,k,pt,loc]=sisotoolFindSelected(editorData,[x,y])
  
  if entityType==[] then
    if AnalysersSettings.gridded(editorData.subWinIndex) then G="✔";else G="";end
    if AnalysersSettings.refreshed(editorData.subWinIndex) then R="✔";else R="";end

    
    listOfMenus = list([_("Select view type"),"sisotoolSelectView("+string(win)+","+string(axesIndex)+")"],..
                       [_("Select transfer functions"),"sisotoolSelectTransfer("+string(win)+","+string(axesIndex)+")"]);

    //Handle characteristics
    [cKeys,cNames]=sisotoolCharacteristicsTypes(ax.tag);
    subWinIndex=ax.user_data.subWinIndex
    characteristics_settings=S.AnalysersSettings.subWins(subWinIndex).characteristics_settings
    if size(cKeys,"*")<>0 then

      Characteristics=list(_("Select Characteristics"))
      for l=1:size(cKeys,"*")
        if characteristics_settings(l) then
          args=[string(subWinIndex) string(win) string(l) sci2exp("delete")]
          Characteristics($+1)=["✔"+cNames(l),...
                    "sisotoolCharacteristicsCallback("+strcat(args,",")+")"];
        else
          args=[string(subWinIndex) string(win) string(l) sci2exp("add")]
          Characteristics($+1)=[cNames(l),...
                  "sisotoolCharacteristicsCallback("+strcat(args,",")+")"];  
        end
      end
      listOfMenus($+1)=Characteristics;
    end
    
    listOfMenus($+1)="-";
    
    reqTypes=sisotoolRequirementTypes(ax.tag);
    if size(reqTypes,"*")<>0 then
      Reqs=list(_("Design Requirements"))
      for l=1:size(reqTypes,"*")
        Reqs($+1)=[R+reqTypes(l).name,"sisotoolRequirementsGui("+string(win)+","+string(l)+")"];
      end
      listOfMenus($+1)= Reqs;
    end

    listOfMenus($+1)=[G+_("Grid"),"sisotoolToggleGrid("+string(win)+")"];
    listOfMenus($+1)=[R+_("Refreshed"),"sisotoolToggleRefresh("+string(win)+")"];
  elseif entityType=="requirements" then
    req=ax.children($).children(i)
    lab=req.tag+"     " //spaces added because popup box may be too small
    ud=ax.user_data
    subWinIndex=ud.subWinIndex
    requirementIndex=i
    argsedit=strcat([string(win),sci2exp(requirementIndex)],",")
    argsdelete=strcat([string(win),sci2exp(ax.tag),sci2exp(req.tag),...
                       sci2exp(requirementIndex), ...
                       sci2exp(ud.subWinIndex)],",")
    listOfMenus = list([_("Edit")+" "+lab,"sisotoolRequirementsGui("+argsedit+")"],...
                       [_("Delete")+" "+lab,"sisotoolDeleteReq("+argsdelete+")"])
 
  else //TBD???
    return
  end
  sisotoolCreatePopupMenu(listOfMenus)
endfunction
