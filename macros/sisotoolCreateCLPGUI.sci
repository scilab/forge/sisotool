// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function gui=sisotoolCreateCLPGUI(mainH)
  sisoguilib=lib(sisotoolPath()+"/macros/gui")
  //Creates the compensator editor GUI 
  margin_x     = 5;      // Horizontal margin between each elements
  margin_y     = 5;      // Vertical margin between each elements
  button_w     = 100;
  button_h     = 30;
  frame1_h     = 200;
  frame1_w     = 350;
  label_h      = 20;
  list_h       = frame1_h-label_h-2*margin_y;
  list_w       = (frame1_w-margin_x)/3;
  list_incr    = list_w;
  axes_w       = 2*margin_x+frame1_w;
  axes_h       = 5*margin_y+frame1_h// Frame height
  defaultfont  = "arial"; // Default Font
  
  [fig,fig_id]=newGUI(axes_w,axes_h,_("Closed loop poles viewer"))
  
  gui=uicontrol( ...
      "parent"              , fig,...
      "style"               , "frame",...
      "units"               , "pixels",...
      "position"            , [0 0 axes_w axes_h],...
      "background"          , lightgray(), ...
      "tag"                 , "CompensatorEditor",...
      "visible"             , "on");
  
  yo=axes_h;
  x=margin_x;
  Frame1=CreateFrame(gui,x,yo-frame1_h,frame1_w,frame1_h,_("Closed loop poles"))

  //Creating 4 vertical lists [Type Location Location Damping Frequency]
  callback=""
  y1=frame1_h-label_h-margin_y
  
  x1=margin_x/2;
  CreateLabel(Frame1,x1,y1,list_w,label_h,_("Location")) 
  locations=CreateList(Frame1,x1,y1-list_h,list_w,list_h,callback)

  x1=x1+list_incr;
  CreateLabel(Frame1,x1,y1,list_w,label_h,_("Damping"))
  dampings=CreateList(Frame1,x1,y1-list_h,list_w,list_h,callback)
   
  x1=x1+list_incr;
  CreateLabel(Frame1,x1,y1,list_w,label_h,_("Frequency"))
  frequencies=CreateList(Frame1,x1,y1-list_h,list_w,list_h,callback)
  
  propertyHandles=[locations,frequencies,dampings]
  ud=struct("mainH",mainH,"Type","CLPoles","propertyHandles",propertyHandles)
  set(archi,'user_data',ud)
  archi.closerequestfcn="sisotoolEditorClosed";  
 
endfunction

