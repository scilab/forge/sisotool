// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolDeleteSubwin(win)
  
  analysersHandle=get_figure_handle(win);
  
  //wait for sub-window selection by the user
  C=analysersHandle.children;
  C=C(C.type=="Axes")
  if C==[] then return;end
  sca(C(1))
  [btn,x,y]=xclick();[x,y]=xchange(x,y,"f2i")
  ax=findSubwin(x,y)
  if ax==[] then return;end
  
  ud=analysersHandle.user_data;
  mainH=ud.mainH
  S=sisotoolGetSession(mainH)
  Analysers=S.AnalysersSettings.Analysers
  N=size(Analysers,'*')
  kd=[]
  for k=1:N
    if (Analysers(k).handle==ax) then
      kd=k
      break
    end
  end
  if kd==[] then return;end
  delete(ax)
  Analysers(kd)=[];
  if N>1 then [axes_bounds,axes_margins]=sisotoolSubwinSettings(N-1);end
  //update curently opened subwins
  for k=1:N-1
    ax=Analysers(k).handle
    bounds=axes_bounds(k,:);
    margins=axes_margins(k,:);
    if Analysers(k).type=="bode" then
      //module
      bounds(4)=bounds(4)/2;
      margins(3)=margins(3)+2/8;
      margins(4)=0.05;
      ax(1).axes_bounds=bounds;
      ax(1).margins=margins; 
      //phase
      margins=axes_margins(k,:);
      bounds(2)=bounds(2)+bounds(4);
      margins(4)=margins(4)+2/8;
      margins(3)= 0.05;
      ax(2).axes_bounds=bounds;
      ax(2).margins=margins;
    else
      ax.axes_bounds=bounds;
      ax.margins=margins;
    end
  end
  S.AnalysersSettings.Analysers=Analysers
  sisotoolSetSession(S,mainH)


endfunction
