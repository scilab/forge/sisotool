// This file is part of sisotool module
// Copyright (C)  2016 - INRIA - Serge Steer
//
// This file is licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// For more information, see the COPYING file which you should have received
// along with this program.
function sisotoolCharacteristicsCallback(subWinIndex,win,sel,opt)
  mainH=get_figure_handle(win).user_data.mainH     
  S=sisotoolGetSession(mainH);
  //For Bode plots the  characteristics are the same and 
  //can be managed from gainplot or phase plot
  ax=S.AnalysersSettings.handles(subWinIndex)
  if ax.tag=="gainplot" then
    sub=[subWinIndex subWinIndex+1]
  elseif ax.tag=="phaseplot" then
    sub=[subWinIndex-1 subWinIndex]
  else
    sub=subWinIndex
  end
  subWinIndex=sub(1) //only use the gainplot
  
  analyserType=ax.tag; 
  analyserData=sisotoolGetEditorData(ax)
  cKeys=sisotoolCharacteristicsTypes(analyserType)

  handles=analyserData.characteristics_handles
  
  
  subWins=S.AnalysersSettings.subWins
  if opt=="delete" then
    handles=sisotoolManageCharacteristics(subWinIndex,cKeys(sel),handles,"delete")  
    for k=1:size(sub,"*"),subWins(sub(k)).characteristics_settings(sel)=%f;end
  else
    handles=sisotoolManageCharacteristics(subWinIndex,cKeys(sel),handles,"create")
    handles=sisotoolManageCharacteristics(subWinIndex,cKeys(sel),handles,"update")
    for k=1:size(sub,"*"),subWins(sub(k)).characteristics_settings(sel)=%t;end
  end
  analyserData.characteristics_handles=handles;
  sisotoolSetEditorData(analyserData,ax)
  S.AnalysersSettings.subWins=subWins;
  sisotoolSetSession(S,mainH);
endfunction
